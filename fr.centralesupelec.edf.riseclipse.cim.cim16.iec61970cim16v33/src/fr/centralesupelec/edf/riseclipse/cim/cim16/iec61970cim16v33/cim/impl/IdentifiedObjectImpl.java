/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimPackage;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObject;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IdentifiedObject;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Name;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identified Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getAliasName <em>Alias Name</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getMRID <em>MRID</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getNames <em>Names</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.IdentifiedObjectImpl#getDiagramObjects <em>Diagram Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IdentifiedObjectImpl extends CimObjectWithIDImpl implements IdentifiedObject {
    /**
     * The default value of the '{@link #getAliasName() <em>Alias Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAliasName()
     * @generated
     * @ordered
     */
    protected static final String ALIAS_NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getAliasName() <em>Alias Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAliasName()
     * @generated
     * @ordered
     */
    protected String aliasName = ALIAS_NAME_EDEFAULT;

    /**
     * This is true if the Alias Name attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean aliasNameESet;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * This is true if the Description attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean descriptionESet;

    /**
     * The default value of the '{@link #getMRID() <em>MRID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMRID()
     * @generated
     * @ordered
     */
    protected static final String MRID_EDEFAULT = null;

    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * This is true if the Name attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean nameESet;

    /**
     * The cached value of the '{@link #getNames() <em>Names</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNames()
     * @generated
     * @ordered
     */
    protected EList< Name > names;

    /**
     * The cached value of the '{@link #getDiagramObjects() <em>Diagram Objects</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDiagramObjects()
     * @generated
     * @ordered
     */
    protected EList< DiagramObject > diagramObjects;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected IdentifiedObjectImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CimPackage.eINSTANCE.getIdentifiedObject();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getAliasName() {
        return aliasName;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setAliasName( String newAliasName ) {
        String oldAliasName = aliasName;
        aliasName = newAliasName;
        boolean oldAliasNameESet = aliasNameESet;
        aliasNameESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME,
                    oldAliasName, aliasName, !oldAliasNameESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetAliasName() {
        String oldAliasName = aliasName;
        boolean oldAliasNameESet = aliasNameESet;
        aliasName = ALIAS_NAME_EDEFAULT;
        aliasNameESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME,
                    oldAliasName, ALIAS_NAME_EDEFAULT, oldAliasNameESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetAliasName() {
        return aliasNameESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setDescription( String newDescription ) {
        String oldDescription = description;
        description = newDescription;
        boolean oldDescriptionESet = descriptionESet;
        descriptionESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.IDENTIFIED_OBJECT__DESCRIPTION,
                    oldDescription, description, !oldDescriptionESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetDescription() {
        String oldDescription = description;
        boolean oldDescriptionESet = descriptionESet;
        description = DESCRIPTION_EDEFAULT;
        descriptionESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.IDENTIFIED_OBJECT__DESCRIPTION,
                    oldDescription, DESCRIPTION_EDEFAULT, oldDescriptionESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetDescription() {
        return descriptionESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getMRID() {
        // TODO: implement this method to return the 'MRID' attribute
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setMRID( String newMRID ) {
        // TODO: implement this method to set the 'MRID' attribute
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetMRID() {
        // TODO: implement this method to unset the 'MRID' attribute
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetMRID() {
        // TODO: implement this method to return whether the 'MRID' attribute is set
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setName( String newName ) {
        String oldName = name;
        name = newName;
        boolean oldNameESet = nameESet;
        nameESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.IDENTIFIED_OBJECT__NAME, oldName, name,
                    !oldNameESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetName() {
        String oldName = name;
        boolean oldNameESet = nameESet;
        name = NAME_EDEFAULT;
        nameESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.IDENTIFIED_OBJECT__NAME, oldName,
                    NAME_EDEFAULT, oldNameESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetName() {
        return nameESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Name > getNames() {
        if( names == null ) {
            names = new EObjectWithInverseResolvingEList.Unsettable< Name >( Name.class, this,
                    CimPackage.IDENTIFIED_OBJECT__NAMES, CimPackage.NAME__IDENTIFIED_OBJECT );
        }
        return names;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetNames() {
        if( names != null ) ( ( InternalEList.Unsettable< ? > ) names ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetNames() {
        return names != null && ( ( InternalEList.Unsettable< ? > ) names ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< DiagramObject > getDiagramObjects() {
        if( diagramObjects == null ) {
            diagramObjects = new EObjectWithInverseResolvingEList.Unsettable< DiagramObject >( DiagramObject.class,
                    this, CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS, CimPackage.DIAGRAM_OBJECT__IDENTIFIED_OBJECT );
        }
        return diagramObjects;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetDiagramObjects() {
        if( diagramObjects != null ) ( ( InternalEList.Unsettable< ? > ) diagramObjects ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetDiagramObjects() {
        return diagramObjects != null && ( ( InternalEList.Unsettable< ? > ) diagramObjects ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getNames() ).basicAdd( otherEnd,
                    msgs );
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getDiagramObjects() )
                    .basicAdd( otherEnd, msgs );
        }
        return super.eInverseAdd( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            return ( ( InternalEList< ? > ) getNames() ).basicRemove( otherEnd, msgs );
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            return ( ( InternalEList< ? > ) getDiagramObjects() ).basicRemove( otherEnd, msgs );
        }
        return super.eInverseRemove( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME:
            return getAliasName();
        case CimPackage.IDENTIFIED_OBJECT__DESCRIPTION:
            return getDescription();
        case CimPackage.IDENTIFIED_OBJECT__MRID:
            return getMRID();
        case CimPackage.IDENTIFIED_OBJECT__NAME:
            return getName();
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            return getNames();
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            return getDiagramObjects();
        }
        return super.eGet( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void eSet( int featureID, Object newValue ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME:
            setAliasName( ( String ) newValue );
            return;
        case CimPackage.IDENTIFIED_OBJECT__DESCRIPTION:
            setDescription( ( String ) newValue );
            return;
        case CimPackage.IDENTIFIED_OBJECT__MRID:
            setMRID( ( String ) newValue );
            return;
        case CimPackage.IDENTIFIED_OBJECT__NAME:
            setName( ( String ) newValue );
            return;
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            getNames().clear();
            getNames().addAll( ( Collection< ? extends Name > ) newValue );
            return;
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            getDiagramObjects().clear();
            getDiagramObjects().addAll( ( Collection< ? extends DiagramObject > ) newValue );
            return;
        }
        super.eSet( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset( int featureID ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME:
            unsetAliasName();
            return;
        case CimPackage.IDENTIFIED_OBJECT__DESCRIPTION:
            unsetDescription();
            return;
        case CimPackage.IDENTIFIED_OBJECT__MRID:
            unsetMRID();
            return;
        case CimPackage.IDENTIFIED_OBJECT__NAME:
            unsetName();
            return;
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            unsetNames();
            return;
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            unsetDiagramObjects();
            return;
        }
        super.eUnset( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet( int featureID ) {
        switch( featureID ) {
        case CimPackage.IDENTIFIED_OBJECT__ALIAS_NAME:
            return isSetAliasName();
        case CimPackage.IDENTIFIED_OBJECT__DESCRIPTION:
            return isSetDescription();
        case CimPackage.IDENTIFIED_OBJECT__MRID:
            return isSetMRID();
        case CimPackage.IDENTIFIED_OBJECT__NAME:
            return isSetName();
        case CimPackage.IDENTIFIED_OBJECT__NAMES:
            return isSetNames();
        case CimPackage.IDENTIFIED_OBJECT__DIAGRAM_OBJECTS:
            return isSetDiagramObjects();
        }
        return super.eIsSet( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if( eIsProxy() ) return super.toString();

        StringBuilder result = new StringBuilder( super.toString() );
        result.append( " (aliasName: " );
        if( aliasNameESet )
            result.append( aliasName );
        else
            result.append( "<unset>" );
        result.append( ", description: " );
        if( descriptionESet )
            result.append( description );
        else
            result.append( "<unset>" );
        result.append( ", name: " );
        if( nameESet )
            result.append( name );
        else
            result.append( "<unset>" );
        result.append( ')' );
        return result.toString();
    }

} //IdentifiedObjectImpl
