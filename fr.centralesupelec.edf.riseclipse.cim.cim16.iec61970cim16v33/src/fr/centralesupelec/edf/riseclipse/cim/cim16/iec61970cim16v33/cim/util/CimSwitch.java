/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.util;

import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimPackage
 * @generated
 */
public class CimSwitch< T > extends Switch< T > {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static CimPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CimSwitch() {
        if( modelPackage == null ) {
            modelPackage = CimPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor( EPackage ePackage ) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch( int classifierID, EObject theEObject ) {
        switch( classifierID ) {
        case CimPackage.CIM_OBJECT_WITH_ID: {
            CimObjectWithID cimObjectWithID = ( CimObjectWithID ) theEObject;
            T result = caseCimObjectWithID( cimObjectWithID );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FAULT_IMPEDANCE: {
            FaultImpedance faultImpedance = ( FaultImpedance ) theEObject;
            T result = caseFaultImpedance( faultImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FLOAT_QUANTITY: {
            FloatQuantity floatQuantity = ( FloatQuantity ) theEObject;
            T result = caseFloatQuantity( floatQuantity );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DATE_TIME_INTERVAL: {
            DateTimeInterval dateTimeInterval = ( DateTimeInterval ) theEObject;
            T result = caseDateTimeInterval( dateTimeInterval );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TIME_INTERVAL: {
            TimeInterval timeInterval = ( TimeInterval ) theEObject;
            T result = caseTimeInterval( timeInterval );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DECIMAL_QUANTITY: {
            DecimalQuantity decimalQuantity = ( DecimalQuantity ) theEObject;
            T result = caseDecimalQuantity( decimalQuantity );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STRING_QUANTITY: {
            StringQuantity stringQuantity = ( StringQuantity ) theEObject;
            T result = caseStringQuantity( stringQuantity );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MONTH_DAY_INTERVAL: {
            MonthDayInterval monthDayInterval = ( MonthDayInterval ) theEObject;
            T result = caseMonthDayInterval( monthDayInterval );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DATE_INTERVAL: {
            DateInterval dateInterval = ( DateInterval ) theEObject;
            T result = caseDateInterval( dateInterval );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.INTEGER_QUANTITY: {
            IntegerQuantity integerQuantity = ( IntegerQuantity ) theEObject;
            T result = caseIntegerQuantity( integerQuantity );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LOAD_RESPONSE_CHARACTERISTIC: {
            LoadResponseCharacteristic loadResponseCharacteristic = ( LoadResponseCharacteristic ) theEObject;
            T result = caseLoadResponseCharacteristic( loadResponseCharacteristic );
            if( result == null ) result = caseIdentifiedObject( loadResponseCharacteristic );
            if( result == null ) result = caseCimObjectWithID( loadResponseCharacteristic );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GEN_UNIT_OP_COST_CURVE: {
            GenUnitOpCostCurve genUnitOpCostCurve = ( GenUnitOpCostCurve ) theEObject;
            T result = caseGenUnitOpCostCurve( genUnitOpCostCurve );
            if( result == null ) result = caseCurve( genUnitOpCostCurve );
            if( result == null ) result = caseIdentifiedObject( genUnitOpCostCurve );
            if( result == null ) result = caseCimObjectWithID( genUnitOpCostCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POTENTIAL_TRANSFORMER: {
            PotentialTransformer potentialTransformer = ( PotentialTransformer ) theEObject;
            T result = casePotentialTransformer( potentialTransformer );
            if( result == null ) result = caseSensor( potentialTransformer );
            if( result == null ) result = caseAuxiliaryEquipment( potentialTransformer );
            if( result == null ) result = caseEquipment( potentialTransformer );
            if( result == null ) result = casePowerSystemResource( potentialTransformer );
            if( result == null ) result = caseIdentifiedObject( potentialTransformer );
            if( result == null ) result = caseCimObjectWithID( potentialTransformer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_CONDUCTING_EQUIPMENT: {
            DCConductingEquipment dcConductingEquipment = ( DCConductingEquipment ) theEObject;
            T result = caseDCConductingEquipment( dcConductingEquipment );
            if( result == null ) result = caseEquipment( dcConductingEquipment );
            if( result == null ) result = casePowerSystemResource( dcConductingEquipment );
            if( result == null ) result = caseIdentifiedObject( dcConductingEquipment );
            if( result == null ) result = caseCimObjectWithID( dcConductingEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RECLOSE_SEQUENCE: {
            RecloseSequence recloseSequence = ( RecloseSequence ) theEObject;
            T result = caseRecloseSequence( recloseSequence );
            if( result == null ) result = caseIdentifiedObject( recloseSequence );
            if( result == null ) result = caseCimObjectWithID( recloseSequence );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RATIO_TAP_CHANGER: {
            RatioTapChanger ratioTapChanger = ( RatioTapChanger ) theEObject;
            T result = caseRatioTapChanger( ratioTapChanger );
            if( result == null ) result = caseTapChanger( ratioTapChanger );
            if( result == null ) result = casePowerSystemResource( ratioTapChanger );
            if( result == null ) result = caseIdentifiedObject( ratioTapChanger );
            if( result == null ) result = caseCimObjectWithID( ratioTapChanger );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_TABLE_POINT: {
            PhaseTapChangerTablePoint phaseTapChangerTablePoint = ( PhaseTapChangerTablePoint ) theEObject;
            T result = casePhaseTapChangerTablePoint( phaseTapChangerTablePoint );
            if( result == null ) result = caseTapChangerTablePoint( phaseTapChangerTablePoint );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerTablePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TIE_FLOW: {
            TieFlow tieFlow = ( TieFlow ) theEObject;
            T result = caseTieFlow( tieFlow );
            if( result == null ) result = caseCimObjectWithID( tieFlow );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VOLTAGE_CONTROL_ZONE: {
            VoltageControlZone voltageControlZone = ( VoltageControlZone ) theEObject;
            T result = caseVoltageControlZone( voltageControlZone );
            if( result == null ) result = casePowerSystemResource( voltageControlZone );
            if( result == null ) result = caseIdentifiedObject( voltageControlZone );
            if( result == null ) result = caseCimObjectWithID( voltageControlZone );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ANALOG: {
            Analog analog = ( Analog ) theEObject;
            T result = caseAnalog( analog );
            if( result == null ) result = caseMeasurement( analog );
            if( result == null ) result = caseIdentifiedObject( analog );
            if( result == null ) result = caseCimObjectWithID( analog );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.JUNCTION: {
            Junction junction = ( Junction ) theEObject;
            T result = caseJunction( junction );
            if( result == null ) result = caseConnector( junction );
            if( result == null ) result = caseConductingEquipment( junction );
            if( result == null ) result = caseEquipment( junction );
            if( result == null ) result = casePowerSystemResource( junction );
            if( result == null ) result = caseIdentifiedObject( junction );
            if( result == null ) result = caseCimObjectWithID( junction );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTROL_AREA: {
            ControlArea controlArea = ( ControlArea ) theEObject;
            T result = caseControlArea( controlArea );
            if( result == null ) result = casePowerSystemResource( controlArea );
            if( result == null ) result = caseIdentifiedObject( controlArea );
            if( result == null ) result = caseCimObjectWithID( controlArea );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VALUE_TO_ALIAS: {
            ValueToAlias valueToAlias = ( ValueToAlias ) theEObject;
            T result = caseValueToAlias( valueToAlias );
            if( result == null ) result = caseIdentifiedObject( valueToAlias );
            if( result == null ) result = caseCimObjectWithID( valueToAlias );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_TURBINE: {
            HydroTurbine hydroTurbine = ( HydroTurbine ) theEObject;
            T result = caseHydroTurbine( hydroTurbine );
            if( result == null ) result = casePrimeMover( hydroTurbine );
            if( result == null ) result = casePowerSystemResource( hydroTurbine );
            if( result == null ) result = caseIdentifiedObject( hydroTurbine );
            if( result == null ) result = caseCimObjectWithID( hydroTurbine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.WAVE_TRAP: {
            WaveTrap waveTrap = ( WaveTrap ) theEObject;
            T result = caseWaveTrap( waveTrap );
            if( result == null ) result = caseAuxiliaryEquipment( waveTrap );
            if( result == null ) result = caseEquipment( waveTrap );
            if( result == null ) result = casePowerSystemResource( waveTrap );
            if( result == null ) result = caseIdentifiedObject( waveTrap );
            if( result == null ) result = caseCimObjectWithID( waveTrap );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ALT_TIE_MEAS: {
            AltTieMeas altTieMeas = ( AltTieMeas ) theEObject;
            T result = caseAltTieMeas( altTieMeas );
            if( result == null ) result = caseCimObjectWithID( altTieMeas );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PSR_TYPE: {
            PSRType psrType = ( PSRType ) theEObject;
            T result = casePSRType( psrType );
            if( result == null ) result = caseIdentifiedObject( psrType );
            if( result == null ) result = caseCimObjectWithID( psrType );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REMOTE_SOURCE: {
            RemoteSource remoteSource = ( RemoteSource ) theEObject;
            T result = caseRemoteSource( remoteSource );
            if( result == null ) result = caseRemotePoint( remoteSource );
            if( result == null ) result = caseIdentifiedObject( remoteSource );
            if( result == null ) result = caseCimObjectWithID( remoteSource );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIVALENT_INJECTION: {
            EquivalentInjection equivalentInjection = ( EquivalentInjection ) theEObject;
            T result = caseEquivalentInjection( equivalentInjection );
            if( result == null ) result = caseEquivalentEquipment( equivalentInjection );
            if( result == null ) result = caseConductingEquipment( equivalentInjection );
            if( result == null ) result = caseEquipment( equivalentInjection );
            if( result == null ) result = casePowerSystemResource( equivalentInjection );
            if( result == null ) result = caseIdentifiedObject( equivalentInjection );
            if( result == null ) result = caseCimObjectWithID( equivalentInjection );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_TAP_STEP: {
            SvTapStep svTapStep = ( SvTapStep ) theEObject;
            T result = caseSvTapStep( svTapStep );
            if( result == null ) result = caseStateVariable( svTapStep );
            if( result == null ) result = caseCimObjectWithID( svTapStep );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PROTECTION_EQUIPMENT: {
            ProtectionEquipment protectionEquipment = ( ProtectionEquipment ) theEObject;
            T result = caseProtectionEquipment( protectionEquipment );
            if( result == null ) result = caseEquipment( protectionEquipment );
            if( result == null ) result = casePowerSystemResource( protectionEquipment );
            if( result == null ) result = caseIdentifiedObject( protectionEquipment );
            if( result == null ) result = caseCimObjectWithID( protectionEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MEASUREMENT: {
            Measurement measurement = ( Measurement ) theEObject;
            T result = caseMeasurement( measurement );
            if( result == null ) result = caseIdentifiedObject( measurement );
            if( result == null ) result = caseCimObjectWithID( measurement );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_SYSTEM_RESOURCE: {
            PowerSystemResource powerSystemResource = ( PowerSystemResource ) theEObject;
            T result = casePowerSystemResource( powerSystemResource );
            if( result == null ) result = caseIdentifiedObject( powerSystemResource );
            if( result == null ) result = caseCimObjectWithID( powerSystemResource );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BWR_STEAM_SUPPLY: {
            BWRSteamSupply bwrSteamSupply = ( BWRSteamSupply ) theEObject;
            T result = caseBWRSteamSupply( bwrSteamSupply );
            if( result == null ) result = caseSteamSupply( bwrSteamSupply );
            if( result == null ) result = casePowerSystemResource( bwrSteamSupply );
            if( result == null ) result = caseIdentifiedObject( bwrSteamSupply );
            if( result == null ) result = caseCimObjectWithID( bwrSteamSupply );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_BUSBAR: {
            DCBusbar dcBusbar = ( DCBusbar ) theEObject;
            T result = caseDCBusbar( dcBusbar );
            if( result == null ) result = caseDCConductingEquipment( dcBusbar );
            if( result == null ) result = caseEquipment( dcBusbar );
            if( result == null ) result = casePowerSystemResource( dcBusbar );
            if( result == null ) result = caseIdentifiedObject( dcBusbar );
            if( result == null ) result = caseCimObjectWithID( dcBusbar );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DISCRETE: {
            Discrete discrete = ( Discrete ) theEObject;
            T result = caseDiscrete( discrete );
            if( result == null ) result = caseMeasurement( discrete );
            if( result == null ) result = caseIdentifiedObject( discrete );
            if( result == null ) result = caseCimObjectWithID( discrete );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONNECTIVITY_NODE_CONTAINER: {
            ConnectivityNodeContainer connectivityNodeContainer = ( ConnectivityNodeContainer ) theEObject;
            T result = caseConnectivityNodeContainer( connectivityNodeContainer );
            if( result == null ) result = casePowerSystemResource( connectivityNodeContainer );
            if( result == null ) result = caseIdentifiedObject( connectivityNodeContainer );
            if( result == null ) result = caseCimObjectWithID( connectivityNodeContainer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_ASYMMETRICAL: {
            PhaseTapChangerAsymmetrical phaseTapChangerAsymmetrical = ( PhaseTapChangerAsymmetrical ) theEObject;
            T result = casePhaseTapChangerAsymmetrical( phaseTapChangerAsymmetrical );
            if( result == null ) result = casePhaseTapChangerNonLinear( phaseTapChangerAsymmetrical );
            if( result == null ) result = casePhaseTapChanger( phaseTapChangerAsymmetrical );
            if( result == null ) result = caseTapChanger( phaseTapChangerAsymmetrical );
            if( result == null ) result = casePowerSystemResource( phaseTapChangerAsymmetrical );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerAsymmetrical );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerAsymmetrical );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_NON_LINEAR: {
            PhaseTapChangerNonLinear phaseTapChangerNonLinear = ( PhaseTapChangerNonLinear ) theEObject;
            T result = casePhaseTapChangerNonLinear( phaseTapChangerNonLinear );
            if( result == null ) result = casePhaseTapChanger( phaseTapChangerNonLinear );
            if( result == null ) result = caseTapChanger( phaseTapChangerNonLinear );
            if( result == null ) result = casePowerSystemResource( phaseTapChangerNonLinear );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerNonLinear );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerNonLinear );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PRIME_MOVER: {
            PrimeMover primeMover = ( PrimeMover ) theEObject;
            T result = casePrimeMover( primeMover );
            if( result == null ) result = casePowerSystemResource( primeMover );
            if( result == null ) result = caseIdentifiedObject( primeMover );
            if( result == null ) result = caseCimObjectWithID( primeMover );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NON_CONFORM_LOAD_SCHEDULE: {
            NonConformLoadSchedule nonConformLoadSchedule = ( NonConformLoadSchedule ) theEObject;
            T result = caseNonConformLoadSchedule( nonConformLoadSchedule );
            if( result == null ) result = caseSeasonDayTypeSchedule( nonConformLoadSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( nonConformLoadSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( nonConformLoadSchedule );
            if( result == null ) result = caseIdentifiedObject( nonConformLoadSchedule );
            if( result == null ) result = caseCimObjectWithID( nonConformLoadSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_LINE: {
            DCLine dcLine = ( DCLine ) theEObject;
            T result = caseDCLine( dcLine );
            if( result == null ) result = caseDCEquipmentContainer( dcLine );
            if( result == null ) result = caseEquipmentContainer( dcLine );
            if( result == null ) result = caseConnectivityNodeContainer( dcLine );
            if( result == null ) result = casePowerSystemResource( dcLine );
            if( result == null ) result = caseIdentifiedObject( dcLine );
            if( result == null ) result = caseCimObjectWithID( dcLine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CURRENT_TRANSFORMER: {
            CurrentTransformer currentTransformer = ( CurrentTransformer ) theEObject;
            T result = caseCurrentTransformer( currentTransformer );
            if( result == null ) result = caseSensor( currentTransformer );
            if( result == null ) result = caseAuxiliaryEquipment( currentTransformer );
            if( result == null ) result = caseEquipment( currentTransformer );
            if( result == null ) result = casePowerSystemResource( currentTransformer );
            if( result == null ) result = caseIdentifiedObject( currentTransformer );
            if( result == null ) result = caseCimObjectWithID( currentTransformer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REMOTE_UNIT: {
            RemoteUnit remoteUnit = ( RemoteUnit ) theEObject;
            T result = caseRemoteUnit( remoteUnit );
            if( result == null ) result = casePowerSystemResource( remoteUnit );
            if( result == null ) result = caseIdentifiedObject( remoteUnit );
            if( result == null ) result = caseCimObjectWithID( remoteUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_GENERATING_UNIT: {
            HydroGeneratingUnit hydroGeneratingUnit = ( HydroGeneratingUnit ) theEObject;
            T result = caseHydroGeneratingUnit( hydroGeneratingUnit );
            if( result == null ) result = caseGeneratingUnit( hydroGeneratingUnit );
            if( result == null ) result = caseEquipment( hydroGeneratingUnit );
            if( result == null ) result = casePowerSystemResource( hydroGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( hydroGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( hydroGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BRANCH_GROUP: {
            BranchGroup branchGroup = ( BranchGroup ) theEObject;
            T result = caseBranchGroup( branchGroup );
            if( result == null ) result = caseIdentifiedObject( branchGroup );
            if( result == null ) result = caseCimObjectWithID( branchGroup );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REMOTE_POINT: {
            RemotePoint remotePoint = ( RemotePoint ) theEObject;
            T result = caseRemotePoint( remotePoint );
            if( result == null ) result = caseIdentifiedObject( remotePoint );
            if( result == null ) result = caseCimObjectWithID( remotePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_ELECTRONICS_WIND_UNIT: {
            PowerElectronicsWindUnit powerElectronicsWindUnit = ( PowerElectronicsWindUnit ) theEObject;
            T result = casePowerElectronicsWindUnit( powerElectronicsWindUnit );
            if( result == null ) result = casePowerElectronicsUnit( powerElectronicsWindUnit );
            if( result == null ) result = caseEquipment( powerElectronicsWindUnit );
            if( result == null ) result = casePowerSystemResource( powerElectronicsWindUnit );
            if( result == null ) result = caseIdentifiedObject( powerElectronicsWindUnit );
            if( result == null ) result = caseCimObjectWithID( powerElectronicsWindUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BAY: {
            Bay bay = ( Bay ) theEObject;
            T result = caseBay( bay );
            if( result == null ) result = caseEquipmentContainer( bay );
            if( result == null ) result = caseConnectivityNodeContainer( bay );
            if( result == null ) result = casePowerSystemResource( bay );
            if( result == null ) result = caseIdentifiedObject( bay );
            if( result == null ) result = caseCimObjectWithID( bay );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NAME: {
            Name name = ( Name ) theEObject;
            T result = caseName( name );
            if( result == null ) result = caseCimObjectWithID( name );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LOAD_BREAK_SWITCH: {
            LoadBreakSwitch loadBreakSwitch = ( LoadBreakSwitch ) theEObject;
            T result = caseLoadBreakSwitch( loadBreakSwitch );
            if( result == null ) result = caseProtectedSwitch( loadBreakSwitch );
            if( result == null ) result = caseSwitch( loadBreakSwitch );
            if( result == null ) result = caseConductingEquipment( loadBreakSwitch );
            if( result == null ) result = caseEquipment( loadBreakSwitch );
            if( result == null ) result = casePowerSystemResource( loadBreakSwitch );
            if( result == null ) result = caseIdentifiedObject( loadBreakSwitch );
            if( result == null ) result = caseCimObjectWithID( loadBreakSwitch );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BASE_FREQUENCY: {
            BaseFrequency baseFrequency = ( BaseFrequency ) theEObject;
            T result = caseBaseFrequency( baseFrequency );
            if( result == null ) result = caseIdentifiedObject( baseFrequency );
            if( result == null ) result = caseCimObjectWithID( baseFrequency );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_CORE_ADMITTANCE: {
            TransformerCoreAdmittance transformerCoreAdmittance = ( TransformerCoreAdmittance ) theEObject;
            T result = caseTransformerCoreAdmittance( transformerCoreAdmittance );
            if( result == null ) result = caseIdentifiedObject( transformerCoreAdmittance );
            if( result == null ) result = caseCimObjectWithID( transformerCoreAdmittance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_SERIES_DEVICE: {
            DCSeriesDevice dcSeriesDevice = ( DCSeriesDevice ) theEObject;
            T result = caseDCSeriesDevice( dcSeriesDevice );
            if( result == null ) result = caseDCConductingEquipment( dcSeriesDevice );
            if( result == null ) result = caseEquipment( dcSeriesDevice );
            if( result == null ) result = casePowerSystemResource( dcSeriesDevice );
            if( result == null ) result = caseIdentifiedObject( dcSeriesDevice );
            if( result == null ) result = caseCimObjectWithID( dcSeriesDevice );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VOLTAGE_LEVEL: {
            VoltageLevel voltageLevel = ( VoltageLevel ) theEObject;
            T result = caseVoltageLevel( voltageLevel );
            if( result == null ) result = caseEquipmentContainer( voltageLevel );
            if( result == null ) result = caseConnectivityNodeContainer( voltageLevel );
            if( result == null ) result = casePowerSystemResource( voltageLevel );
            if( result == null ) result = caseIdentifiedObject( voltageLevel );
            if( result == null ) result = caseCimObjectWithID( voltageLevel );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_GROUND: {
            DCGround dcGround = ( DCGround ) theEObject;
            T result = caseDCGround( dcGround );
            if( result == null ) result = caseDCConductingEquipment( dcGround );
            if( result == null ) result = caseEquipment( dcGround );
            if( result == null ) result = casePowerSystemResource( dcGround );
            if( result == null ) result = caseIdentifiedObject( dcGround );
            if( result == null ) result = caseCimObjectWithID( dcGround );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SUB_GEOGRAPHICAL_REGION: {
            SubGeographicalRegion subGeographicalRegion = ( SubGeographicalRegion ) theEObject;
            T result = caseSubGeographicalRegion( subGeographicalRegion );
            if( result == null ) result = caseIdentifiedObject( subGeographicalRegion );
            if( result == null ) result = caseCimObjectWithID( subGeographicalRegion );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COGENERATION_PLANT: {
            CogenerationPlant cogenerationPlant = ( CogenerationPlant ) theEObject;
            T result = caseCogenerationPlant( cogenerationPlant );
            if( result == null ) result = casePowerSystemResource( cogenerationPlant );
            if( result == null ) result = caseIdentifiedObject( cogenerationPlant );
            if( result == null ) result = caseCimObjectWithID( cogenerationPlant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FAULT_CAUSE_TYPE: {
            FaultCauseType faultCauseType = ( FaultCauseType ) theEObject;
            T result = caseFaultCauseType( faultCauseType );
            if( result == null ) result = caseIdentifiedObject( faultCauseType );
            if( result == null ) result = caseCimObjectWithID( faultCauseType );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_LINE_SEGMENT: {
            DCLineSegment dcLineSegment = ( DCLineSegment ) theEObject;
            T result = caseDCLineSegment( dcLineSegment );
            if( result == null ) result = caseDCConductingEquipment( dcLineSegment );
            if( result == null ) result = caseEquipment( dcLineSegment );
            if( result == null ) result = casePowerSystemResource( dcLineSegment );
            if( result == null ) result = caseIdentifiedObject( dcLineSegment );
            if( result == null ) result = caseCimObjectWithID( dcLineSegment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIPMENT_CONTAINER: {
            EquipmentContainer equipmentContainer = ( EquipmentContainer ) theEObject;
            T result = caseEquipmentContainer( equipmentContainer );
            if( result == null ) result = caseConnectivityNodeContainer( equipmentContainer );
            if( result == null ) result = casePowerSystemResource( equipmentContainer );
            if( result == null ) result = caseIdentifiedObject( equipmentContainer );
            if( result == null ) result = caseCimObjectWithID( equipmentContainer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONDUCTOR: {
            Conductor conductor = ( Conductor ) theEObject;
            T result = caseConductor( conductor );
            if( result == null ) result = caseConductingEquipment( conductor );
            if( result == null ) result = caseEquipment( conductor );
            if( result == null ) result = casePowerSystemResource( conductor );
            if( result == null ) result = caseIdentifiedObject( conductor );
            if( result == null ) result = caseCimObjectWithID( conductor );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.WIND_GENERATING_UNIT: {
            WindGeneratingUnit windGeneratingUnit = ( WindGeneratingUnit ) theEObject;
            T result = caseWindGeneratingUnit( windGeneratingUnit );
            if( result == null ) result = caseGeneratingUnit( windGeneratingUnit );
            if( result == null ) result = caseEquipment( windGeneratingUnit );
            if( result == null ) result = casePowerSystemResource( windGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( windGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( windGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PENSTOCK_LOSS_CURVE: {
            PenstockLossCurve penstockLossCurve = ( PenstockLossCurve ) theEObject;
            T result = casePenstockLossCurve( penstockLossCurve );
            if( result == null ) result = caseCurve( penstockLossCurve );
            if( result == null ) result = caseIdentifiedObject( penstockLossCurve );
            if( result == null ) result = caseCimObjectWithID( penstockLossCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VISIBILITY_LAYER: {
            VisibilityLayer visibilityLayer = ( VisibilityLayer ) theEObject;
            T result = caseVisibilityLayer( visibilityLayer );
            if( result == null ) result = caseIdentifiedObject( visibilityLayer );
            if( result == null ) result = caseCimObjectWithID( visibilityLayer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_TOPOLOGICAL_ISLAND: {
            DCTopologicalIsland dcTopologicalIsland = ( DCTopologicalIsland ) theEObject;
            T result = caseDCTopologicalIsland( dcTopologicalIsland );
            if( result == null ) result = caseIdentifiedObject( dcTopologicalIsland );
            if( result == null ) result = caseCimObjectWithID( dcTopologicalIsland );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COMBINED_CYCLE_PLANT: {
            CombinedCyclePlant combinedCyclePlant = ( CombinedCyclePlant ) theEObject;
            T result = caseCombinedCyclePlant( combinedCyclePlant );
            if( result == null ) result = casePowerSystemResource( combinedCyclePlant );
            if( result == null ) result = caseIdentifiedObject( combinedCyclePlant );
            if( result == null ) result = caseCimObjectWithID( combinedCyclePlant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COMPOSITE_SWITCH: {
            CompositeSwitch compositeSwitch = ( CompositeSwitch ) theEObject;
            T result = caseCompositeSwitch( compositeSwitch );
            if( result == null ) result = caseEquipment( compositeSwitch );
            if( result == null ) result = casePowerSystemResource( compositeSwitch );
            if( result == null ) result = caseIdentifiedObject( compositeSwitch );
            if( result == null ) result = caseCimObjectWithID( compositeSwitch );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.AUXILIARY_EQUIPMENT: {
            AuxiliaryEquipment auxiliaryEquipment = ( AuxiliaryEquipment ) theEObject;
            T result = caseAuxiliaryEquipment( auxiliaryEquipment );
            if( result == null ) result = caseEquipment( auxiliaryEquipment );
            if( result == null ) result = casePowerSystemResource( auxiliaryEquipment );
            if( result == null ) result = caseIdentifiedObject( auxiliaryEquipment );
            if( result == null ) result = caseCimObjectWithID( auxiliaryEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LIMIT_SET: {
            LimitSet limitSet = ( LimitSet ) theEObject;
            T result = caseLimitSet( limitSet );
            if( result == null ) result = caseIdentifiedObject( limitSet );
            if( result == null ) result = caseCimObjectWithID( limitSet );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NON_CONFORM_LOAD: {
            NonConformLoad nonConformLoad = ( NonConformLoad ) theEObject;
            T result = caseNonConformLoad( nonConformLoad );
            if( result == null ) result = caseEnergyConsumer( nonConformLoad );
            if( result == null ) result = caseConductingEquipment( nonConformLoad );
            if( result == null ) result = caseEquipment( nonConformLoad );
            if( result == null ) result = casePowerSystemResource( nonConformLoad );
            if( result == null ) result = caseIdentifiedObject( nonConformLoad );
            if( result == null ) result = caseCimObjectWithID( nonConformLoad );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIPMENT: {
            Equipment equipment = ( Equipment ) theEObject;
            T result = caseEquipment( equipment );
            if( result == null ) result = casePowerSystemResource( equipment );
            if( result == null ) result = caseIdentifiedObject( equipment );
            if( result == null ) result = caseCimObjectWithID( equipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.QUALITY61850: {
            Quality61850 quality61850 = ( Quality61850 ) theEObject;
            T result = caseQuality61850( quality61850 );
            if( result == null ) result = caseCimObjectWithID( quality61850 );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_TANK_END: {
            TransformerTankEnd transformerTankEnd = ( TransformerTankEnd ) theEObject;
            T result = caseTransformerTankEnd( transformerTankEnd );
            if( result == null ) result = caseTransformerEnd( transformerTankEnd );
            if( result == null ) result = caseIdentifiedObject( transformerTankEnd );
            if( result == null ) result = caseCimObjectWithID( transformerTankEnd );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIVALENT_EQUIPMENT: {
            EquivalentEquipment equivalentEquipment = ( EquivalentEquipment ) theEObject;
            T result = caseEquivalentEquipment( equivalentEquipment );
            if( result == null ) result = caseConductingEquipment( equivalentEquipment );
            if( result == null ) result = caseEquipment( equivalentEquipment );
            if( result == null ) result = casePowerSystemResource( equivalentEquipment );
            if( result == null ) result = caseIdentifiedObject( equivalentEquipment );
            if( result == null ) result = caseCimObjectWithID( equivalentEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TAP_CHANGER_CONTROL: {
            TapChangerControl tapChangerControl = ( TapChangerControl ) theEObject;
            T result = caseTapChangerControl( tapChangerControl );
            if( result == null ) result = caseRegulatingControl( tapChangerControl );
            if( result == null ) result = casePowerSystemResource( tapChangerControl );
            if( result == null ) result = caseIdentifiedObject( tapChangerControl );
            if( result == null ) result = caseCimObjectWithID( tapChangerControl );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REMOTE_CONTROL: {
            RemoteControl remoteControl = ( RemoteControl ) theEObject;
            T result = caseRemoteControl( remoteControl );
            if( result == null ) result = caseRemotePoint( remoteControl );
            if( result == null ) result = caseIdentifiedObject( remoteControl );
            if( result == null ) result = caseCimObjectWithID( remoteControl );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONDUCTING_EQUIPMENT: {
            ConductingEquipment conductingEquipment = ( ConductingEquipment ) theEObject;
            T result = caseConductingEquipment( conductingEquipment );
            if( result == null ) result = caseEquipment( conductingEquipment );
            if( result == null ) result = casePowerSystemResource( conductingEquipment );
            if( result == null ) result = caseIdentifiedObject( conductingEquipment );
            if( result == null ) result = caseCimObjectWithID( conductingEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CT_TEMP_ACTIVE_POWER_CURVE: {
            CTTempActivePowerCurve ctTempActivePowerCurve = ( CTTempActivePowerCurve ) theEObject;
            T result = caseCTTempActivePowerCurve( ctTempActivePowerCurve );
            if( result == null ) result = caseCurve( ctTempActivePowerCurve );
            if( result == null ) result = caseIdentifiedObject( ctTempActivePowerCurve );
            if( result == null ) result = caseCimObjectWithID( ctTempActivePowerCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.IRREGULAR_INTERVAL_SCHEDULE: {
            IrregularIntervalSchedule irregularIntervalSchedule = ( IrregularIntervalSchedule ) theEObject;
            T result = caseIrregularIntervalSchedule( irregularIntervalSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( irregularIntervalSchedule );
            if( result == null ) result = caseIdentifiedObject( irregularIntervalSchedule );
            if( result == null ) result = caseCimObjectWithID( irregularIntervalSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ANALOG_CONTROL: {
            AnalogControl analogControl = ( AnalogControl ) theEObject;
            T result = caseAnalogControl( analogControl );
            if( result == null ) result = caseControl( analogControl );
            if( result == null ) result = caseIdentifiedObject( analogControl );
            if( result == null ) result = caseCimObjectWithID( analogControl );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CURRENT_LIMIT: {
            CurrentLimit currentLimit = ( CurrentLimit ) theEObject;
            T result = caseCurrentLimit( currentLimit );
            if( result == null ) result = caseOperationalLimit( currentLimit );
            if( result == null ) result = caseIdentifiedObject( currentLimit );
            if( result == null ) result = caseCimObjectWithID( currentLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER: {
            PhaseTapChanger phaseTapChanger = ( PhaseTapChanger ) theEObject;
            T result = casePhaseTapChanger( phaseTapChanger );
            if( result == null ) result = caseTapChanger( phaseTapChanger );
            if( result == null ) result = casePowerSystemResource( phaseTapChanger );
            if( result == null ) result = caseIdentifiedObject( phaseTapChanger );
            if( result == null ) result = caseCimObjectWithID( phaseTapChanger );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.AIR_COMPRESSOR: {
            AirCompressor airCompressor = ( AirCompressor ) theEObject;
            T result = caseAirCompressor( airCompressor );
            if( result == null ) result = casePowerSystemResource( airCompressor );
            if( result == null ) result = caseIdentifiedObject( airCompressor );
            if( result == null ) result = caseCimObjectWithID( airCompressor );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_INJECTION: {
            SvInjection svInjection = ( SvInjection ) theEObject;
            T result = caseSvInjection( svInjection );
            if( result == null ) result = caseStateVariable( svInjection );
            if( result == null ) result = caseCimObjectWithID( svInjection );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SET_POINT: {
            SetPoint setPoint = ( SetPoint ) theEObject;
            T result = caseSetPoint( setPoint );
            if( result == null ) result = caseAnalogControl( setPoint );
            if( result == null ) result = caseControl( setPoint );
            if( result == null ) result = caseIdentifiedObject( setPoint );
            if( result == null ) result = caseCimObjectWithID( setPoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REGULATING_CONTROL: {
            RegulatingControl regulatingControl = ( RegulatingControl ) theEObject;
            T result = caseRegulatingControl( regulatingControl );
            if( result == null ) result = casePowerSystemResource( regulatingControl );
            if( result == null ) result = caseIdentifiedObject( regulatingControl );
            if( result == null ) result = caseCimObjectWithID( regulatingControl );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACCUMULATOR_LIMIT: {
            AccumulatorLimit accumulatorLimit = ( AccumulatorLimit ) theEObject;
            T result = caseAccumulatorLimit( accumulatorLimit );
            if( result == null ) result = caseLimit( accumulatorLimit );
            if( result == null ) result = caseIdentifiedObject( accumulatorLimit );
            if( result == null ) result = caseCimObjectWithID( accumulatorLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FUEL_ALLOCATION_SCHEDULE: {
            FuelAllocationSchedule fuelAllocationSchedule = ( FuelAllocationSchedule ) theEObject;
            T result = caseFuelAllocationSchedule( fuelAllocationSchedule );
            if( result == null ) result = caseCurve( fuelAllocationSchedule );
            if( result == null ) result = caseIdentifiedObject( fuelAllocationSchedule );
            if( result == null ) result = caseCimObjectWithID( fuelAllocationSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SYNCHRONOUS_MACHINE: {
            SynchronousMachine synchronousMachine = ( SynchronousMachine ) theEObject;
            T result = caseSynchronousMachine( synchronousMachine );
            if( result == null ) result = caseRotatingMachine( synchronousMachine );
            if( result == null ) result = caseRegulatingCondEq( synchronousMachine );
            if( result == null ) result = caseConductingEquipment( synchronousMachine );
            if( result == null ) result = caseEquipment( synchronousMachine );
            if( result == null ) result = casePowerSystemResource( synchronousMachine );
            if( result == null ) result = caseIdentifiedObject( synchronousMachine );
            if( result == null ) result = caseCimObjectWithID( synchronousMachine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SECTIONALISER: {
            Sectionaliser sectionaliser = ( Sectionaliser ) theEObject;
            T result = caseSectionaliser( sectionaliser );
            if( result == null ) result = caseSwitch( sectionaliser );
            if( result == null ) result = caseConductingEquipment( sectionaliser );
            if( result == null ) result = caseEquipment( sectionaliser );
            if( result == null ) result = casePowerSystemResource( sectionaliser );
            if( result == null ) result = caseIdentifiedObject( sectionaliser );
            if( result == null ) result = caseCimObjectWithID( sectionaliser );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_BASE_TERMINAL: {
            DCBaseTerminal dcBaseTerminal = ( DCBaseTerminal ) theEObject;
            T result = caseDCBaseTerminal( dcBaseTerminal );
            if( result == null ) result = caseACDCTerminal( dcBaseTerminal );
            if( result == null ) result = caseIdentifiedObject( dcBaseTerminal );
            if( result == null ) result = caseCimObjectWithID( dcBaseTerminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REPORTING_SUPER_GROUP: {
            ReportingSuperGroup reportingSuperGroup = ( ReportingSuperGroup ) theEObject;
            T result = caseReportingSuperGroup( reportingSuperGroup );
            if( result == null ) result = caseIdentifiedObject( reportingSuperGroup );
            if( result == null ) result = caseCimObjectWithID( reportingSuperGroup );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REGULATING_COND_EQ: {
            RegulatingCondEq regulatingCondEq = ( RegulatingCondEq ) theEObject;
            T result = caseRegulatingCondEq( regulatingCondEq );
            if( result == null ) result = caseConductingEquipment( regulatingCondEq );
            if( result == null ) result = caseEquipment( regulatingCondEq );
            if( result == null ) result = casePowerSystemResource( regulatingCondEq );
            if( result == null ) result = caseIdentifiedObject( regulatingCondEq );
            if( result == null ) result = caseCimObjectWithID( regulatingCondEq );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STATE_VARIABLE: {
            StateVariable stateVariable = ( StateVariable ) theEObject;
            T result = caseStateVariable( stateVariable );
            if( result == null ) result = caseCimObjectWithID( stateVariable );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_ELECTRONICS_CONNECTION: {
            PowerElectronicsConnection powerElectronicsConnection = ( PowerElectronicsConnection ) theEObject;
            T result = casePowerElectronicsConnection( powerElectronicsConnection );
            if( result == null ) result = caseRegulatingCondEq( powerElectronicsConnection );
            if( result == null ) result = caseConductingEquipment( powerElectronicsConnection );
            if( result == null ) result = caseEquipment( powerElectronicsConnection );
            if( result == null ) result = casePowerSystemResource( powerElectronicsConnection );
            if( result == null ) result = caseIdentifiedObject( powerElectronicsConnection );
            if( result == null ) result = caseCimObjectWithID( powerElectronicsConnection );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PER_LENGTH_SEQUENCE_IMPEDANCE: {
            PerLengthSequenceImpedance perLengthSequenceImpedance = ( PerLengthSequenceImpedance ) theEObject;
            T result = casePerLengthSequenceImpedance( perLengthSequenceImpedance );
            if( result == null ) result = casePerLengthImpedance( perLengthSequenceImpedance );
            if( result == null ) result = casePerLengthLineParameter( perLengthSequenceImpedance );
            if( result == null ) result = caseIdentifiedObject( perLengthSequenceImpedance );
            if( result == null ) result = caseCimObjectWithID( perLengthSequenceImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COMMAND: {
            Command command = ( Command ) theEObject;
            T result = caseCommand( command );
            if( result == null ) result = caseControl( command );
            if( result == null ) result = caseIdentifiedObject( command );
            if( result == null ) result = caseCimObjectWithID( command );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FUSE: {
            Fuse fuse = ( Fuse ) theEObject;
            T result = caseFuse( fuse );
            if( result == null ) result = caseSwitch( fuse );
            if( result == null ) result = caseConductingEquipment( fuse );
            if( result == null ) result = caseEquipment( fuse );
            if( result == null ) result = casePowerSystemResource( fuse );
            if( result == null ) result = caseIdentifiedObject( fuse );
            if( result == null ) result = caseCimObjectWithID( fuse );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_CUT_ZONE: {
            PowerCutZone powerCutZone = ( PowerCutZone ) theEObject;
            T result = casePowerCutZone( powerCutZone );
            if( result == null ) result = casePowerSystemResource( powerCutZone );
            if( result == null ) result = caseIdentifiedObject( powerCutZone );
            if( result == null ) result = caseCimObjectWithID( powerCutZone );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NON_CONFORM_LOAD_GROUP: {
            NonConformLoadGroup nonConformLoadGroup = ( NonConformLoadGroup ) theEObject;
            T result = caseNonConformLoadGroup( nonConformLoadGroup );
            if( result == null ) result = caseLoadGroup( nonConformLoadGroup );
            if( result == null ) result = caseIdentifiedObject( nonConformLoadGroup );
            if( result == null ) result = caseCimObjectWithID( nonConformLoadGroup );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACCUMULATOR_RESET: {
            AccumulatorReset accumulatorReset = ( AccumulatorReset ) theEObject;
            T result = caseAccumulatorReset( accumulatorReset );
            if( result == null ) result = caseControl( accumulatorReset );
            if( result == null ) result = caseIdentifiedObject( accumulatorReset );
            if( result == null ) result = caseCimObjectWithID( accumulatorReset );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIPMENT_FAULT: {
            EquipmentFault equipmentFault = ( EquipmentFault ) theEObject;
            T result = caseEquipmentFault( equipmentFault );
            if( result == null ) result = caseFault( equipmentFault );
            if( result == null ) result = caseIdentifiedObject( equipmentFault );
            if( result == null ) result = caseCimObjectWithID( equipmentFault );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TAP_CHANGER: {
            TapChanger tapChanger = ( TapChanger ) theEObject;
            T result = caseTapChanger( tapChanger );
            if( result == null ) result = casePowerSystemResource( tapChanger );
            if( result == null ) result = caseIdentifiedObject( tapChanger );
            if( result == null ) result = caseCimObjectWithID( tapChanger );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REACTIVE_CAPABILITY_CURVE: {
            ReactiveCapabilityCurve reactiveCapabilityCurve = ( ReactiveCapabilityCurve ) theEObject;
            T result = caseReactiveCapabilityCurve( reactiveCapabilityCurve );
            if( result == null ) result = caseCurve( reactiveCapabilityCurve );
            if( result == null ) result = caseIdentifiedObject( reactiveCapabilityCurve );
            if( result == null ) result = caseCimObjectWithID( reactiveCapabilityCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACDC_CONVERTER: {
            ACDCConverter acdcConverter = ( ACDCConverter ) theEObject;
            T result = caseACDCConverter( acdcConverter );
            if( result == null ) result = caseConductingEquipment( acdcConverter );
            if( result == null ) result = caseEquipment( acdcConverter );
            if( result == null ) result = casePowerSystemResource( acdcConverter );
            if( result == null ) result = caseIdentifiedObject( acdcConverter );
            if( result == null ) result = caseCimObjectWithID( acdcConverter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NUCLEAR_GENERATING_UNIT: {
            NuclearGeneratingUnit nuclearGeneratingUnit = ( NuclearGeneratingUnit ) theEObject;
            T result = caseNuclearGeneratingUnit( nuclearGeneratingUnit );
            if( result == null ) result = caseGeneratingUnit( nuclearGeneratingUnit );
            if( result == null ) result = caseEquipment( nuclearGeneratingUnit );
            if( result == null ) result = casePowerSystemResource( nuclearGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( nuclearGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( nuclearGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTINGENCY_EQUIPMENT: {
            ContingencyEquipment contingencyEquipment = ( ContingencyEquipment ) theEObject;
            T result = caseContingencyEquipment( contingencyEquipment );
            if( result == null ) result = caseContingencyElement( contingencyEquipment );
            if( result == null ) result = caseIdentifiedObject( contingencyEquipment );
            if( result == null ) result = caseCimObjectWithID( contingencyEquipment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_STATUS: {
            SvStatus svStatus = ( SvStatus ) theEObject;
            T result = caseSvStatus( svStatus );
            if( result == null ) result = caseStateVariable( svStatus );
            if( result == null ) result = caseCimObjectWithID( svStatus );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_TABULAR: {
            PhaseTapChangerTabular phaseTapChangerTabular = ( PhaseTapChangerTabular ) theEObject;
            T result = casePhaseTapChangerTabular( phaseTapChangerTabular );
            if( result == null ) result = casePhaseTapChanger( phaseTapChangerTabular );
            if( result == null ) result = caseTapChanger( phaseTapChangerTabular );
            if( result == null ) result = casePowerSystemResource( phaseTapChangerTabular );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerTabular );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerTabular );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SENSOR: {
            Sensor sensor = ( Sensor ) theEObject;
            T result = caseSensor( sensor );
            if( result == null ) result = caseAuxiliaryEquipment( sensor );
            if( result == null ) result = caseEquipment( sensor );
            if( result == null ) result = casePowerSystemResource( sensor );
            if( result == null ) result = caseIdentifiedObject( sensor );
            if( result == null ) result = caseCimObjectWithID( sensor );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SHUNT_COMPENSATOR_PHASE: {
            ShuntCompensatorPhase shuntCompensatorPhase = ( ShuntCompensatorPhase ) theEObject;
            T result = caseShuntCompensatorPhase( shuntCompensatorPhase );
            if( result == null ) result = casePowerSystemResource( shuntCompensatorPhase );
            if( result == null ) result = caseIdentifiedObject( shuntCompensatorPhase );
            if( result == null ) result = caseCimObjectWithID( shuntCompensatorPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONFORM_LOAD_GROUP: {
            ConformLoadGroup conformLoadGroup = ( ConformLoadGroup ) theEObject;
            T result = caseConformLoadGroup( conformLoadGroup );
            if( result == null ) result = caseLoadGroup( conformLoadGroup );
            if( result == null ) result = caseIdentifiedObject( conformLoadGroup );
            if( result == null ) result = caseCimObjectWithID( conformLoadGroup );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.OPERATING_PARTICIPANT: {
            OperatingParticipant operatingParticipant = ( OperatingParticipant ) theEObject;
            T result = caseOperatingParticipant( operatingParticipant );
            if( result == null ) result = caseIdentifiedObject( operatingParticipant );
            if( result == null ) result = caseCimObjectWithID( operatingParticipant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACCUMULATOR_LIMIT_SET: {
            AccumulatorLimitSet accumulatorLimitSet = ( AccumulatorLimitSet ) theEObject;
            T result = caseAccumulatorLimitSet( accumulatorLimitSet );
            if( result == null ) result = caseLimitSet( accumulatorLimitSet );
            if( result == null ) result = caseIdentifiedObject( accumulatorLimitSet );
            if( result == null ) result = caseCimObjectWithID( accumulatorLimitSet );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_NODE: {
            DCNode dcNode = ( DCNode ) theEObject;
            T result = caseDCNode( dcNode );
            if( result == null ) result = caseIdentifiedObject( dcNode );
            if( result == null ) result = caseCimObjectWithID( dcNode );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_STAR_IMPEDANCE: {
            TransformerStarImpedance transformerStarImpedance = ( TransformerStarImpedance ) theEObject;
            T result = caseTransformerStarImpedance( transformerStarImpedance );
            if( result == null ) result = caseIdentifiedObject( transformerStarImpedance );
            if( result == null ) result = caseCimObjectWithID( transformerStarImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SUBCRITICAL: {
            Subcritical subcritical = ( Subcritical ) theEObject;
            T result = caseSubcritical( subcritical );
            if( result == null ) result = caseFossilSteamSupply( subcritical );
            if( result == null ) result = caseSteamSupply( subcritical );
            if( result == null ) result = casePowerSystemResource( subcritical );
            if( result == null ) result = caseIdentifiedObject( subcritical );
            if( result == null ) result = caseCimObjectWithID( subcritical );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACTIVE_POWER_LIMIT: {
            ActivePowerLimit activePowerLimit = ( ActivePowerLimit ) theEObject;
            T result = caseActivePowerLimit( activePowerLimit );
            if( result == null ) result = caseOperationalLimit( activePowerLimit );
            if( result == null ) result = caseIdentifiedObject( activePowerLimit );
            if( result == null ) result = caseCimObjectWithID( activePowerLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REGULATION_SCHEDULE: {
            RegulationSchedule regulationSchedule = ( RegulationSchedule ) theEObject;
            T result = caseRegulationSchedule( regulationSchedule );
            if( result == null ) result = caseSeasonDayTypeSchedule( regulationSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( regulationSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( regulationSchedule );
            if( result == null ) result = caseIdentifiedObject( regulationSchedule );
            if( result == null ) result = caseCimObjectWithID( regulationSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GROUND: {
            Ground ground = ( Ground ) theEObject;
            T result = caseGround( ground );
            if( result == null ) result = caseConductingEquipment( ground );
            if( result == null ) result = caseEquipment( ground );
            if( result == null ) result = casePowerSystemResource( ground );
            if( result == null ) result = caseIdentifiedObject( ground );
            if( result == null ) result = caseCimObjectWithID( ground );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PETERSEN_COIL: {
            PetersenCoil petersenCoil = ( PetersenCoil ) theEObject;
            T result = casePetersenCoil( petersenCoil );
            if( result == null ) result = caseEarthFaultCompensator( petersenCoil );
            if( result == null ) result = caseConductingEquipment( petersenCoil );
            if( result == null ) result = caseEquipment( petersenCoil );
            if( result == null ) result = casePowerSystemResource( petersenCoil );
            if( result == null ) result = caseIdentifiedObject( petersenCoil );
            if( result == null ) result = caseCimObjectWithID( petersenCoil );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LOAD_GROUP: {
            LoadGroup loadGroup = ( LoadGroup ) theEObject;
            T result = caseLoadGroup( loadGroup );
            if( result == null ) result = caseIdentifiedObject( loadGroup );
            if( result == null ) result = caseCimObjectWithID( loadGroup );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GROSS_TO_NET_ACTIVE_POWER_CURVE: {
            GrossToNetActivePowerCurve grossToNetActivePowerCurve = ( GrossToNetActivePowerCurve ) theEObject;
            T result = caseGrossToNetActivePowerCurve( grossToNetActivePowerCurve );
            if( result == null ) result = caseCurve( grossToNetActivePowerCurve );
            if( result == null ) result = caseIdentifiedObject( grossToNetActivePowerCurve );
            if( result == null ) result = caseCimObjectWithID( grossToNetActivePowerCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VS_CAPABILITY_CURVE: {
            VsCapabilityCurve vsCapabilityCurve = ( VsCapabilityCurve ) theEObject;
            T result = caseVsCapabilityCurve( vsCapabilityCurve );
            if( result == null ) result = caseCurve( vsCapabilityCurve );
            if( result == null ) result = caseIdentifiedObject( vsCapabilityCurve );
            if( result == null ) result = caseCimObjectWithID( vsCapabilityCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACDC_TERMINAL: {
            ACDCTerminal acdcTerminal = ( ACDCTerminal ) theEObject;
            T result = caseACDCTerminal( acdcTerminal );
            if( result == null ) result = caseIdentifiedObject( acdcTerminal );
            if( result == null ) result = caseCimObjectWithID( acdcTerminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CLAMP: {
            Clamp clamp = ( Clamp ) theEObject;
            T result = caseClamp( clamp );
            if( result == null ) result = caseConductingEquipment( clamp );
            if( result == null ) result = caseEquipment( clamp );
            if( result == null ) result = casePowerSystemResource( clamp );
            if( result == null ) result = caseIdentifiedObject( clamp );
            if( result == null ) result = caseCimObjectWithID( clamp );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TARGET_LEVEL_SCHEDULE: {
            TargetLevelSchedule targetLevelSchedule = ( TargetLevelSchedule ) theEObject;
            T result = caseTargetLevelSchedule( targetLevelSchedule );
            if( result == null ) result = caseCurve( targetLevelSchedule );
            if( result == null ) result = caseIdentifiedObject( targetLevelSchedule );
            if( result == null ) result = caseCimObjectWithID( targetLevelSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_TANK: {
            TransformerTank transformerTank = ( TransformerTank ) theEObject;
            T result = caseTransformerTank( transformerTank );
            if( result == null ) result = caseEquipment( transformerTank );
            if( result == null ) result = casePowerSystemResource( transformerTank );
            if( result == null ) result = caseIdentifiedObject( transformerTank );
            if( result == null ) result = caseCimObjectWithID( transformerTank );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GEN_UNIT_OP_SCHEDULE: {
            GenUnitOpSchedule genUnitOpSchedule = ( GenUnitOpSchedule ) theEObject;
            T result = caseGenUnitOpSchedule( genUnitOpSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( genUnitOpSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( genUnitOpSchedule );
            if( result == null ) result = caseIdentifiedObject( genUnitOpSchedule );
            if( result == null ) result = caseCimObjectWithID( genUnitOpSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACDC_CONVERTER_DC_TERMINAL: {
            ACDCConverterDCTerminal acdcConverterDCTerminal = ( ACDCConverterDCTerminal ) theEObject;
            T result = caseACDCConverterDCTerminal( acdcConverterDCTerminal );
            if( result == null ) result = caseDCBaseTerminal( acdcConverterDCTerminal );
            if( result == null ) result = caseACDCTerminal( acdcConverterDCTerminal );
            if( result == null ) result = caseIdentifiedObject( acdcConverterDCTerminal );
            if( result == null ) result = caseCimObjectWithID( acdcConverterDCTerminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TEXT_DIAGRAM_OBJECT: {
            TextDiagramObject textDiagramObject = ( TextDiagramObject ) theEObject;
            T result = caseTextDiagramObject( textDiagramObject );
            if( result == null ) result = caseDiagramObject( textDiagramObject );
            if( result == null ) result = caseIdentifiedObject( textDiagramObject );
            if( result == null ) result = caseCimObjectWithID( textDiagramObject );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_SHUNT_COMPENSATOR_SECTIONS: {
            SvShuntCompensatorSections svShuntCompensatorSections = ( SvShuntCompensatorSections ) theEObject;
            T result = caseSvShuntCompensatorSections( svShuntCompensatorSections );
            if( result == null ) result = caseStateVariable( svShuntCompensatorSections );
            if( result == null ) result = caseCimObjectWithID( svShuntCompensatorSections );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PROTECTED_SWITCH: {
            ProtectedSwitch protectedSwitch = ( ProtectedSwitch ) theEObject;
            T result = caseProtectedSwitch( protectedSwitch );
            if( result == null ) result = caseSwitch( protectedSwitch );
            if( result == null ) result = caseConductingEquipment( protectedSwitch );
            if( result == null ) result = caseEquipment( protectedSwitch );
            if( result == null ) result = casePowerSystemResource( protectedSwitch );
            if( result == null ) result = caseIdentifiedObject( protectedSwitch );
            if( result == null ) result = caseCimObjectWithID( protectedSwitch );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.START_IGN_FUEL_CURVE: {
            StartIgnFuelCurve startIgnFuelCurve = ( StartIgnFuelCurve ) theEObject;
            T result = caseStartIgnFuelCurve( startIgnFuelCurve );
            if( result == null ) result = caseCurve( startIgnFuelCurve );
            if( result == null ) result = caseIdentifiedObject( startIgnFuelCurve );
            if( result == null ) result = caseCimObjectWithID( startIgnFuelCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM_OBJECT_POINT: {
            DiagramObjectPoint diagramObjectPoint = ( DiagramObjectPoint ) theEObject;
            T result = caseDiagramObjectPoint( diagramObjectPoint );
            if( result == null ) result = caseCimObjectWithID( diagramObjectPoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.OPERATING_SHARE: {
            OperatingShare operatingShare = ( OperatingShare ) theEObject;
            T result = caseOperatingShare( operatingShare );
            if( result == null ) result = caseCimObjectWithID( operatingShare );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_POINT: {
            NonlinearShuntCompensatorPoint nonlinearShuntCompensatorPoint = ( NonlinearShuntCompensatorPoint ) theEObject;
            T result = caseNonlinearShuntCompensatorPoint( nonlinearShuntCompensatorPoint );
            if( result == null ) result = caseCimObjectWithID( nonlinearShuntCompensatorPoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ANALOG_LIMIT_SET: {
            AnalogLimitSet analogLimitSet = ( AnalogLimitSet ) theEObject;
            T result = caseAnalogLimitSet( analogLimitSet );
            if( result == null ) result = caseLimitSet( analogLimitSet );
            if( result == null ) result = caseIdentifiedObject( analogLimitSet );
            if( result == null ) result = caseCimObjectWithID( analogLimitSet );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTINGENCY: {
            Contingency contingency = ( Contingency ) theEObject;
            T result = caseContingency( contingency );
            if( result == null ) result = caseIdentifiedObject( contingency );
            if( result == null ) result = caseCimObjectWithID( contingency );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_TERMINAL: {
            DCTerminal dcTerminal = ( DCTerminal ) theEObject;
            T result = caseDCTerminal( dcTerminal );
            if( result == null ) result = caseDCBaseTerminal( dcTerminal );
            if( result == null ) result = caseACDCTerminal( dcTerminal );
            if( result == null ) result = caseIdentifiedObject( dcTerminal );
            if( result == null ) result = caseCimObjectWithID( dcTerminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_SYMMETRICAL: {
            PhaseTapChangerSymmetrical phaseTapChangerSymmetrical = ( PhaseTapChangerSymmetrical ) theEObject;
            T result = casePhaseTapChangerSymmetrical( phaseTapChangerSymmetrical );
            if( result == null ) result = casePhaseTapChangerNonLinear( phaseTapChangerSymmetrical );
            if( result == null ) result = casePhaseTapChanger( phaseTapChangerSymmetrical );
            if( result == null ) result = caseTapChanger( phaseTapChangerSymmetrical );
            if( result == null ) result = casePowerSystemResource( phaseTapChangerSymmetrical );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerSymmetrical );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerSymmetrical );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_END: {
            TransformerEnd transformerEnd = ( TransformerEnd ) theEObject;
            T result = caseTransformerEnd( transformerEnd );
            if( result == null ) result = caseIdentifiedObject( transformerEnd );
            if( result == null ) result = caseCimObjectWithID( transformerEnd );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BRANCH_GROUP_TERMINAL: {
            BranchGroupTerminal branchGroupTerminal = ( BranchGroupTerminal ) theEObject;
            T result = caseBranchGroupTerminal( branchGroupTerminal );
            if( result == null ) result = caseCimObjectWithID( branchGroupTerminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DISCRETE_VALUE: {
            DiscreteValue discreteValue = ( DiscreteValue ) theEObject;
            T result = caseDiscreteValue( discreteValue );
            if( result == null ) result = caseMeasurementValue( discreteValue );
            if( result == null ) result = caseIdentifiedObject( discreteValue );
            if( result == null ) result = caseCimObjectWithID( discreteValue );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STATIC_VAR_COMPENSATOR: {
            StaticVarCompensator staticVarCompensator = ( StaticVarCompensator ) theEObject;
            T result = caseStaticVarCompensator( staticVarCompensator );
            if( result == null ) result = caseRegulatingCondEq( staticVarCompensator );
            if( result == null ) result = caseConductingEquipment( staticVarCompensator );
            if( result == null ) result = caseEquipment( staticVarCompensator );
            if( result == null ) result = casePowerSystemResource( staticVarCompensator );
            if( result == null ) result = caseIdentifiedObject( staticVarCompensator );
            if( result == null ) result = caseCimObjectWithID( staticVarCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ALT_GENERATING_UNIT_MEAS: {
            AltGeneratingUnitMeas altGeneratingUnitMeas = ( AltGeneratingUnitMeas ) theEObject;
            T result = caseAltGeneratingUnitMeas( altGeneratingUnitMeas );
            if( result == null ) result = caseCimObjectWithID( altGeneratingUnitMeas );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LIMIT: {
            Limit limit = ( Limit ) theEObject;
            T result = caseLimit( limit );
            if( result == null ) result = caseIdentifiedObject( limit );
            if( result == null ) result = caseCimObjectWithID( limit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR: {
            NonlinearShuntCompensator nonlinearShuntCompensator = ( NonlinearShuntCompensator ) theEObject;
            T result = caseNonlinearShuntCompensator( nonlinearShuntCompensator );
            if( result == null ) result = caseShuntCompensator( nonlinearShuntCompensator );
            if( result == null ) result = caseRegulatingCondEq( nonlinearShuntCompensator );
            if( result == null ) result = caseConductingEquipment( nonlinearShuntCompensator );
            if( result == null ) result = caseEquipment( nonlinearShuntCompensator );
            if( result == null ) result = casePowerSystemResource( nonlinearShuntCompensator );
            if( result == null ) result = caseIdentifiedObject( nonlinearShuntCompensator );
            if( result == null ) result = caseCimObjectWithID( nonlinearShuntCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONFORM_LOAD_SCHEDULE: {
            ConformLoadSchedule conformLoadSchedule = ( ConformLoadSchedule ) theEObject;
            T result = caseConformLoadSchedule( conformLoadSchedule );
            if( result == null ) result = caseSeasonDayTypeSchedule( conformLoadSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( conformLoadSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( conformLoadSchedule );
            if( result == null ) result = caseIdentifiedObject( conformLoadSchedule );
            if( result == null ) result = caseCimObjectWithID( conformLoadSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM_OBJECT_STYLE: {
            DiagramObjectStyle diagramObjectStyle = ( DiagramObjectStyle ) theEObject;
            T result = caseDiagramObjectStyle( diagramObjectStyle );
            if( result == null ) result = caseIdentifiedObject( diagramObjectStyle );
            if( result == null ) result = caseCimObjectWithID( diagramObjectStyle );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BUSBAR_SECTION: {
            BusbarSection busbarSection = ( BusbarSection ) theEObject;
            T result = caseBusbarSection( busbarSection );
            if( result == null ) result = caseConnector( busbarSection );
            if( result == null ) result = caseConductingEquipment( busbarSection );
            if( result == null ) result = caseEquipment( busbarSection );
            if( result == null ) result = casePowerSystemResource( busbarSection );
            if( result == null ) result = caseIdentifiedObject( busbarSection );
            if( result == null ) result = caseCimObjectWithID( busbarSection );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIVALENT_NETWORK: {
            EquivalentNetwork equivalentNetwork = ( EquivalentNetwork ) theEObject;
            T result = caseEquivalentNetwork( equivalentNetwork );
            if( result == null ) result = caseConnectivityNodeContainer( equivalentNetwork );
            if( result == null ) result = casePowerSystemResource( equivalentNetwork );
            if( result == null ) result = caseIdentifiedObject( equivalentNetwork );
            if( result == null ) result = caseCimObjectWithID( equivalentNetwork );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ENERGY_CONSUMER: {
            EnergyConsumer energyConsumer = ( EnergyConsumer ) theEObject;
            T result = caseEnergyConsumer( energyConsumer );
            if( result == null ) result = caseConductingEquipment( energyConsumer );
            if( result == null ) result = caseEquipment( energyConsumer );
            if( result == null ) result = casePowerSystemResource( energyConsumer );
            if( result == null ) result = caseIdentifiedObject( energyConsumer );
            if( result == null ) result = caseCimObjectWithID( energyConsumer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CURRENT_RELAY: {
            CurrentRelay currentRelay = ( CurrentRelay ) theEObject;
            T result = caseCurrentRelay( currentRelay );
            if( result == null ) result = caseProtectionEquipment( currentRelay );
            if( result == null ) result = caseEquipment( currentRelay );
            if( result == null ) result = casePowerSystemResource( currentRelay );
            if( result == null ) result = caseIdentifiedObject( currentRelay );
            if( result == null ) result = caseCimObjectWithID( currentRelay );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LEVEL_VS_VOLUME_CURVE: {
            LevelVsVolumeCurve levelVsVolumeCurve = ( LevelVsVolumeCurve ) theEObject;
            T result = caseLevelVsVolumeCurve( levelVsVolumeCurve );
            if( result == null ) result = caseCurve( levelVsVolumeCurve );
            if( result == null ) result = caseIdentifiedObject( levelVsVolumeCurve );
            if( result == null ) result = caseCimObjectWithID( levelVsVolumeCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NAME_TYPE_AUTHORITY: {
            NameTypeAuthority nameTypeAuthority = ( NameTypeAuthority ) theEObject;
            T result = caseNameTypeAuthority( nameTypeAuthority );
            if( result == null ) result = caseCimObjectWithID( nameTypeAuthority );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PLANT: {
            Plant plant = ( Plant ) theEObject;
            T result = casePlant( plant );
            if( result == null ) result = caseEquipmentContainer( plant );
            if( result == null ) result = caseConnectivityNodeContainer( plant );
            if( result == null ) result = casePowerSystemResource( plant );
            if( result == null ) result = caseIdentifiedObject( plant );
            if( result == null ) result = caseCimObjectWithID( plant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CURVE_DATA: {
            CurveData curveData = ( CurveData ) theEObject;
            T result = caseCurveData( curveData );
            if( result == null ) result = caseCimObjectWithID( curveData );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_LINEAR: {
            PhaseTapChangerLinear phaseTapChangerLinear = ( PhaseTapChangerLinear ) theEObject;
            T result = casePhaseTapChangerLinear( phaseTapChangerLinear );
            if( result == null ) result = casePhaseTapChanger( phaseTapChangerLinear );
            if( result == null ) result = caseTapChanger( phaseTapChangerLinear );
            if( result == null ) result = casePowerSystemResource( phaseTapChangerLinear );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerLinear );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerLinear );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PER_LENGTH_LINE_PARAMETER: {
            PerLengthLineParameter perLengthLineParameter = ( PerLengthLineParameter ) theEObject;
            T result = casePerLengthLineParameter( perLengthLineParameter );
            if( result == null ) result = caseIdentifiedObject( perLengthLineParameter );
            if( result == null ) result = caseCimObjectWithID( perLengthLineParameter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RAISE_LOWER_COMMAND: {
            RaiseLowerCommand raiseLowerCommand = ( RaiseLowerCommand ) theEObject;
            T result = caseRaiseLowerCommand( raiseLowerCommand );
            if( result == null ) result = caseAnalogControl( raiseLowerCommand );
            if( result == null ) result = caseControl( raiseLowerCommand );
            if( result == null ) result = caseIdentifiedObject( raiseLowerCommand );
            if( result == null ) result = caseCimObjectWithID( raiseLowerCommand );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_CONVERTER_UNIT: {
            DCConverterUnit dcConverterUnit = ( DCConverterUnit ) theEObject;
            T result = caseDCConverterUnit( dcConverterUnit );
            if( result == null ) result = caseDCEquipmentContainer( dcConverterUnit );
            if( result == null ) result = caseEquipmentContainer( dcConverterUnit );
            if( result == null ) result = caseConnectivityNodeContainer( dcConverterUnit );
            if( result == null ) result = casePowerSystemResource( dcConverterUnit );
            if( result == null ) result = caseIdentifiedObject( dcConverterUnit );
            if( result == null ) result = caseCimObjectWithID( dcConverterUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REGULAR_TIME_POINT: {
            RegularTimePoint regularTimePoint = ( RegularTimePoint ) theEObject;
            T result = caseRegularTimePoint( regularTimePoint );
            if( result == null ) result = caseCimObjectWithID( regularTimePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VALUE_ALIAS_SET: {
            ValueAliasSet valueAliasSet = ( ValueAliasSet ) theEObject;
            T result = caseValueAliasSet( valueAliasSet );
            if( result == null ) result = caseIdentifiedObject( valueAliasSet );
            if( result == null ) result = caseCimObjectWithID( valueAliasSet );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACCUMULATOR_VALUE: {
            AccumulatorValue accumulatorValue = ( AccumulatorValue ) theEObject;
            T result = caseAccumulatorValue( accumulatorValue );
            if( result == null ) result = caseMeasurementValue( accumulatorValue );
            if( result == null ) result = caseIdentifiedObject( accumulatorValue );
            if( result == null ) result = caseCimObjectWithID( accumulatorValue );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MEASUREMENT_VALUE: {
            MeasurementValue measurementValue = ( MeasurementValue ) theEObject;
            T result = caseMeasurementValue( measurementValue );
            if( result == null ) result = caseIdentifiedObject( measurementValue );
            if( result == null ) result = caseCimObjectWithID( measurementValue );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_PUMP: {
            HydroPump hydroPump = ( HydroPump ) theEObject;
            T result = caseHydroPump( hydroPump );
            if( result == null ) result = caseEquipment( hydroPump );
            if( result == null ) result = casePowerSystemResource( hydroPump );
            if( result == null ) result = caseIdentifiedObject( hydroPump );
            if( result == null ) result = caseCimObjectWithID( hydroPump );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_SWITCH: {
            DCSwitch dcSwitch = ( DCSwitch ) theEObject;
            T result = caseDCSwitch( dcSwitch );
            if( result == null ) result = caseDCConductingEquipment( dcSwitch );
            if( result == null ) result = caseEquipment( dcSwitch );
            if( result == null ) result = casePowerSystemResource( dcSwitch );
            if( result == null ) result = caseIdentifiedObject( dcSwitch );
            if( result == null ) result = caseCimObjectWithID( dcSwitch );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_POWER_FLOW: {
            SvPowerFlow svPowerFlow = ( SvPowerFlow ) theEObject;
            T result = caseSvPowerFlow( svPowerFlow );
            if( result == null ) result = caseStateVariable( svPowerFlow );
            if( result == null ) result = caseCimObjectWithID( svPowerFlow );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SWITCH_SCHEDULE: {
            SwitchSchedule switchSchedule = ( SwitchSchedule ) theEObject;
            T result = caseSwitchSchedule( switchSchedule );
            if( result == null ) result = caseSeasonDayTypeSchedule( switchSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( switchSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( switchSchedule );
            if( result == null ) result = caseIdentifiedObject( switchSchedule );
            if( result == null ) result = caseCimObjectWithID( switchSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RATIO_TAP_CHANGER_TABLE_POINT: {
            RatioTapChangerTablePoint ratioTapChangerTablePoint = ( RatioTapChangerTablePoint ) theEObject;
            T result = caseRatioTapChangerTablePoint( ratioTapChangerTablePoint );
            if( result == null ) result = caseTapChangerTablePoint( ratioTapChangerTablePoint );
            if( result == null ) result = caseCimObjectWithID( ratioTapChangerTablePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RESERVOIR: {
            Reservoir reservoir = ( Reservoir ) theEObject;
            T result = caseReservoir( reservoir );
            if( result == null ) result = casePowerSystemResource( reservoir );
            if( result == null ) result = caseIdentifiedObject( reservoir );
            if( result == null ) result = caseCimObjectWithID( reservoir );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIVALENT_SHUNT: {
            EquivalentShunt equivalentShunt = ( EquivalentShunt ) theEObject;
            T result = caseEquivalentShunt( equivalentShunt );
            if( result == null ) result = caseEquivalentEquipment( equivalentShunt );
            if( result == null ) result = caseConductingEquipment( equivalentShunt );
            if( result == null ) result = caseEquipment( equivalentShunt );
            if( result == null ) result = casePowerSystemResource( equivalentShunt );
            if( result == null ) result = caseIdentifiedObject( equivalentShunt );
            if( result == null ) result = caseCimObjectWithID( equivalentShunt );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SEASON: {
            Season season = ( Season ) theEObject;
            T result = caseSeason( season );
            if( result == null ) result = caseIdentifiedObject( season );
            if( result == null ) result = caseCimObjectWithID( season );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SERIES_COMPENSATOR: {
            SeriesCompensator seriesCompensator = ( SeriesCompensator ) theEObject;
            T result = caseSeriesCompensator( seriesCompensator );
            if( result == null ) result = caseConductingEquipment( seriesCompensator );
            if( result == null ) result = caseEquipment( seriesCompensator );
            if( result == null ) result = casePowerSystemResource( seriesCompensator );
            if( result == null ) result = caseIdentifiedObject( seriesCompensator );
            if( result == null ) result = caseCimObjectWithID( seriesCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HEAT_RECOVERY_BOILER: {
            HeatRecoveryBoiler heatRecoveryBoiler = ( HeatRecoveryBoiler ) theEObject;
            T result = caseHeatRecoveryBoiler( heatRecoveryBoiler );
            if( result == null ) result = caseFossilSteamSupply( heatRecoveryBoiler );
            if( result == null ) result = caseSteamSupply( heatRecoveryBoiler );
            if( result == null ) result = casePowerSystemResource( heatRecoveryBoiler );
            if( result == null ) result = caseIdentifiedObject( heatRecoveryBoiler );
            if( result == null ) result = caseCimObjectWithID( heatRecoveryBoiler );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EXTERNAL_NETWORK_INJECTION: {
            ExternalNetworkInjection externalNetworkInjection = ( ExternalNetworkInjection ) theEObject;
            T result = caseExternalNetworkInjection( externalNetworkInjection );
            if( result == null ) result = caseRegulatingCondEq( externalNetworkInjection );
            if( result == null ) result = caseConductingEquipment( externalNetworkInjection );
            if( result == null ) result = caseEquipment( externalNetworkInjection );
            if( result == null ) result = casePowerSystemResource( externalNetworkInjection );
            if( result == null ) result = caseIdentifiedObject( externalNetworkInjection );
            if( result == null ) result = caseCimObjectWithID( externalNetworkInjection );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TRANSFORMER_MESH_IMPEDANCE: {
            TransformerMeshImpedance transformerMeshImpedance = ( TransformerMeshImpedance ) theEObject;
            T result = caseTransformerMeshImpedance( transformerMeshImpedance );
            if( result == null ) result = caseIdentifiedObject( transformerMeshImpedance );
            if( result == null ) result = caseCimObjectWithID( transformerMeshImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SV_VOLTAGE: {
            SvVoltage svVoltage = ( SvVoltage ) theEObject;
            T result = caseSvVoltage( svVoltage );
            if( result == null ) result = caseStateVariable( svVoltage );
            if( result == null ) result = caseCimObjectWithID( svVoltage );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.IEC61970CIM_VERSION: {
            IEC61970CIMVersion iec61970CIMVersion = ( IEC61970CIMVersion ) theEObject;
            T result = caseIEC61970CIMVersion( iec61970CIMVersion );
            if( result == null ) result = caseCimObjectWithID( iec61970CIMVersion );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STRING_MEASUREMENT_VALUE: {
            StringMeasurementValue stringMeasurementValue = ( StringMeasurementValue ) theEObject;
            T result = caseStringMeasurementValue( stringMeasurementValue );
            if( result == null ) result = caseMeasurementValue( stringMeasurementValue );
            if( result == null ) result = caseIdentifiedObject( stringMeasurementValue );
            if( result == null ) result = caseCimObjectWithID( stringMeasurementValue );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ANALOG_VALUE: {
            AnalogValue analogValue = ( AnalogValue ) theEObject;
            T result = caseAnalogValue( analogValue );
            if( result == null ) result = caseMeasurementValue( analogValue );
            if( result == null ) result = caseIdentifiedObject( analogValue );
            if( result == null ) result = caseCimObjectWithID( analogValue );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RATIO_TAP_CHANGER_TABLE: {
            RatioTapChangerTable ratioTapChangerTable = ( RatioTapChangerTable ) theEObject;
            T result = caseRatioTapChangerTable( ratioTapChangerTable );
            if( result == null ) result = caseIdentifiedObject( ratioTapChangerTable );
            if( result == null ) result = caseCimObjectWithID( ratioTapChangerTable );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.IRREGULAR_TIME_POINT: {
            IrregularTimePoint irregularTimePoint = ( IrregularTimePoint ) theEObject;
            T result = caseIrregularTimePoint( irregularTimePoint );
            if( result == null ) result = caseCimObjectWithID( irregularTimePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.AC_LINE_SEGMENT_PHASE: {
            ACLineSegmentPhase acLineSegmentPhase = ( ACLineSegmentPhase ) theEObject;
            T result = caseACLineSegmentPhase( acLineSegmentPhase );
            if( result == null ) result = casePowerSystemResource( acLineSegmentPhase );
            if( result == null ) result = caseIdentifiedObject( acLineSegmentPhase );
            if( result == null ) result = caseCimObjectWithID( acLineSegmentPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONFORM_LOAD: {
            ConformLoad conformLoad = ( ConformLoad ) theEObject;
            T result = caseConformLoad( conformLoad );
            if( result == null ) result = caseEnergyConsumer( conformLoad );
            if( result == null ) result = caseConductingEquipment( conformLoad );
            if( result == null ) result = caseEquipment( conformLoad );
            if( result == null ) result = casePowerSystemResource( conformLoad );
            if( result == null ) result = caseIdentifiedObject( conformLoad );
            if( result == null ) result = caseCimObjectWithID( conformLoad );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.AC_LINE_SEGMENT: {
            ACLineSegment acLineSegment = ( ACLineSegment ) theEObject;
            T result = caseACLineSegment( acLineSegment );
            if( result == null ) result = caseConductor( acLineSegment );
            if( result == null ) result = caseConductingEquipment( acLineSegment );
            if( result == null ) result = caseEquipment( acLineSegment );
            if( result == null ) result = casePowerSystemResource( acLineSegment );
            if( result == null ) result = caseIdentifiedObject( acLineSegment );
            if( result == null ) result = caseCimObjectWithID( acLineSegment );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STARTUP_MODEL: {
            StartupModel startupModel = ( StartupModel ) theEObject;
            T result = caseStartupModel( startupModel );
            if( result == null ) result = caseIdentifiedObject( startupModel );
            if( result == null ) result = caseCimObjectWithID( startupModel );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EQUIVALENT_BRANCH: {
            EquivalentBranch equivalentBranch = ( EquivalentBranch ) theEObject;
            T result = caseEquivalentBranch( equivalentBranch );
            if( result == null ) result = caseEquivalentEquipment( equivalentBranch );
            if( result == null ) result = caseConductingEquipment( equivalentBranch );
            if( result == null ) result = caseEquipment( equivalentBranch );
            if( result == null ) result = casePowerSystemResource( equivalentBranch );
            if( result == null ) result = caseIdentifiedObject( equivalentBranch );
            if( result == null ) result = caseCimObjectWithID( equivalentBranch );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LINE: {
            Line line = ( Line ) theEObject;
            T result = caseLine( line );
            if( result == null ) result = caseEquipmentContainer( line );
            if( result == null ) result = caseConnectivityNodeContainer( line );
            if( result == null ) result = casePowerSystemResource( line );
            if( result == null ) result = caseIdentifiedObject( line );
            if( result == null ) result = caseCimObjectWithID( line );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BUS_NAME_MARKER: {
            BusNameMarker busNameMarker = ( BusNameMarker ) theEObject;
            T result = caseBusNameMarker( busNameMarker );
            if( result == null ) result = caseIdentifiedObject( busNameMarker );
            if( result == null ) result = caseCimObjectWithID( busNameMarker );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_TOPOLOGICAL_NODE: {
            DCTopologicalNode dcTopologicalNode = ( DCTopologicalNode ) theEObject;
            T result = caseDCTopologicalNode( dcTopologicalNode );
            if( result == null ) result = caseIdentifiedObject( dcTopologicalNode );
            if( result == null ) result = caseCimObjectWithID( dcTopologicalNode );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_DISCONNECTOR: {
            DCDisconnector dcDisconnector = ( DCDisconnector ) theEObject;
            T result = caseDCDisconnector( dcDisconnector );
            if( result == null ) result = caseDCSwitch( dcDisconnector );
            if( result == null ) result = caseDCConductingEquipment( dcDisconnector );
            if( result == null ) result = caseEquipment( dcDisconnector );
            if( result == null ) result = casePowerSystemResource( dcDisconnector );
            if( result == null ) result = caseIdentifiedObject( dcDisconnector );
            if( result == null ) result = caseCimObjectWithID( dcDisconnector );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_SHUNT: {
            DCShunt dcShunt = ( DCShunt ) theEObject;
            T result = caseDCShunt( dcShunt );
            if( result == null ) result = caseDCConductingEquipment( dcShunt );
            if( result == null ) result = caseEquipment( dcShunt );
            if( result == null ) result = casePowerSystemResource( dcShunt );
            if( result == null ) result = caseIdentifiedObject( dcShunt );
            if( result == null ) result = caseCimObjectWithID( dcShunt );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_IMPEDANCE_DATA: {
            PhaseImpedanceData phaseImpedanceData = ( PhaseImpedanceData ) theEObject;
            T result = casePhaseImpedanceData( phaseImpedanceData );
            if( result == null ) result = caseCimObjectWithID( phaseImpedanceData );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.INCREMENTAL_HEAT_RATE_CURVE: {
            IncrementalHeatRateCurve incrementalHeatRateCurve = ( IncrementalHeatRateCurve ) theEObject;
            T result = caseIncrementalHeatRateCurve( incrementalHeatRateCurve );
            if( result == null ) result = caseCurve( incrementalHeatRateCurve );
            if( result == null ) result = caseIdentifiedObject( incrementalHeatRateCurve );
            if( result == null ) result = caseCimObjectWithID( incrementalHeatRateCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SUB_LOAD_AREA: {
            SubLoadArea subLoadArea = ( SubLoadArea ) theEObject;
            T result = caseSubLoadArea( subLoadArea );
            if( result == null ) result = caseEnergyArea( subLoadArea );
            if( result == null ) result = caseIdentifiedObject( subLoadArea );
            if( result == null ) result = caseCimObjectWithID( subLoadArea );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DAY_TYPE: {
            DayType dayType = ( DayType ) theEObject;
            T result = caseDayType( dayType );
            if( result == null ) result = caseIdentifiedObject( dayType );
            if( result == null ) result = caseCimObjectWithID( dayType );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COMBUSTION_TURBINE: {
            CombustionTurbine combustionTurbine = ( CombustionTurbine ) theEObject;
            T result = caseCombustionTurbine( combustionTurbine );
            if( result == null ) result = casePrimeMover( combustionTurbine );
            if( result == null ) result = casePowerSystemResource( combustionTurbine );
            if( result == null ) result = caseIdentifiedObject( combustionTurbine );
            if( result == null ) result = caseCimObjectWithID( combustionTurbine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BREAKER: {
            Breaker breaker = ( Breaker ) theEObject;
            T result = caseBreaker( breaker );
            if( result == null ) result = caseProtectedSwitch( breaker );
            if( result == null ) result = caseSwitch( breaker );
            if( result == null ) result = caseConductingEquipment( breaker );
            if( result == null ) result = caseEquipment( breaker );
            if( result == null ) result = casePowerSystemResource( breaker );
            if( result == null ) result = caseIdentifiedObject( breaker );
            if( result == null ) result = caseCimObjectWithID( breaker );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GENERATING_UNIT: {
            GeneratingUnit generatingUnit = ( GeneratingUnit ) theEObject;
            T result = caseGeneratingUnit( generatingUnit );
            if( result == null ) result = caseEquipment( generatingUnit );
            if( result == null ) result = casePowerSystemResource( generatingUnit );
            if( result == null ) result = caseIdentifiedObject( generatingUnit );
            if( result == null ) result = caseCimObjectWithID( generatingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EMISSION_ACCOUNT: {
            EmissionAccount emissionAccount = ( EmissionAccount ) theEObject;
            T result = caseEmissionAccount( emissionAccount );
            if( result == null ) result = caseCurve( emissionAccount );
            if( result == null ) result = caseIdentifiedObject( emissionAccount );
            if( result == null ) result = caseCimObjectWithID( emissionAccount );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ASYNCHRONOUS_MACHINE: {
            AsynchronousMachine asynchronousMachine = ( AsynchronousMachine ) theEObject;
            T result = caseAsynchronousMachine( asynchronousMachine );
            if( result == null ) result = caseRotatingMachine( asynchronousMachine );
            if( result == null ) result = caseRegulatingCondEq( asynchronousMachine );
            if( result == null ) result = caseConductingEquipment( asynchronousMachine );
            if( result == null ) result = caseEquipment( asynchronousMachine );
            if( result == null ) result = casePowerSystemResource( asynchronousMachine );
            if( result == null ) result = caseIdentifiedObject( asynchronousMachine );
            if( result == null ) result = caseCimObjectWithID( asynchronousMachine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STATION_SUPPLY: {
            StationSupply stationSupply = ( StationSupply ) theEObject;
            T result = caseStationSupply( stationSupply );
            if( result == null ) result = caseEnergyConsumer( stationSupply );
            if( result == null ) result = caseConductingEquipment( stationSupply );
            if( result == null ) result = caseEquipment( stationSupply );
            if( result == null ) result = casePowerSystemResource( stationSupply );
            if( result == null ) result = caseIdentifiedObject( stationSupply );
            if( result == null ) result = caseCimObjectWithID( stationSupply );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ACCUMULATOR: {
            Accumulator accumulator = ( Accumulator ) theEObject;
            T result = caseAccumulator( accumulator );
            if( result == null ) result = caseMeasurement( accumulator );
            if( result == null ) result = caseIdentifiedObject( accumulator );
            if( result == null ) result = caseCimObjectWithID( accumulator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CAES_PLANT: {
            CAESPlant caesPlant = ( CAESPlant ) theEObject;
            T result = caseCAESPlant( caesPlant );
            if( result == null ) result = casePowerSystemResource( caesPlant );
            if( result == null ) result = caseIdentifiedObject( caesPlant );
            if( result == null ) result = caseCimObjectWithID( caesPlant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PER_LENGTH_PHASE_IMPEDANCE: {
            PerLengthPhaseImpedance perLengthPhaseImpedance = ( PerLengthPhaseImpedance ) theEObject;
            T result = casePerLengthPhaseImpedance( perLengthPhaseImpedance );
            if( result == null ) result = casePerLengthImpedance( perLengthPhaseImpedance );
            if( result == null ) result = casePerLengthLineParameter( perLengthPhaseImpedance );
            if( result == null ) result = caseIdentifiedObject( perLengthPhaseImpedance );
            if( result == null ) result = caseCimObjectWithID( perLengthPhaseImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SHUNT_COMPENSATOR: {
            ShuntCompensator shuntCompensator = ( ShuntCompensator ) theEObject;
            T result = caseShuntCompensator( shuntCompensator );
            if( result == null ) result = caseRegulatingCondEq( shuntCompensator );
            if( result == null ) result = caseConductingEquipment( shuntCompensator );
            if( result == null ) result = caseEquipment( shuntCompensator );
            if( result == null ) result = casePowerSystemResource( shuntCompensator );
            if( result == null ) result = caseIdentifiedObject( shuntCompensator );
            if( result == null ) result = caseCimObjectWithID( shuntCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LINEAR_SHUNT_COMPENSATOR_PHASE: {
            LinearShuntCompensatorPhase linearShuntCompensatorPhase = ( LinearShuntCompensatorPhase ) theEObject;
            T result = caseLinearShuntCompensatorPhase( linearShuntCompensatorPhase );
            if( result == null ) result = caseShuntCompensatorPhase( linearShuntCompensatorPhase );
            if( result == null ) result = casePowerSystemResource( linearShuntCompensatorPhase );
            if( result == null ) result = caseIdentifiedObject( linearShuntCompensatorPhase );
            if( result == null ) result = caseCimObjectWithID( linearShuntCompensatorPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ANALOG_LIMIT: {
            AnalogLimit analogLimit = ( AnalogLimit ) theEObject;
            T result = caseAnalogLimit( analogLimit );
            if( result == null ) result = caseLimit( analogLimit );
            if( result == null ) result = caseIdentifiedObject( analogLimit );
            if( result == null ) result = caseCimObjectWithID( analogLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_PHASE: {
            NonlinearShuntCompensatorPhase nonlinearShuntCompensatorPhase = ( NonlinearShuntCompensatorPhase ) theEObject;
            T result = caseNonlinearShuntCompensatorPhase( nonlinearShuntCompensatorPhase );
            if( result == null ) result = caseShuntCompensatorPhase( nonlinearShuntCompensatorPhase );
            if( result == null ) result = casePowerSystemResource( nonlinearShuntCompensatorPhase );
            if( result == null ) result = caseIdentifiedObject( nonlinearShuntCompensatorPhase );
            if( result == null ) result = caseCimObjectWithID( nonlinearShuntCompensatorPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BASIC_INTERVAL_SCHEDULE: {
            BasicIntervalSchedule basicIntervalSchedule = ( BasicIntervalSchedule ) theEObject;
            T result = caseBasicIntervalSchedule( basicIntervalSchedule );
            if( result == null ) result = caseIdentifiedObject( basicIntervalSchedule );
            if( result == null ) result = caseCimObjectWithID( basicIntervalSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BASE_VOLTAGE: {
            BaseVoltage baseVoltage = ( BaseVoltage ) theEObject;
            T result = caseBaseVoltage( baseVoltage );
            if( result == null ) result = caseIdentifiedObject( baseVoltage );
            if( result == null ) result = caseCimObjectWithID( baseVoltage );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TAP_SCHEDULE: {
            TapSchedule tapSchedule = ( TapSchedule ) theEObject;
            T result = caseTapSchedule( tapSchedule );
            if( result == null ) result = caseSeasonDayTypeSchedule( tapSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( tapSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( tapSchedule );
            if( result == null ) result = caseIdentifiedObject( tapSchedule );
            if( result == null ) result = caseCimObjectWithID( tapSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DISCONNECTOR: {
            Disconnector disconnector = ( Disconnector ) theEObject;
            T result = caseDisconnector( disconnector );
            if( result == null ) result = caseSwitch( disconnector );
            if( result == null ) result = caseConductingEquipment( disconnector );
            if( result == null ) result = caseEquipment( disconnector );
            if( result == null ) result = casePowerSystemResource( disconnector );
            if( result == null ) result = caseIdentifiedObject( disconnector );
            if( result == null ) result = caseCimObjectWithID( disconnector );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SYNCHROCHECK_RELAY: {
            SynchrocheckRelay synchrocheckRelay = ( SynchrocheckRelay ) theEObject;
            T result = caseSynchrocheckRelay( synchrocheckRelay );
            if( result == null ) result = caseProtectionEquipment( synchrocheckRelay );
            if( result == null ) result = caseEquipment( synchrocheckRelay );
            if( result == null ) result = casePowerSystemResource( synchrocheckRelay );
            if( result == null ) result = caseIdentifiedObject( synchrocheckRelay );
            if( result == null ) result = caseCimObjectWithID( synchrocheckRelay );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GEOGRAPHICAL_REGION: {
            GeographicalRegion geographicalRegion = ( GeographicalRegion ) theEObject;
            T result = caseGeographicalRegion( geographicalRegion );
            if( result == null ) result = caseIdentifiedObject( geographicalRegion );
            if( result == null ) result = caseCimObjectWithID( geographicalRegion );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PHASE_TAP_CHANGER_TABLE: {
            PhaseTapChangerTable phaseTapChangerTable = ( PhaseTapChangerTable ) theEObject;
            T result = casePhaseTapChangerTable( phaseTapChangerTable );
            if( result == null ) result = caseIdentifiedObject( phaseTapChangerTable );
            if( result == null ) result = caseCimObjectWithID( phaseTapChangerTable );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.COMMUNICATION_LINK: {
            CommunicationLink communicationLink = ( CommunicationLink ) theEObject;
            T result = caseCommunicationLink( communicationLink );
            if( result == null ) result = casePowerSystemResource( communicationLink );
            if( result == null ) result = caseIdentifiedObject( communicationLink );
            if( result == null ) result = caseCimObjectWithID( communicationLink );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FREQUENCY_CONVERTER: {
            FrequencyConverter frequencyConverter = ( FrequencyConverter ) theEObject;
            T result = caseFrequencyConverter( frequencyConverter );
            if( result == null ) result = caseRegulatingCondEq( frequencyConverter );
            if( result == null ) result = caseConductingEquipment( frequencyConverter );
            if( result == null ) result = caseEquipment( frequencyConverter );
            if( result == null ) result = casePowerSystemResource( frequencyConverter );
            if( result == null ) result = caseIdentifiedObject( frequencyConverter );
            if( result == null ) result = caseCimObjectWithID( frequencyConverter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.OPERATIONAL_LIMIT_TYPE: {
            OperationalLimitType operationalLimitType = ( OperationalLimitType ) theEObject;
            T result = caseOperationalLimitType( operationalLimitType );
            if( result == null ) result = caseIdentifiedObject( operationalLimitType );
            if( result == null ) result = caseCimObjectWithID( operationalLimitType );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MEASUREMENT_VALUE_QUALITY: {
            MeasurementValueQuality measurementValueQuality = ( MeasurementValueQuality ) theEObject;
            T result = caseMeasurementValueQuality( measurementValueQuality );
            if( result == null ) result = caseQuality61850( measurementValueQuality );
            if( result == null ) result = caseCimObjectWithID( measurementValueQuality );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONNECTOR: {
            Connector connector = ( Connector ) theEObject;
            T result = caseConnector( connector );
            if( result == null ) result = caseConductingEquipment( connector );
            if( result == null ) result = caseEquipment( connector );
            if( result == null ) result = casePowerSystemResource( connector );
            if( result == null ) result = caseIdentifiedObject( connector );
            if( result == null ) result = caseCimObjectWithID( connector );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SUPERCRITICAL: {
            Supercritical supercritical = ( Supercritical ) theEObject;
            T result = caseSupercritical( supercritical );
            if( result == null ) result = caseFossilSteamSupply( supercritical );
            if( result == null ) result = caseSteamSupply( supercritical );
            if( result == null ) result = casePowerSystemResource( supercritical );
            if( result == null ) result = caseIdentifiedObject( supercritical );
            if( result == null ) result = caseCimObjectWithID( supercritical );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM_STYLE: {
            DiagramStyle diagramStyle = ( DiagramStyle ) theEObject;
            T result = caseDiagramStyle( diagramStyle );
            if( result == null ) result = caseIdentifiedObject( diagramStyle );
            if( result == null ) result = caseCimObjectWithID( diagramStyle );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EMISSION_CURVE: {
            EmissionCurve emissionCurve = ( EmissionCurve ) theEObject;
            T result = caseEmissionCurve( emissionCurve );
            if( result == null ) result = caseCurve( emissionCurve );
            if( result == null ) result = caseIdentifiedObject( emissionCurve );
            if( result == null ) result = caseCimObjectWithID( emissionCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TAILBAY_LOSS_CURVE: {
            TailbayLossCurve tailbayLossCurve = ( TailbayLossCurve ) theEObject;
            T result = caseTailbayLossCurve( tailbayLossCurve );
            if( result == null ) result = caseCurve( tailbayLossCurve );
            if( result == null ) result = caseIdentifiedObject( tailbayLossCurve );
            if( result == null ) result = caseCimObjectWithID( tailbayLossCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.APPARENT_POWER_LIMIT: {
            ApparentPowerLimit apparentPowerLimit = ( ApparentPowerLimit ) theEObject;
            T result = caseApparentPowerLimit( apparentPowerLimit );
            if( result == null ) result = caseOperationalLimit( apparentPowerLimit );
            if( result == null ) result = caseIdentifiedObject( apparentPowerLimit );
            if( result == null ) result = caseCimObjectWithID( apparentPowerLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FOSSIL_STEAM_SUPPLY: {
            FossilSteamSupply fossilSteamSupply = ( FossilSteamSupply ) theEObject;
            T result = caseFossilSteamSupply( fossilSteamSupply );
            if( result == null ) result = caseSteamSupply( fossilSteamSupply );
            if( result == null ) result = casePowerSystemResource( fossilSteamSupply );
            if( result == null ) result = caseIdentifiedObject( fossilSteamSupply );
            if( result == null ) result = caseCimObjectWithID( fossilSteamSupply );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ENERGY_SOURCE: {
            EnergySource energySource = ( EnergySource ) theEObject;
            T result = caseEnergySource( energySource );
            if( result == null ) result = caseConductingEquipment( energySource );
            if( result == null ) result = caseEquipment( energySource );
            if( result == null ) result = casePowerSystemResource( energySource );
            if( result == null ) result = caseIdentifiedObject( energySource );
            if( result == null ) result = caseCimObjectWithID( energySource );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM: {
            Diagram diagram = ( Diagram ) theEObject;
            T result = caseDiagram( diagram );
            if( result == null ) result = caseIdentifiedObject( diagram );
            if( result == null ) result = caseCimObjectWithID( diagram );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STRING_MEASUREMENT: {
            StringMeasurement stringMeasurement = ( StringMeasurement ) theEObject;
            T result = caseStringMeasurement( stringMeasurement );
            if( result == null ) result = caseMeasurement( stringMeasurement );
            if( result == null ) result = caseIdentifiedObject( stringMeasurement );
            if( result == null ) result = caseCimObjectWithID( stringMeasurement );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SHUTDOWN_CURVE: {
            ShutdownCurve shutdownCurve = ( ShutdownCurve ) theEObject;
            T result = caseShutdownCurve( shutdownCurve );
            if( result == null ) result = caseCurve( shutdownCurve );
            if( result == null ) result = caseIdentifiedObject( shutdownCurve );
            if( result == null ) result = caseCimObjectWithID( shutdownCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_PUMP_OP_SCHEDULE: {
            HydroPumpOpSchedule hydroPumpOpSchedule = ( HydroPumpOpSchedule ) theEObject;
            T result = caseHydroPumpOpSchedule( hydroPumpOpSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( hydroPumpOpSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( hydroPumpOpSchedule );
            if( result == null ) result = caseIdentifiedObject( hydroPumpOpSchedule );
            if( result == null ) result = caseCimObjectWithID( hydroPumpOpSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.START_RAMP_CURVE: {
            StartRampCurve startRampCurve = ( StartRampCurve ) theEObject;
            T result = caseStartRampCurve( startRampCurve );
            if( result == null ) result = caseCurve( startRampCurve );
            if( result == null ) result = caseIdentifiedObject( startRampCurve );
            if( result == null ) result = caseCimObjectWithID( startRampCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VS_CONVERTER: {
            VsConverter vsConverter = ( VsConverter ) theEObject;
            T result = caseVsConverter( vsConverter );
            if( result == null ) result = caseACDCConverter( vsConverter );
            if( result == null ) result = caseConductingEquipment( vsConverter );
            if( result == null ) result = caseEquipment( vsConverter );
            if( result == null ) result = casePowerSystemResource( vsConverter );
            if( result == null ) result = caseIdentifiedObject( vsConverter );
            if( result == null ) result = caseCimObjectWithID( vsConverter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.THERMAL_GENERATING_UNIT: {
            ThermalGeneratingUnit thermalGeneratingUnit = ( ThermalGeneratingUnit ) theEObject;
            T result = caseThermalGeneratingUnit( thermalGeneratingUnit );
            if( result == null ) result = caseGeneratingUnit( thermalGeneratingUnit );
            if( result == null ) result = caseEquipment( thermalGeneratingUnit );
            if( result == null ) result = casePowerSystemResource( thermalGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( thermalGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( thermalGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_BREAKER: {
            DCBreaker dcBreaker = ( DCBreaker ) theEObject;
            T result = caseDCBreaker( dcBreaker );
            if( result == null ) result = caseDCSwitch( dcBreaker );
            if( result == null ) result = caseDCConductingEquipment( dcBreaker );
            if( result == null ) result = caseEquipment( dcBreaker );
            if( result == null ) result = casePowerSystemResource( dcBreaker );
            if( result == null ) result = caseIdentifiedObject( dcBreaker );
            if( result == null ) result = caseCimObjectWithID( dcBreaker );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.REGULAR_INTERVAL_SCHEDULE: {
            RegularIntervalSchedule regularIntervalSchedule = ( RegularIntervalSchedule ) theEObject;
            T result = caseRegularIntervalSchedule( regularIntervalSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( regularIntervalSchedule );
            if( result == null ) result = caseIdentifiedObject( regularIntervalSchedule );
            if( result == null ) result = caseCimObjectWithID( regularIntervalSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ENERGY_CONSUMER_PHASE: {
            EnergyConsumerPhase energyConsumerPhase = ( EnergyConsumerPhase ) theEObject;
            T result = caseEnergyConsumerPhase( energyConsumerPhase );
            if( result == null ) result = casePowerSystemResource( energyConsumerPhase );
            if( result == null ) result = caseIdentifiedObject( energyConsumerPhase );
            if( result == null ) result = caseCimObjectWithID( energyConsumerPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FAULT_INDICATOR: {
            FaultIndicator faultIndicator = ( FaultIndicator ) theEObject;
            T result = caseFaultIndicator( faultIndicator );
            if( result == null ) result = caseAuxiliaryEquipment( faultIndicator );
            if( result == null ) result = caseEquipment( faultIndicator );
            if( result == null ) result = casePowerSystemResource( faultIndicator );
            if( result == null ) result = caseIdentifiedObject( faultIndicator );
            if( result == null ) result = caseCimObjectWithID( faultIndicator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_GENERATING_EFFICIENCY_CURVE: {
            HydroGeneratingEfficiencyCurve hydroGeneratingEfficiencyCurve = ( HydroGeneratingEfficiencyCurve ) theEObject;
            T result = caseHydroGeneratingEfficiencyCurve( hydroGeneratingEfficiencyCurve );
            if( result == null ) result = caseCurve( hydroGeneratingEfficiencyCurve );
            if( result == null ) result = caseIdentifiedObject( hydroGeneratingEfficiencyCurve );
            if( result == null ) result = caseCimObjectWithID( hydroGeneratingEfficiencyCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.EARTH_FAULT_COMPENSATOR: {
            EarthFaultCompensator earthFaultCompensator = ( EarthFaultCompensator ) theEObject;
            T result = caseEarthFaultCompensator( earthFaultCompensator );
            if( result == null ) result = caseConductingEquipment( earthFaultCompensator );
            if( result == null ) result = caseEquipment( earthFaultCompensator );
            if( result == null ) result = casePowerSystemResource( earthFaultCompensator );
            if( result == null ) result = caseIdentifiedObject( earthFaultCompensator );
            if( result == null ) result = caseCimObjectWithID( earthFaultCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_EQUIPMENT_CONTAINER: {
            DCEquipmentContainer dcEquipmentContainer = ( DCEquipmentContainer ) theEObject;
            T result = caseDCEquipmentContainer( dcEquipmentContainer );
            if( result == null ) result = caseEquipmentContainer( dcEquipmentContainer );
            if( result == null ) result = caseConnectivityNodeContainer( dcEquipmentContainer );
            if( result == null ) result = casePowerSystemResource( dcEquipmentContainer );
            if( result == null ) result = caseIdentifiedObject( dcEquipmentContainer );
            if( result == null ) result = caseCimObjectWithID( dcEquipmentContainer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SUBSTATION: {
            Substation substation = ( Substation ) theEObject;
            T result = caseSubstation( substation );
            if( result == null ) result = caseEquipmentContainer( substation );
            if( result == null ) result = caseConnectivityNodeContainer( substation );
            if( result == null ) result = casePowerSystemResource( substation );
            if( result == null ) result = caseIdentifiedObject( substation );
            if( result == null ) result = caseCimObjectWithID( substation );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.OPERATIONAL_LIMIT_SET: {
            OperationalLimitSet operationalLimitSet = ( OperationalLimitSet ) theEObject;
            T result = caseOperationalLimitSet( operationalLimitSet );
            if( result == null ) result = caseIdentifiedObject( operationalLimitSet );
            if( result == null ) result = caseCimObjectWithID( operationalLimitSet );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CURVE: {
            Curve curve = ( Curve ) theEObject;
            T result = caseCurve( curve );
            if( result == null ) result = caseIdentifiedObject( curve );
            if( result == null ) result = caseCimObjectWithID( curve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HYDRO_POWER_PLANT: {
            HydroPowerPlant hydroPowerPlant = ( HydroPowerPlant ) theEObject;
            T result = caseHydroPowerPlant( hydroPowerPlant );
            if( result == null ) result = casePowerSystemResource( hydroPowerPlant );
            if( result == null ) result = caseIdentifiedObject( hydroPowerPlant );
            if( result == null ) result = caseCimObjectWithID( hydroPowerPlant );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SURGE_ARRESTER: {
            SurgeArrester surgeArrester = ( SurgeArrester ) theEObject;
            T result = caseSurgeArrester( surgeArrester );
            if( result == null ) result = caseAuxiliaryEquipment( surgeArrester );
            if( result == null ) result = caseEquipment( surgeArrester );
            if( result == null ) result = casePowerSystemResource( surgeArrester );
            if( result == null ) result = caseIdentifiedObject( surgeArrester );
            if( result == null ) result = caseCimObjectWithID( surgeArrester );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MUTUAL_COUPLING: {
            MutualCoupling mutualCoupling = ( MutualCoupling ) theEObject;
            T result = caseMutualCoupling( mutualCoupling );
            if( result == null ) result = caseIdentifiedObject( mutualCoupling );
            if( result == null ) result = caseCimObjectWithID( mutualCoupling );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM_OBJECT_GLUE_POINT: {
            DiagramObjectGluePoint diagramObjectGluePoint = ( DiagramObjectGluePoint ) theEObject;
            T result = caseDiagramObjectGluePoint( diagramObjectGluePoint );
            if( result == null ) result = caseCimObjectWithID( diagramObjectGluePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTROL_AREA_GENERATING_UNIT: {
            ControlAreaGeneratingUnit controlAreaGeneratingUnit = ( ControlAreaGeneratingUnit ) theEObject;
            T result = caseControlAreaGeneratingUnit( controlAreaGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( controlAreaGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( controlAreaGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HEAT_RATE_CURVE: {
            HeatRateCurve heatRateCurve = ( HeatRateCurve ) theEObject;
            T result = caseHeatRateCurve( heatRateCurve );
            if( result == null ) result = caseCurve( heatRateCurve );
            if( result == null ) result = caseIdentifiedObject( heatRateCurve );
            if( result == null ) result = caseCimObjectWithID( heatRateCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.JUMPER: {
            Jumper jumper = ( Jumper ) theEObject;
            T result = caseJumper( jumper );
            if( result == null ) result = caseSwitch( jumper );
            if( result == null ) result = caseConductingEquipment( jumper );
            if( result == null ) result = caseEquipment( jumper );
            if( result == null ) result = casePowerSystemResource( jumper );
            if( result == null ) result = caseIdentifiedObject( jumper );
            if( result == null ) result = caseCimObjectWithID( jumper );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.INFLOW_FORECAST: {
            InflowForecast inflowForecast = ( InflowForecast ) theEObject;
            T result = caseInflowForecast( inflowForecast );
            if( result == null ) result = caseRegularIntervalSchedule( inflowForecast );
            if( result == null ) result = caseBasicIntervalSchedule( inflowForecast );
            if( result == null ) result = caseIdentifiedObject( inflowForecast );
            if( result == null ) result = caseCimObjectWithID( inflowForecast );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CUT: {
            Cut cut = ( Cut ) theEObject;
            T result = caseCut( cut );
            if( result == null ) result = caseSwitch( cut );
            if( result == null ) result = caseConductingEquipment( cut );
            if( result == null ) result = caseEquipment( cut );
            if( result == null ) result = casePowerSystemResource( cut );
            if( result == null ) result = caseIdentifiedObject( cut );
            if( result == null ) result = caseCimObjectWithID( cut );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONNECTIVITY_NODE: {
            ConnectivityNode connectivityNode = ( ConnectivityNode ) theEObject;
            T result = caseConnectivityNode( connectivityNode );
            if( result == null ) result = caseIdentifiedObject( connectivityNode );
            if( result == null ) result = caseCimObjectWithID( connectivityNode );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TOPOLOGICAL_NODE: {
            TopologicalNode topologicalNode = ( TopologicalNode ) theEObject;
            T result = caseTopologicalNode( topologicalNode );
            if( result == null ) result = caseIdentifiedObject( topologicalNode );
            if( result == null ) result = caseCimObjectWithID( topologicalNode );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PWR_STEAM_SUPPLY: {
            PWRSteamSupply pwrSteamSupply = ( PWRSteamSupply ) theEObject;
            T result = casePWRSteamSupply( pwrSteamSupply );
            if( result == null ) result = caseSteamSupply( pwrSteamSupply );
            if( result == null ) result = casePowerSystemResource( pwrSteamSupply );
            if( result == null ) result = caseIdentifiedObject( pwrSteamSupply );
            if( result == null ) result = caseCimObjectWithID( pwrSteamSupply );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FOSSIL_FUEL: {
            FossilFuel fossilFuel = ( FossilFuel ) theEObject;
            T result = caseFossilFuel( fossilFuel );
            if( result == null ) result = caseIdentifiedObject( fossilFuel );
            if( result == null ) result = caseCimObjectWithID( fossilFuel );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GROUNDING_IMPEDANCE: {
            GroundingImpedance groundingImpedance = ( GroundingImpedance ) theEObject;
            T result = caseGroundingImpedance( groundingImpedance );
            if( result == null ) result = caseEarthFaultCompensator( groundingImpedance );
            if( result == null ) result = caseConductingEquipment( groundingImpedance );
            if( result == null ) result = caseEquipment( groundingImpedance );
            if( result == null ) result = casePowerSystemResource( groundingImpedance );
            if( result == null ) result = caseIdentifiedObject( groundingImpedance );
            if( result == null ) result = caseCimObjectWithID( groundingImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LOAD_AREA: {
            LoadArea loadArea = ( LoadArea ) theEObject;
            T result = caseLoadArea( loadArea );
            if( result == null ) result = caseEnergyArea( loadArea );
            if( result == null ) result = caseIdentifiedObject( loadArea );
            if( result == null ) result = caseCimObjectWithID( loadArea );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SOLAR_GENERATING_UNIT: {
            SolarGeneratingUnit solarGeneratingUnit = ( SolarGeneratingUnit ) theEObject;
            T result = caseSolarGeneratingUnit( solarGeneratingUnit );
            if( result == null ) result = caseGeneratingUnit( solarGeneratingUnit );
            if( result == null ) result = caseEquipment( solarGeneratingUnit );
            if( result == null ) result = casePowerSystemResource( solarGeneratingUnit );
            if( result == null ) result = caseIdentifiedObject( solarGeneratingUnit );
            if( result == null ) result = caseCimObjectWithID( solarGeneratingUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ENERGY_AREA: {
            EnergyArea energyArea = ( EnergyArea ) theEObject;
            T result = caseEnergyArea( energyArea );
            if( result == null ) result = caseIdentifiedObject( energyArea );
            if( result == null ) result = caseCimObjectWithID( energyArea );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TOPOLOGICAL_ISLAND: {
            TopologicalIsland topologicalIsland = ( TopologicalIsland ) theEObject;
            T result = caseTopologicalIsland( topologicalIsland );
            if( result == null ) result = caseIdentifiedObject( topologicalIsland );
            if( result == null ) result = caseCimObjectWithID( topologicalIsland );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DISCRETE_COMMAND: {
            DiscreteCommand discreteCommand = ( DiscreteCommand ) theEObject;
            T result = caseDiscreteCommand( discreteCommand );
            if( result == null ) result = caseCommand( discreteCommand );
            if( result == null ) result = caseControl( discreteCommand );
            if( result == null ) result = caseIdentifiedObject( discreteCommand );
            if( result == null ) result = caseCimObjectWithID( discreteCommand );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_PHASE_POINT: {
            NonlinearShuntCompensatorPhasePoint nonlinearShuntCompensatorPhasePoint = ( NonlinearShuntCompensatorPhasePoint ) theEObject;
            T result = caseNonlinearShuntCompensatorPhasePoint( nonlinearShuntCompensatorPhasePoint );
            if( result == null ) result = caseCimObjectWithID( nonlinearShuntCompensatorPhasePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.MEASUREMENT_VALUE_SOURCE: {
            MeasurementValueSource measurementValueSource = ( MeasurementValueSource ) theEObject;
            T result = caseMeasurementValueSource( measurementValueSource );
            if( result == null ) result = caseIdentifiedObject( measurementValueSource );
            if( result == null ) result = caseCimObjectWithID( measurementValueSource );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STEAM_SENDOUT_SCHEDULE: {
            SteamSendoutSchedule steamSendoutSchedule = ( SteamSendoutSchedule ) theEObject;
            T result = caseSteamSendoutSchedule( steamSendoutSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( steamSendoutSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( steamSendoutSchedule );
            if( result == null ) result = caseIdentifiedObject( steamSendoutSchedule );
            if( result == null ) result = caseCimObjectWithID( steamSendoutSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TERMINAL: {
            Terminal terminal = ( Terminal ) theEObject;
            T result = caseTerminal( terminal );
            if( result == null ) result = caseACDCTerminal( terminal );
            if( result == null ) result = caseIdentifiedObject( terminal );
            if( result == null ) result = caseCimObjectWithID( terminal );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.IDENTIFIED_OBJECT: {
            IdentifiedObject identifiedObject = ( IdentifiedObject ) theEObject;
            T result = caseIdentifiedObject( identifiedObject );
            if( result == null ) result = caseCimObjectWithID( identifiedObject );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SEASON_DAY_TYPE_SCHEDULE: {
            SeasonDayTypeSchedule seasonDayTypeSchedule = ( SeasonDayTypeSchedule ) theEObject;
            T result = caseSeasonDayTypeSchedule( seasonDayTypeSchedule );
            if( result == null ) result = caseRegularIntervalSchedule( seasonDayTypeSchedule );
            if( result == null ) result = caseBasicIntervalSchedule( seasonDayTypeSchedule );
            if( result == null ) result = caseIdentifiedObject( seasonDayTypeSchedule );
            if( result == null ) result = caseCimObjectWithID( seasonDayTypeSchedule );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DC_CHOPPER: {
            DCChopper dcChopper = ( DCChopper ) theEObject;
            T result = caseDCChopper( dcChopper );
            if( result == null ) result = caseDCConductingEquipment( dcChopper );
            if( result == null ) result = caseEquipment( dcChopper );
            if( result == null ) result = casePowerSystemResource( dcChopper );
            if( result == null ) result = caseIdentifiedObject( dcChopper );
            if( result == null ) result = caseCimObjectWithID( dcChopper );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LINE_FAULT: {
            LineFault lineFault = ( LineFault ) theEObject;
            T result = caseLineFault( lineFault );
            if( result == null ) result = caseFault( lineFault );
            if( result == null ) result = caseIdentifiedObject( lineFault );
            if( result == null ) result = caseCimObjectWithID( lineFault );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CS_CONVERTER: {
            CsConverter csConverter = ( CsConverter ) theEObject;
            T result = caseCsConverter( csConverter );
            if( result == null ) result = caseACDCConverter( csConverter );
            if( result == null ) result = caseConductingEquipment( csConverter );
            if( result == null ) result = caseEquipment( csConverter );
            if( result == null ) result = casePowerSystemResource( csConverter );
            if( result == null ) result = caseIdentifiedObject( csConverter );
            if( result == null ) result = caseCimObjectWithID( csConverter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PER_LENGTH_IMPEDANCE: {
            PerLengthImpedance perLengthImpedance = ( PerLengthImpedance ) theEObject;
            T result = casePerLengthImpedance( perLengthImpedance );
            if( result == null ) result = casePerLengthLineParameter( perLengthImpedance );
            if( result == null ) result = caseIdentifiedObject( perLengthImpedance );
            if( result == null ) result = caseCimObjectWithID( perLengthImpedance );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.BASE_POWER: {
            BasePower basePower = ( BasePower ) theEObject;
            T result = caseBasePower( basePower );
            if( result == null ) result = caseIdentifiedObject( basePower );
            if( result == null ) result = caseCimObjectWithID( basePower );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_ELECTRONICS_UNIT: {
            PowerElectronicsUnit powerElectronicsUnit = ( PowerElectronicsUnit ) theEObject;
            T result = casePowerElectronicsUnit( powerElectronicsUnit );
            if( result == null ) result = caseEquipment( powerElectronicsUnit );
            if( result == null ) result = casePowerSystemResource( powerElectronicsUnit );
            if( result == null ) result = caseIdentifiedObject( powerElectronicsUnit );
            if( result == null ) result = caseCimObjectWithID( powerElectronicsUnit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DIAGRAM_OBJECT: {
            DiagramObject diagramObject = ( DiagramObject ) theEObject;
            T result = caseDiagramObject( diagramObject );
            if( result == null ) result = caseIdentifiedObject( diagramObject );
            if( result == null ) result = caseCimObjectWithID( diagramObject );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.DRUM_BOILER: {
            DrumBoiler drumBoiler = ( DrumBoiler ) theEObject;
            T result = caseDrumBoiler( drumBoiler );
            if( result == null ) result = caseFossilSteamSupply( drumBoiler );
            if( result == null ) result = caseSteamSupply( drumBoiler );
            if( result == null ) result = casePowerSystemResource( drumBoiler );
            if( result == null ) result = caseIdentifiedObject( drumBoiler );
            if( result == null ) result = caseCimObjectWithID( drumBoiler );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.FAULT: {
            Fault fault = ( Fault ) theEObject;
            T result = caseFault( fault );
            if( result == null ) result = caseIdentifiedObject( fault );
            if( result == null ) result = caseCimObjectWithID( fault );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.ROTATING_MACHINE: {
            RotatingMachine rotatingMachine = ( RotatingMachine ) theEObject;
            T result = caseRotatingMachine( rotatingMachine );
            if( result == null ) result = caseRegulatingCondEq( rotatingMachine );
            if( result == null ) result = caseConductingEquipment( rotatingMachine );
            if( result == null ) result = caseEquipment( rotatingMachine );
            if( result == null ) result = casePowerSystemResource( rotatingMachine );
            if( result == null ) result = caseIdentifiedObject( rotatingMachine );
            if( result == null ) result = caseCimObjectWithID( rotatingMachine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_TRANSFORMER_END: {
            PowerTransformerEnd powerTransformerEnd = ( PowerTransformerEnd ) theEObject;
            T result = casePowerTransformerEnd( powerTransformerEnd );
            if( result == null ) result = caseTransformerEnd( powerTransformerEnd );
            if( result == null ) result = caseIdentifiedObject( powerTransformerEnd );
            if( result == null ) result = caseCimObjectWithID( powerTransformerEnd );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STEAM_SUPPLY: {
            SteamSupply steamSupply = ( SteamSupply ) theEObject;
            T result = caseSteamSupply( steamSupply );
            if( result == null ) result = casePowerSystemResource( steamSupply );
            if( result == null ) result = caseIdentifiedObject( steamSupply );
            if( result == null ) result = caseCimObjectWithID( steamSupply );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POST_LINE_SENSOR: {
            PostLineSensor postLineSensor = ( PostLineSensor ) theEObject;
            T result = casePostLineSensor( postLineSensor );
            if( result == null ) result = caseSensor( postLineSensor );
            if( result == null ) result = caseAuxiliaryEquipment( postLineSensor );
            if( result == null ) result = caseEquipment( postLineSensor );
            if( result == null ) result = casePowerSystemResource( postLineSensor );
            if( result == null ) result = caseIdentifiedObject( postLineSensor );
            if( result == null ) result = caseCimObjectWithID( postLineSensor );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.STEAM_TURBINE: {
            SteamTurbine steamTurbine = ( SteamTurbine ) theEObject;
            T result = caseSteamTurbine( steamTurbine );
            if( result == null ) result = casePrimeMover( steamTurbine );
            if( result == null ) result = casePowerSystemResource( steamTurbine );
            if( result == null ) result = caseIdentifiedObject( steamTurbine );
            if( result == null ) result = caseCimObjectWithID( steamTurbine );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.RECLOSER: {
            Recloser recloser = ( Recloser ) theEObject;
            T result = caseRecloser( recloser );
            if( result == null ) result = caseProtectedSwitch( recloser );
            if( result == null ) result = caseSwitch( recloser );
            if( result == null ) result = caseConductingEquipment( recloser );
            if( result == null ) result = caseEquipment( recloser );
            if( result == null ) result = casePowerSystemResource( recloser );
            if( result == null ) result = caseIdentifiedObject( recloser );
            if( result == null ) result = caseCimObjectWithID( recloser );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SWITCH_PHASE: {
            SwitchPhase switchPhase = ( SwitchPhase ) theEObject;
            T result = caseSwitchPhase( switchPhase );
            if( result == null ) result = casePowerSystemResource( switchPhase );
            if( result == null ) result = caseIdentifiedObject( switchPhase );
            if( result == null ) result = caseCimObjectWithID( switchPhase );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.SWITCH: {
            fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Switch switch_ = ( fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Switch ) theEObject;
            T result = caseSwitch( switch_ );
            if( result == null ) result = caseConductingEquipment( switch_ );
            if( result == null ) result = caseEquipment( switch_ );
            if( result == null ) result = casePowerSystemResource( switch_ );
            if( result == null ) result = caseIdentifiedObject( switch_ );
            if( result == null ) result = caseCimObjectWithID( switch_ );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.GROUND_DISCONNECTOR: {
            GroundDisconnector groundDisconnector = ( GroundDisconnector ) theEObject;
            T result = caseGroundDisconnector( groundDisconnector );
            if( result == null ) result = caseSwitch( groundDisconnector );
            if( result == null ) result = caseConductingEquipment( groundDisconnector );
            if( result == null ) result = caseEquipment( groundDisconnector );
            if( result == null ) result = casePowerSystemResource( groundDisconnector );
            if( result == null ) result = caseIdentifiedObject( groundDisconnector );
            if( result == null ) result = caseCimObjectWithID( groundDisconnector );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.NAME_TYPE: {
            NameType nameType = ( NameType ) theEObject;
            T result = caseNameType( nameType );
            if( result == null ) result = caseCimObjectWithID( nameType );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTINGENCY_ELEMENT: {
            ContingencyElement contingencyElement = ( ContingencyElement ) theEObject;
            T result = caseContingencyElement( contingencyElement );
            if( result == null ) result = caseIdentifiedObject( contingencyElement );
            if( result == null ) result = caseCimObjectWithID( contingencyElement );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.HEAT_INPUT_CURVE: {
            HeatInputCurve heatInputCurve = ( HeatInputCurve ) theEObject;
            T result = caseHeatInputCurve( heatInputCurve );
            if( result == null ) result = caseCurve( heatInputCurve );
            if( result == null ) result = caseIdentifiedObject( heatInputCurve );
            if( result == null ) result = caseCimObjectWithID( heatInputCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.OPERATIONAL_LIMIT: {
            OperationalLimit operationalLimit = ( OperationalLimit ) theEObject;
            T result = caseOperationalLimit( operationalLimit );
            if( result == null ) result = caseIdentifiedObject( operationalLimit );
            if( result == null ) result = caseCimObjectWithID( operationalLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.START_MAIN_FUEL_CURVE: {
            StartMainFuelCurve startMainFuelCurve = ( StartMainFuelCurve ) theEObject;
            T result = caseStartMainFuelCurve( startMainFuelCurve );
            if( result == null ) result = caseCurve( startMainFuelCurve );
            if( result == null ) result = caseIdentifiedObject( startMainFuelCurve );
            if( result == null ) result = caseCimObjectWithID( startMainFuelCurve );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.PER_LENGTH_DC_LINE_PARAMETER: {
            PerLengthDCLineParameter perLengthDCLineParameter = ( PerLengthDCLineParameter ) theEObject;
            T result = casePerLengthDCLineParameter( perLengthDCLineParameter );
            if( result == null ) result = casePerLengthLineParameter( perLengthDCLineParameter );
            if( result == null ) result = caseIdentifiedObject( perLengthDCLineParameter );
            if( result == null ) result = caseCimObjectWithID( perLengthDCLineParameter );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.POWER_TRANSFORMER: {
            PowerTransformer powerTransformer = ( PowerTransformer ) theEObject;
            T result = casePowerTransformer( powerTransformer );
            if( result == null ) result = caseConductingEquipment( powerTransformer );
            if( result == null ) result = caseEquipment( powerTransformer );
            if( result == null ) result = casePowerSystemResource( powerTransformer );
            if( result == null ) result = caseIdentifiedObject( powerTransformer );
            if( result == null ) result = caseCimObjectWithID( powerTransformer );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.CONTROL: {
            Control control = ( Control ) theEObject;
            T result = caseControl( control );
            if( result == null ) result = caseIdentifiedObject( control );
            if( result == null ) result = caseCimObjectWithID( control );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.LINEAR_SHUNT_COMPENSATOR: {
            LinearShuntCompensator linearShuntCompensator = ( LinearShuntCompensator ) theEObject;
            T result = caseLinearShuntCompensator( linearShuntCompensator );
            if( result == null ) result = caseShuntCompensator( linearShuntCompensator );
            if( result == null ) result = caseRegulatingCondEq( linearShuntCompensator );
            if( result == null ) result = caseConductingEquipment( linearShuntCompensator );
            if( result == null ) result = caseEquipment( linearShuntCompensator );
            if( result == null ) result = casePowerSystemResource( linearShuntCompensator );
            if( result == null ) result = caseIdentifiedObject( linearShuntCompensator );
            if( result == null ) result = caseCimObjectWithID( linearShuntCompensator );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.TAP_CHANGER_TABLE_POINT: {
            TapChangerTablePoint tapChangerTablePoint = ( TapChangerTablePoint ) theEObject;
            T result = caseTapChangerTablePoint( tapChangerTablePoint );
            if( result == null ) result = caseCimObjectWithID( tapChangerTablePoint );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        case CimPackage.VOLTAGE_LIMIT: {
            VoltageLimit voltageLimit = ( VoltageLimit ) theEObject;
            T result = caseVoltageLimit( voltageLimit );
            if( result == null ) result = caseOperationalLimit( voltageLimit );
            if( result == null ) result = caseIdentifiedObject( voltageLimit );
            if( result == null ) result = caseCimObjectWithID( voltageLimit );
            if( result == null ) result = defaultCase( theEObject );
            return result;
        }
        default:
            return defaultCase( theEObject );
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Object With ID</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Object With ID</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCimObjectWithID( CimObjectWithID object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Date Time Interval</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Date Time Interval</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDateTimeInterval( DateTimeInterval object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Integer Quantity</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Integer Quantity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIntegerQuantity( IntegerQuantity object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Time Interval</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Time Interval</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTimeInterval( TimeInterval object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fault Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fault Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFaultImpedance( FaultImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Decimal Quantity</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Decimal Quantity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDecimalQuantity( DecimalQuantity object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Quantity</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Quantity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringQuantity( StringQuantity object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Date Interval</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Date Interval</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDateInterval( DateInterval object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Float Quantity</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Float Quantity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFloatQuantity( FloatQuantity object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Month Day Interval</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Month Day Interval</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMonthDayInterval( MonthDayInterval object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power System Resource</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power System Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerSystemResource( PowerSystemResource object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Voltage</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Voltage</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvVoltage( SvVoltage object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Non Conform Load</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Non Conform Load</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonConformLoad( NonConformLoad object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sub Geographical Region</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sub Geographical Region</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSubGeographicalRegion( SubGeographicalRegion object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Discrete Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Discrete Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiscreteValue( DiscreteValue object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Accumulator Limit Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Accumulator Limit Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccumulatorLimitSet( AccumulatorLimitSet object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Reservoir</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Reservoir</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReservoir( Reservoir object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Impedance Data</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Impedance Data</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseImpedanceData( PhaseImpedanceData object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Steam Supply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSteamSupply( SteamSupply object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>PSR Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PSR Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePSRType( PSRType object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Status</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Status</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvStatus( SvStatus object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Discrete Command</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Discrete Command</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiscreteCommand( DiscreteCommand object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Steam Sendout Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Steam Sendout Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSteamSendoutSchedule( SteamSendoutSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Energy Consumer Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Energy Consumer Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEnergyConsumerPhase( EnergyConsumerPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Busbar</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Busbar</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCBusbar( DCBusbar object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Linear Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Linear Shunt Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLinearShuntCompensator( LinearShuntCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Per Length DC Line Parameter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Per Length DC Line Parameter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePerLengthDCLineParameter( PerLengthDCLineParameter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operational Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operational Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOperationalLimit( OperationalLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Surge Arrester</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Surge Arrester</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSurgeArrester( SurgeArrester object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>AC Line Segment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>AC Line Segment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseACLineSegment( ACLineSegment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Recloser</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Recloser</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRecloser( Recloser object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Voltage Control Zone</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Voltage Control Zone</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVoltageControlZone( VoltageControlZone object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Wind Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Wind Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseWindGeneratingUnit( WindGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Voltage Level</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Voltage Level</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVoltageLevel( VoltageLevel object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Transformer End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Transformer End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerTransformerEnd( PowerTransformerEnd object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IEC61970CIM Version</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IEC61970CIM Version</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIEC61970CIMVersion( IEC61970CIMVersion object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Synchronous Machine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Synchronous Machine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSynchronousMachine( SynchronousMachine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Energy Source</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Energy Source</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEnergySource( EnergySource object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Cut Zone</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Cut Zone</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerCutZone( PowerCutZone object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ACDC Converter DC Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ACDC Converter DC Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseACDCConverterDCTerminal( ACDCConverterDCTerminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>State Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>State Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStateVariable( StateVariable object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>AC Line Segment Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>AC Line Segment Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseACLineSegmentPhase( ACLineSegmentPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Protected Switch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Protected Switch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseProtectedSwitch( ProtectedSwitch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Energy Area</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Energy Area</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEnergyArea( EnergyArea object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Startup Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Startup Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStartupModel( StartupModel object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Combined Cycle Plant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Combined Cycle Plant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCombinedCyclePlant( CombinedCyclePlant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePlant( Plant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operational Limit Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operational Limit Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOperationalLimitSet( OperationalLimitSet object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Contingency</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Contingency</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContingency( Contingency object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Shunt</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Shunt</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCShunt( DCShunt object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Season Day Type Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Season Day Type Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSeasonDayTypeSchedule( SeasonDayTypeSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Line</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Line</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLine( Line object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Shunt Compensator Sections</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Shunt Compensator Sections</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvShuntCompensatorSections( SvShuntCompensatorSections object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonlinearShuntCompensator( NonlinearShuntCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Measurement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Measurement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMeasurement( Measurement object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Pump Op Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Pump Op Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroPumpOpSchedule( HydroPumpOpSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ratio Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ratio Tap Changer Table Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRatioTapChangerTablePoint( RatioTapChangerTablePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTerminal( Terminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer Star Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer Star Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerStarImpedance( TransformerStarImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Remote Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Remote Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRemotePoint( RemotePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Switch Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Switch Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSwitchSchedule( SwitchSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Analog</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Analog</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAnalog( Analog object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Earth Fault Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Earth Fault Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEarthFaultCompensator( EarthFaultCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>BWR Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>BWR Steam Supply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBWRSteamSupply( BWRSteamSupply object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>PWR Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PWR Steam Supply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePWRSteamSupply( PWRSteamSupply object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Style</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Style</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramStyle( DiagramStyle object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Remote Source</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Remote Source</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRemoteSource( RemoteSource object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Base Power</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Base Power</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBasePower( BasePower object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Name Type Authority</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Name Type Authority</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNameTypeAuthority( NameTypeAuthority object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Analog Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Analog Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAnalogLimit( AnalogLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Ground</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Ground</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCGround( DCGround object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquipment( Equipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Substation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Substation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSubstation( Substation object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Command</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Command</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCommand( Command object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tie Flow</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tie Flow</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTieFlow( TieFlow object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Linear</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Linear</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerLinear( PhaseTapChangerLinear object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nuclear Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nuclear Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNuclearGeneratingUnit( NuclearGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Turbine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Turbine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroTurbine( HydroTurbine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Branch Group Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Branch Group Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBranchGroupTerminal( BranchGroupTerminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Base Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Base Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCBaseTerminal( DCBaseTerminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Current Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Current Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCurrentLimit( CurrentLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regulating Control</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regulating Control</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegulatingControl( RegulatingControl object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sub Load Area</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sub Load Area</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSubLoadArea( SubLoadArea object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Base Frequency</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Base Frequency</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBaseFrequency( BaseFrequency object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Text Diagram Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Text Diagram Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTextDiagramObject( TextDiagramObject object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Start Main Fuel Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Start Main Fuel Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStartMainFuelCurve( StartMainFuelCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Control Area Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Control Area Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseControlAreaGeneratingUnit( ControlAreaGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonlinearShuntCompensatorPoint( NonlinearShuntCompensatorPoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Disconnector</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Disconnector</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDisconnector( Disconnector object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tap Changer Table Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTapChangerTablePoint( TapChangerTablePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ground</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ground</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGround( Ground object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Electronics Wind Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Electronics Wind Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerElectronicsWindUnit( PowerElectronicsWindUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Remote Control</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Remote Control</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRemoteControl( RemoteControl object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Table</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Table</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerTable( PhaseTapChangerTable object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Topological Island</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Topological Island</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTopologicalIsland( TopologicalIsland object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fuel Allocation Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fuel Allocation Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFuelAllocationSchedule( FuelAllocationSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Tap Step</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Tap Step</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvTapStep( SvTapStep object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Accumulator Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Accumulator Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccumulatorLimit( AccumulatorLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Clamp</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Clamp</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseClamp( Clamp object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Accumulator Reset</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Accumulator Reset</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccumulatorReset( AccumulatorReset object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Electronics Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Electronics Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerElectronicsUnit( PowerElectronicsUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Prime Mover</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Prime Mover</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePrimeMover( PrimeMover object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Asynchronous Machine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Asynchronous Machine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAsynchronousMachine( AsynchronousMachine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Heat Rate Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Heat Rate Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHeatRateCurve( HeatRateCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Inflow Forecast</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Inflow Forecast</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseInflowForecast( InflowForecast object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Curve Data</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Curve Data</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCurveData( CurveData object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Line</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Line</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCLine( DCLine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Day Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Day Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDayType( DayType object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Per Length Phase Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Per Length Phase Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePerLengthPhaseImpedance( PerLengthPhaseImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ACDC Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ACDC Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseACDCTerminal( ACDCTerminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Object Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Object Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramObjectPoint( DiagramObjectPoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Accumulator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Accumulator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccumulator( Accumulator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Communication Link</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Communication Link</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCommunicationLink( CommunicationLink object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equivalent Shunt</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equivalent Shunt</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquivalentShunt( EquivalentShunt object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ratio Tap Changer Table</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ratio Tap Changer Table</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRatioTapChangerTable( RatioTapChangerTable object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fossil Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fossil Steam Supply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFossilSteamSupply( FossilSteamSupply object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Target Level Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Target Level Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTargetLevelSchedule( TargetLevelSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Visibility Layer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Visibility Layer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVisibilityLayer( VisibilityLayer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Measurement Value Quality</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Measurement Value Quality</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMeasurementValueQuality( MeasurementValueQuality object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Gen Unit Op Cost Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Gen Unit Op Cost Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGenUnitOpCostCurve( GenUnitOpCostCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Load Response Characteristic</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Load Response Characteristic</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLoadResponseCharacteristic( LoadResponseCharacteristic object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Static Var Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Static Var Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStaticVarCompensator( StaticVarCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Protection Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Protection Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseProtectionEquipment( ProtectionEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Limit Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Limit Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLimitSet( LimitSet object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Potential Transformer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Potential Transformer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePotentialTransformer( PotentialTransformer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Non Conform Load Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Non Conform Load Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonConformLoadSchedule( NonConformLoadSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Reactive Capability Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Reactive Capability Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReactiveCapabilityCurve( ReactiveCapabilityCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonlinearShuntCompensatorPhase( NonlinearShuntCompensatorPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagram( Diagram object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Symmetrical</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Symmetrical</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerSymmetrical( PhaseTapChangerSymmetrical object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Composite Switch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Composite Switch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCompositeSwitch( CompositeSwitch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bay</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bay</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBay( Bay object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Apparent Power Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Apparent Power Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApparentPowerLimit( ApparentPowerLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fault Indicator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fault Indicator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFaultIndicator( FaultIndicator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Alt Generating Unit Meas</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Alt Generating Unit Meas</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAltGeneratingUnitMeas( AltGeneratingUnitMeas object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Remote Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Remote Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRemoteUnit( RemoteUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Non Conform Load Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Non Conform Load Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonConformLoadGroup( NonConformLoadGroup object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Voltage Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Voltage Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVoltageLimit( VoltageLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Conform Load Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Conform Load Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConformLoadGroup( ConformLoadGroup object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Line Segment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Line Segment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCLineSegment( DCLineSegment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Measurement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Measurement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringMeasurement( StringMeasurement object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Start Ign Fuel Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Start Ign Fuel Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStartIgnFuelCurve( StartIgnFuelCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLimit( Limit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Generating Efficiency Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Generating Efficiency Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroGeneratingEfficiencyCurve( HydroGeneratingEfficiencyCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Incremental Heat Rate Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Incremental Heat Rate Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIncrementalHeatRateCurve( IncrementalHeatRateCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Penstock Loss Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Penstock Loss Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePenstockLossCurve( PenstockLossCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Switch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Switch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCSwitch( DCSwitch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Irregular Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Irregular Interval Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIrregularIntervalSchedule( IrregularIntervalSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CAES Plant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CAES Plant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCAESPlant( CAESPlant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramObject( DiagramObject object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Petersen Coil</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Petersen Coil</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePetersenCoil( PetersenCoil object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCNode( DCNode object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Conform Load Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Conform Load Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConformLoadSchedule( ConformLoadSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equipment Fault</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equipment Fault</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquipmentFault( EquipmentFault object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Synchrocheck Relay</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Synchrocheck Relay</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSynchrocheckRelay( SynchrocheckRelay object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Object Style</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Object Style</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramObjectStyle( DiagramObjectStyle object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Quality61850</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Quality61850</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseQuality61850( Quality61850 object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Table Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerTablePoint( PhaseTapChangerTablePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ground Disconnector</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ground Disconnector</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGroundDisconnector( GroundDisconnector object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer Tank End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer Tank End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerTankEnd( TransformerTankEnd object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Linear Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Linear Shunt Compensator Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLinearShuntCompensatorPhase( LinearShuntCompensatorPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Shunt Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseShuntCompensator( ShuntCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Reclose Sequence</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Reclose Sequence</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRecloseSequence( RecloseSequence object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Series Compensator</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Series Compensator</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSeriesCompensator( SeriesCompensator object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Name Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Name Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNameType( NameType object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer Mesh Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer Mesh Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerMeshImpedance( TransformerMeshImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Drum Boiler</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Drum Boiler</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDrumBoiler( DrumBoiler object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Measurement Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Measurement Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMeasurementValue( MeasurementValue object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Set Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Set Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSetPoint( SetPoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fault</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fault</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFault( Fault object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Steam Turbine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Steam Turbine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSteamTurbine( SteamTurbine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Rotating Machine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Rotating Machine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRotatingMachine( RotatingMachine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Heat Recovery Boiler</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Heat Recovery Boiler</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHeatRecoveryBoiler( HeatRecoveryBoiler object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Connectivity Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Connectivity Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConnectivityNode( ConnectivityNode object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cut</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cut</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCut( Cut object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operational Limit Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operational Limit Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOperationalLimitType( OperationalLimitType object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Irregular Time Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Irregular Time Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIrregularTimePoint( IrregularTimePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fuse</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fuse</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFuse( Fuse object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Identified Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Identified Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIdentifiedObject( IdentifiedObject object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Conducting Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Conducting Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConductingEquipment( ConductingEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Grounding Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Grounding Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGroundingImpedance( GroundingImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Line Fault</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Line Fault</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLineFault( LineFault object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Air Compressor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Air Compressor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAirCompressor( AirCompressor object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bus Name Marker</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bus Name Marker</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusNameMarker( BusNameMarker object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Shunt Compensator Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseShuntCompensatorPhase( ShuntCompensatorPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Contingency Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Contingency Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContingencyElement( ContingencyElement object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regulating Cond Eq</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regulating Cond Eq</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegulatingCondEq( RegulatingCondEq object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Analog Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Analog Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAnalogValue( AnalogValue object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Analog Limit Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Analog Limit Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAnalogLimitSet( AnalogLimitSet object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Current Relay</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Current Relay</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCurrentRelay( CurrentRelay object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Heat Input Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Heat Input Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHeatInputCurve( HeatInputCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Control</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Control</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseControl( Control object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Pump</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Pump</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroPump( HydroPump object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Auxiliary Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Auxiliary Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAuxiliaryEquipment( AuxiliaryEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Connectivity Node Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Connectivity Node Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConnectivityNodeContainer( ConnectivityNodeContainer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Alt Tie Meas</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Alt Tie Meas</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAltTieMeas( AltTieMeas object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Switch Phase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Switch Phase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSwitchPhase( SwitchPhase object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Supercritical</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Supercritical</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSupercritical( Supercritical object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fault Cause Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fault Cause Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFaultCauseType( FaultCauseType object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Current Transformer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Current Transformer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCurrentTransformer( CurrentTransformer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Vs Capability Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Vs Capability Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsCapabilityCurve( VsCapabilityCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regular Time Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regular Time Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegularTimePoint( RegularTimePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Frequency Converter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Frequency Converter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFrequencyConverter( FrequencyConverter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Conform Load</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Conform Load</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConformLoad( ConformLoad object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Power Plant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Power Plant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroPowerPlant( HydroPowerPlant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Gross To Net Active Power Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Gross To Net Active Power Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGrossToNetActivePowerCurve( GrossToNetActivePowerCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Fossil Fuel</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fossil Fuel</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFossilFuel( FossilFuel object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Measurement Value Source</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Measurement Value Source</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMeasurementValueSource( MeasurementValueSource object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Shutdown Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Shutdown Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseShutdownCurve( ShutdownCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer Core Admittance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer Core Admittance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerCoreAdmittance( TransformerCoreAdmittance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Energy Consumer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Energy Consumer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEnergyConsumer( EnergyConsumer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Busbar Section</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Busbar Section</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusbarSection( BusbarSection object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equipment Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equipment Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquipmentContainer( EquipmentContainer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Breaker</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Breaker</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCBreaker( DCBreaker object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Measurement Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Measurement Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringMeasurementValue( StringMeasurementValue object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Raise Lower Command</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Raise Lower Command</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRaiseLowerCommand( RaiseLowerCommand object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tap Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tap Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTapSchedule( TapSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value To Alias</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value To Alias</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValueToAlias( ValueToAlias object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Object Glue Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Object Glue Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramObjectGluePoint( DiagramObjectGluePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Switch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Switch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSwitch( fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Switch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Breaker</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Breaker</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBreaker( Breaker object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Terminal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Terminal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCTerminal( DCTerminal object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sectionaliser</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sectionaliser</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSectionaliser( Sectionaliser object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Injection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Injection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvInjection( SvInjection object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Gen Unit Op Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Gen Unit Op Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGenUnitOpSchedule( GenUnitOpSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer End</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer End</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerEnd( TransformerEnd object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Load Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Load Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLoadGroup( LoadGroup object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Analog Control</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Analog Control</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAnalogControl( AnalogControl object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Converter Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Converter Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCConverterUnit( DCConverterUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tap Changer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTapChanger( TapChanger object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cs Converter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cs Converter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCsConverter( CsConverter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value Alias Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value Alias Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValueAliasSet( ValueAliasSet object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Series Device</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Series Device</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCSeriesDevice( DCSeriesDevice object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Wave Trap</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Wave Trap</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseWaveTrap( WaveTrap object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Name</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Name</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseName( Name object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Control Area</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Control Area</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseControlArea( ControlArea object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equivalent Injection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equivalent Injection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquivalentInjection( EquivalentInjection object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Topological Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Topological Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCTopologicalNode( DCTopologicalNode object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Chopper</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Chopper</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCChopper( DCChopper object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Per Length Sequence Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Per Length Sequence Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePerLengthSequenceImpedance( PerLengthSequenceImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Start Ramp Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Start Ramp Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStartRampCurve( StartRampCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ACDC Converter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ACDC Converter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseACDCConverter( ACDCConverter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Transformer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Transformer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerTransformer( PowerTransformer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Active Power Limit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Active Power Limit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseActivePowerLimit( ActivePowerLimit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Emission Account</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Emission Account</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmissionAccount( EmissionAccount object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCurve( Curve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Power Electronics Connection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Power Electronics Connection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePowerElectronicsConnection( PowerElectronicsConnection object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Level Vs Volume Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Level Vs Volume Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLevelVsVolumeCurve( LevelVsVolumeCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ratio Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ratio Tap Changer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRatioTapChanger( RatioTapChanger object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Hydro Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Hydro Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHydroGeneratingUnit( HydroGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operating Participant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operating Participant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOperatingParticipant( OperatingParticipant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Per Length Impedance</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Per Length Impedance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePerLengthImpedance( PerLengthImpedance object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Base Voltage</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Base Voltage</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBaseVoltage( BaseVoltage object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Season</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Season</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSeason( Season object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Load Area</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Load Area</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLoadArea( LoadArea object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Contingency Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Contingency Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContingencyEquipment( ContingencyEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Combustion Turbine</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Combustion Turbine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCombustionTurbine( CombustionTurbine object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Geographical Region</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Geographical Region</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGeographicalRegion( GeographicalRegion object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Basic Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Basic Interval Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBasicIntervalSchedule( BasicIntervalSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Disconnector</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Disconnector</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCDisconnector( DCDisconnector object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cogeneration Plant</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cogeneration Plant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCogenerationPlant( CogenerationPlant object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Non Linear</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Non Linear</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerNonLinear( PhaseTapChangerNonLinear object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Emission Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Emission Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmissionCurve( EmissionCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equivalent Branch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equivalent Branch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquivalentBranch( EquivalentBranch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Post Line Sensor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Post Line Sensor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePostLineSensor( PostLineSensor object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regulation Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regulation Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegulationSchedule( RegulationSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Thermal Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Thermal Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseThermalGeneratingUnit( ThermalGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGeneratingUnit( GeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tap Changer Control</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tap Changer Control</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTapChangerControl( TapChangerControl object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equivalent Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equivalent Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquivalentEquipment( EquivalentEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Connector</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Connector</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConnector( Connector object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regular Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regular Interval Schedule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegularIntervalSchedule( RegularIntervalSchedule object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChanger( PhaseTapChanger object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Discrete</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Discrete</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiscrete( Discrete object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Topological Island</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Topological Island</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCTopologicalIsland( DCTopologicalIsland object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sensor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sensor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSensor( Sensor object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sv Power Flow</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sv Power Flow</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSvPowerFlow( SvPowerFlow object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Branch Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Branch Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBranchGroup( BranchGroup object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Topological Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Topological Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTopologicalNode( TopologicalNode object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Subcritical</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Subcritical</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSubcritical( Subcritical object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Accumulator Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Accumulator Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccumulatorValue( AccumulatorValue object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Station Supply</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Station Supply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStationSupply( StationSupply object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equivalent Network</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equivalent Network</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquivalentNetwork( EquivalentNetwork object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Solar Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Solar Generating Unit</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSolarGeneratingUnit( SolarGeneratingUnit object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Equipment Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Equipment Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCEquipmentContainer( DCEquipmentContainer object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DC Conducting Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DC Conducting Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDCConductingEquipment( DCConductingEquipment object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Jumper</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Jumper</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseJumper( Jumper object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Per Length Line Parameter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Per Length Line Parameter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePerLengthLineParameter( PerLengthLineParameter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Asymmetrical</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Asymmetrical</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerAsymmetrical( PhaseTapChangerAsymmetrical object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Reporting Super Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Reporting Super Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReportingSuperGroup( ReportingSuperGroup object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Vs Converter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Vs Converter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsConverter( VsConverter object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CT Temp Active Power Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CT Temp Active Power Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCTTempActivePowerCurve( CTTempActivePowerCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Operating Share</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Operating Share</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOperatingShare( OperatingShare object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Phase Tap Changer Tabular</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Phase Tap Changer Tabular</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhaseTapChangerTabular( PhaseTapChangerTabular object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Phase Point</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nonlinear Shunt Compensator Phase Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonlinearShuntCompensatorPhasePoint( NonlinearShuntCompensatorPhasePoint object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Junction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Junction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseJunction( Junction object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transformer Tank</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transformer Tank</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransformerTank( TransformerTank object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Tailbay Loss Curve</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Tailbay Loss Curve</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTailbayLossCurve( TailbayLossCurve object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Mutual Coupling</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Mutual Coupling</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMutualCoupling( MutualCoupling object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Load Break Switch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Load Break Switch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLoadBreakSwitch( LoadBreakSwitch object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Conductor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Conductor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConductor( Conductor object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>External Network Injection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>External Network Injection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExternalNetworkInjection( ExternalNetworkInjection object ) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase( EObject object ) {
        return null;
    }

} //CimSwitch
