/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimPackage
 * @generated
 */
public class CimAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static CimPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CimAdapterFactory() {
        if( modelPackage == null ) {
            modelPackage = CimPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType( Object object ) {
        if( object == modelPackage ) {
            return true;
        }
        if( object instanceof EObject ) {
            return ( ( EObject ) object ).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected CimSwitch< Adapter > modelSwitch = new CimSwitch< Adapter >() {
        @Override
        public Adapter caseCimObjectWithID( CimObjectWithID object ) {
            return createCimObjectWithIDAdapter();
        }

        @Override
        public Adapter caseFaultImpedance( FaultImpedance object ) {
            return createFaultImpedanceAdapter();
        }

        @Override
        public Adapter caseFloatQuantity( FloatQuantity object ) {
            return createFloatQuantityAdapter();
        }

        @Override
        public Adapter caseDateTimeInterval( DateTimeInterval object ) {
            return createDateTimeIntervalAdapter();
        }

        @Override
        public Adapter caseTimeInterval( TimeInterval object ) {
            return createTimeIntervalAdapter();
        }

        @Override
        public Adapter caseDecimalQuantity( DecimalQuantity object ) {
            return createDecimalQuantityAdapter();
        }

        @Override
        public Adapter caseStringQuantity( StringQuantity object ) {
            return createStringQuantityAdapter();
        }

        @Override
        public Adapter caseMonthDayInterval( MonthDayInterval object ) {
            return createMonthDayIntervalAdapter();
        }

        @Override
        public Adapter caseDateInterval( DateInterval object ) {
            return createDateIntervalAdapter();
        }

        @Override
        public Adapter caseIntegerQuantity( IntegerQuantity object ) {
            return createIntegerQuantityAdapter();
        }

        @Override
        public Adapter caseLoadResponseCharacteristic( LoadResponseCharacteristic object ) {
            return createLoadResponseCharacteristicAdapter();
        }

        @Override
        public Adapter caseGenUnitOpCostCurve( GenUnitOpCostCurve object ) {
            return createGenUnitOpCostCurveAdapter();
        }

        @Override
        public Adapter casePotentialTransformer( PotentialTransformer object ) {
            return createPotentialTransformerAdapter();
        }

        @Override
        public Adapter caseDCConductingEquipment( DCConductingEquipment object ) {
            return createDCConductingEquipmentAdapter();
        }

        @Override
        public Adapter caseRecloseSequence( RecloseSequence object ) {
            return createRecloseSequenceAdapter();
        }

        @Override
        public Adapter caseRatioTapChanger( RatioTapChanger object ) {
            return createRatioTapChangerAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerTablePoint( PhaseTapChangerTablePoint object ) {
            return createPhaseTapChangerTablePointAdapter();
        }

        @Override
        public Adapter caseTieFlow( TieFlow object ) {
            return createTieFlowAdapter();
        }

        @Override
        public Adapter caseVoltageControlZone( VoltageControlZone object ) {
            return createVoltageControlZoneAdapter();
        }

        @Override
        public Adapter caseAnalog( Analog object ) {
            return createAnalogAdapter();
        }

        @Override
        public Adapter caseJunction( Junction object ) {
            return createJunctionAdapter();
        }

        @Override
        public Adapter caseControlArea( ControlArea object ) {
            return createControlAreaAdapter();
        }

        @Override
        public Adapter caseValueToAlias( ValueToAlias object ) {
            return createValueToAliasAdapter();
        }

        @Override
        public Adapter caseHydroTurbine( HydroTurbine object ) {
            return createHydroTurbineAdapter();
        }

        @Override
        public Adapter caseWaveTrap( WaveTrap object ) {
            return createWaveTrapAdapter();
        }

        @Override
        public Adapter caseAltTieMeas( AltTieMeas object ) {
            return createAltTieMeasAdapter();
        }

        @Override
        public Adapter casePSRType( PSRType object ) {
            return createPSRTypeAdapter();
        }

        @Override
        public Adapter caseRemoteSource( RemoteSource object ) {
            return createRemoteSourceAdapter();
        }

        @Override
        public Adapter caseEquivalentInjection( EquivalentInjection object ) {
            return createEquivalentInjectionAdapter();
        }

        @Override
        public Adapter caseSvTapStep( SvTapStep object ) {
            return createSvTapStepAdapter();
        }

        @Override
        public Adapter caseProtectionEquipment( ProtectionEquipment object ) {
            return createProtectionEquipmentAdapter();
        }

        @Override
        public Adapter caseMeasurement( Measurement object ) {
            return createMeasurementAdapter();
        }

        @Override
        public Adapter casePowerSystemResource( PowerSystemResource object ) {
            return createPowerSystemResourceAdapter();
        }

        @Override
        public Adapter caseBWRSteamSupply( BWRSteamSupply object ) {
            return createBWRSteamSupplyAdapter();
        }

        @Override
        public Adapter caseDCBusbar( DCBusbar object ) {
            return createDCBusbarAdapter();
        }

        @Override
        public Adapter caseDiscrete( Discrete object ) {
            return createDiscreteAdapter();
        }

        @Override
        public Adapter caseConnectivityNodeContainer( ConnectivityNodeContainer object ) {
            return createConnectivityNodeContainerAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerAsymmetrical( PhaseTapChangerAsymmetrical object ) {
            return createPhaseTapChangerAsymmetricalAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerNonLinear( PhaseTapChangerNonLinear object ) {
            return createPhaseTapChangerNonLinearAdapter();
        }

        @Override
        public Adapter casePrimeMover( PrimeMover object ) {
            return createPrimeMoverAdapter();
        }

        @Override
        public Adapter caseNonConformLoadSchedule( NonConformLoadSchedule object ) {
            return createNonConformLoadScheduleAdapter();
        }

        @Override
        public Adapter caseDCLine( DCLine object ) {
            return createDCLineAdapter();
        }

        @Override
        public Adapter caseCurrentTransformer( CurrentTransformer object ) {
            return createCurrentTransformerAdapter();
        }

        @Override
        public Adapter caseRemoteUnit( RemoteUnit object ) {
            return createRemoteUnitAdapter();
        }

        @Override
        public Adapter caseHydroGeneratingUnit( HydroGeneratingUnit object ) {
            return createHydroGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseBranchGroup( BranchGroup object ) {
            return createBranchGroupAdapter();
        }

        @Override
        public Adapter caseRemotePoint( RemotePoint object ) {
            return createRemotePointAdapter();
        }

        @Override
        public Adapter casePowerElectronicsWindUnit( PowerElectronicsWindUnit object ) {
            return createPowerElectronicsWindUnitAdapter();
        }

        @Override
        public Adapter caseBay( Bay object ) {
            return createBayAdapter();
        }

        @Override
        public Adapter caseName( Name object ) {
            return createNameAdapter();
        }

        @Override
        public Adapter caseLoadBreakSwitch( LoadBreakSwitch object ) {
            return createLoadBreakSwitchAdapter();
        }

        @Override
        public Adapter caseBaseFrequency( BaseFrequency object ) {
            return createBaseFrequencyAdapter();
        }

        @Override
        public Adapter caseTransformerCoreAdmittance( TransformerCoreAdmittance object ) {
            return createTransformerCoreAdmittanceAdapter();
        }

        @Override
        public Adapter caseDCSeriesDevice( DCSeriesDevice object ) {
            return createDCSeriesDeviceAdapter();
        }

        @Override
        public Adapter caseVoltageLevel( VoltageLevel object ) {
            return createVoltageLevelAdapter();
        }

        @Override
        public Adapter caseDCGround( DCGround object ) {
            return createDCGroundAdapter();
        }

        @Override
        public Adapter caseSubGeographicalRegion( SubGeographicalRegion object ) {
            return createSubGeographicalRegionAdapter();
        }

        @Override
        public Adapter caseCogenerationPlant( CogenerationPlant object ) {
            return createCogenerationPlantAdapter();
        }

        @Override
        public Adapter caseFaultCauseType( FaultCauseType object ) {
            return createFaultCauseTypeAdapter();
        }

        @Override
        public Adapter caseDCLineSegment( DCLineSegment object ) {
            return createDCLineSegmentAdapter();
        }

        @Override
        public Adapter caseEquipmentContainer( EquipmentContainer object ) {
            return createEquipmentContainerAdapter();
        }

        @Override
        public Adapter caseConductor( Conductor object ) {
            return createConductorAdapter();
        }

        @Override
        public Adapter caseWindGeneratingUnit( WindGeneratingUnit object ) {
            return createWindGeneratingUnitAdapter();
        }

        @Override
        public Adapter casePenstockLossCurve( PenstockLossCurve object ) {
            return createPenstockLossCurveAdapter();
        }

        @Override
        public Adapter caseVisibilityLayer( VisibilityLayer object ) {
            return createVisibilityLayerAdapter();
        }

        @Override
        public Adapter caseDCTopologicalIsland( DCTopologicalIsland object ) {
            return createDCTopologicalIslandAdapter();
        }

        @Override
        public Adapter caseCombinedCyclePlant( CombinedCyclePlant object ) {
            return createCombinedCyclePlantAdapter();
        }

        @Override
        public Adapter caseCompositeSwitch( CompositeSwitch object ) {
            return createCompositeSwitchAdapter();
        }

        @Override
        public Adapter caseAuxiliaryEquipment( AuxiliaryEquipment object ) {
            return createAuxiliaryEquipmentAdapter();
        }

        @Override
        public Adapter caseLimitSet( LimitSet object ) {
            return createLimitSetAdapter();
        }

        @Override
        public Adapter caseNonConformLoad( NonConformLoad object ) {
            return createNonConformLoadAdapter();
        }

        @Override
        public Adapter caseEquipment( Equipment object ) {
            return createEquipmentAdapter();
        }

        @Override
        public Adapter caseQuality61850( Quality61850 object ) {
            return createQuality61850Adapter();
        }

        @Override
        public Adapter caseTransformerTankEnd( TransformerTankEnd object ) {
            return createTransformerTankEndAdapter();
        }

        @Override
        public Adapter caseEquivalentEquipment( EquivalentEquipment object ) {
            return createEquivalentEquipmentAdapter();
        }

        @Override
        public Adapter caseTapChangerControl( TapChangerControl object ) {
            return createTapChangerControlAdapter();
        }

        @Override
        public Adapter caseRemoteControl( RemoteControl object ) {
            return createRemoteControlAdapter();
        }

        @Override
        public Adapter caseConductingEquipment( ConductingEquipment object ) {
            return createConductingEquipmentAdapter();
        }

        @Override
        public Adapter caseCTTempActivePowerCurve( CTTempActivePowerCurve object ) {
            return createCTTempActivePowerCurveAdapter();
        }

        @Override
        public Adapter caseIrregularIntervalSchedule( IrregularIntervalSchedule object ) {
            return createIrregularIntervalScheduleAdapter();
        }

        @Override
        public Adapter caseAnalogControl( AnalogControl object ) {
            return createAnalogControlAdapter();
        }

        @Override
        public Adapter caseCurrentLimit( CurrentLimit object ) {
            return createCurrentLimitAdapter();
        }

        @Override
        public Adapter casePhaseTapChanger( PhaseTapChanger object ) {
            return createPhaseTapChangerAdapter();
        }

        @Override
        public Adapter caseAirCompressor( AirCompressor object ) {
            return createAirCompressorAdapter();
        }

        @Override
        public Adapter caseSvInjection( SvInjection object ) {
            return createSvInjectionAdapter();
        }

        @Override
        public Adapter caseSetPoint( SetPoint object ) {
            return createSetPointAdapter();
        }

        @Override
        public Adapter caseRegulatingControl( RegulatingControl object ) {
            return createRegulatingControlAdapter();
        }

        @Override
        public Adapter caseAccumulatorLimit( AccumulatorLimit object ) {
            return createAccumulatorLimitAdapter();
        }

        @Override
        public Adapter caseFuelAllocationSchedule( FuelAllocationSchedule object ) {
            return createFuelAllocationScheduleAdapter();
        }

        @Override
        public Adapter caseSynchronousMachine( SynchronousMachine object ) {
            return createSynchronousMachineAdapter();
        }

        @Override
        public Adapter caseSectionaliser( Sectionaliser object ) {
            return createSectionaliserAdapter();
        }

        @Override
        public Adapter caseDCBaseTerminal( DCBaseTerminal object ) {
            return createDCBaseTerminalAdapter();
        }

        @Override
        public Adapter caseReportingSuperGroup( ReportingSuperGroup object ) {
            return createReportingSuperGroupAdapter();
        }

        @Override
        public Adapter caseRegulatingCondEq( RegulatingCondEq object ) {
            return createRegulatingCondEqAdapter();
        }

        @Override
        public Adapter caseStateVariable( StateVariable object ) {
            return createStateVariableAdapter();
        }

        @Override
        public Adapter casePowerElectronicsConnection( PowerElectronicsConnection object ) {
            return createPowerElectronicsConnectionAdapter();
        }

        @Override
        public Adapter casePerLengthSequenceImpedance( PerLengthSequenceImpedance object ) {
            return createPerLengthSequenceImpedanceAdapter();
        }

        @Override
        public Adapter caseCommand( Command object ) {
            return createCommandAdapter();
        }

        @Override
        public Adapter caseFuse( Fuse object ) {
            return createFuseAdapter();
        }

        @Override
        public Adapter casePowerCutZone( PowerCutZone object ) {
            return createPowerCutZoneAdapter();
        }

        @Override
        public Adapter caseNonConformLoadGroup( NonConformLoadGroup object ) {
            return createNonConformLoadGroupAdapter();
        }

        @Override
        public Adapter caseAccumulatorReset( AccumulatorReset object ) {
            return createAccumulatorResetAdapter();
        }

        @Override
        public Adapter caseEquipmentFault( EquipmentFault object ) {
            return createEquipmentFaultAdapter();
        }

        @Override
        public Adapter caseTapChanger( TapChanger object ) {
            return createTapChangerAdapter();
        }

        @Override
        public Adapter caseReactiveCapabilityCurve( ReactiveCapabilityCurve object ) {
            return createReactiveCapabilityCurveAdapter();
        }

        @Override
        public Adapter caseACDCConverter( ACDCConverter object ) {
            return createACDCConverterAdapter();
        }

        @Override
        public Adapter caseNuclearGeneratingUnit( NuclearGeneratingUnit object ) {
            return createNuclearGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseContingencyEquipment( ContingencyEquipment object ) {
            return createContingencyEquipmentAdapter();
        }

        @Override
        public Adapter caseSvStatus( SvStatus object ) {
            return createSvStatusAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerTabular( PhaseTapChangerTabular object ) {
            return createPhaseTapChangerTabularAdapter();
        }

        @Override
        public Adapter caseSensor( Sensor object ) {
            return createSensorAdapter();
        }

        @Override
        public Adapter caseShuntCompensatorPhase( ShuntCompensatorPhase object ) {
            return createShuntCompensatorPhaseAdapter();
        }

        @Override
        public Adapter caseConformLoadGroup( ConformLoadGroup object ) {
            return createConformLoadGroupAdapter();
        }

        @Override
        public Adapter caseOperatingParticipant( OperatingParticipant object ) {
            return createOperatingParticipantAdapter();
        }

        @Override
        public Adapter caseAccumulatorLimitSet( AccumulatorLimitSet object ) {
            return createAccumulatorLimitSetAdapter();
        }

        @Override
        public Adapter caseDCNode( DCNode object ) {
            return createDCNodeAdapter();
        }

        @Override
        public Adapter caseTransformerStarImpedance( TransformerStarImpedance object ) {
            return createTransformerStarImpedanceAdapter();
        }

        @Override
        public Adapter caseSubcritical( Subcritical object ) {
            return createSubcriticalAdapter();
        }

        @Override
        public Adapter caseActivePowerLimit( ActivePowerLimit object ) {
            return createActivePowerLimitAdapter();
        }

        @Override
        public Adapter caseRegulationSchedule( RegulationSchedule object ) {
            return createRegulationScheduleAdapter();
        }

        @Override
        public Adapter caseGround( Ground object ) {
            return createGroundAdapter();
        }

        @Override
        public Adapter casePetersenCoil( PetersenCoil object ) {
            return createPetersenCoilAdapter();
        }

        @Override
        public Adapter caseLoadGroup( LoadGroup object ) {
            return createLoadGroupAdapter();
        }

        @Override
        public Adapter caseGrossToNetActivePowerCurve( GrossToNetActivePowerCurve object ) {
            return createGrossToNetActivePowerCurveAdapter();
        }

        @Override
        public Adapter caseVsCapabilityCurve( VsCapabilityCurve object ) {
            return createVsCapabilityCurveAdapter();
        }

        @Override
        public Adapter caseACDCTerminal( ACDCTerminal object ) {
            return createACDCTerminalAdapter();
        }

        @Override
        public Adapter caseClamp( Clamp object ) {
            return createClampAdapter();
        }

        @Override
        public Adapter caseTargetLevelSchedule( TargetLevelSchedule object ) {
            return createTargetLevelScheduleAdapter();
        }

        @Override
        public Adapter caseTransformerTank( TransformerTank object ) {
            return createTransformerTankAdapter();
        }

        @Override
        public Adapter caseGenUnitOpSchedule( GenUnitOpSchedule object ) {
            return createGenUnitOpScheduleAdapter();
        }

        @Override
        public Adapter caseACDCConverterDCTerminal( ACDCConverterDCTerminal object ) {
            return createACDCConverterDCTerminalAdapter();
        }

        @Override
        public Adapter caseTextDiagramObject( TextDiagramObject object ) {
            return createTextDiagramObjectAdapter();
        }

        @Override
        public Adapter caseSvShuntCompensatorSections( SvShuntCompensatorSections object ) {
            return createSvShuntCompensatorSectionsAdapter();
        }

        @Override
        public Adapter caseProtectedSwitch( ProtectedSwitch object ) {
            return createProtectedSwitchAdapter();
        }

        @Override
        public Adapter caseStartIgnFuelCurve( StartIgnFuelCurve object ) {
            return createStartIgnFuelCurveAdapter();
        }

        @Override
        public Adapter caseDiagramObjectPoint( DiagramObjectPoint object ) {
            return createDiagramObjectPointAdapter();
        }

        @Override
        public Adapter caseOperatingShare( OperatingShare object ) {
            return createOperatingShareAdapter();
        }

        @Override
        public Adapter caseNonlinearShuntCompensatorPoint( NonlinearShuntCompensatorPoint object ) {
            return createNonlinearShuntCompensatorPointAdapter();
        }

        @Override
        public Adapter caseAnalogLimitSet( AnalogLimitSet object ) {
            return createAnalogLimitSetAdapter();
        }

        @Override
        public Adapter caseContingency( Contingency object ) {
            return createContingencyAdapter();
        }

        @Override
        public Adapter caseDCTerminal( DCTerminal object ) {
            return createDCTerminalAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerSymmetrical( PhaseTapChangerSymmetrical object ) {
            return createPhaseTapChangerSymmetricalAdapter();
        }

        @Override
        public Adapter caseTransformerEnd( TransformerEnd object ) {
            return createTransformerEndAdapter();
        }

        @Override
        public Adapter caseBranchGroupTerminal( BranchGroupTerminal object ) {
            return createBranchGroupTerminalAdapter();
        }

        @Override
        public Adapter caseDiscreteValue( DiscreteValue object ) {
            return createDiscreteValueAdapter();
        }

        @Override
        public Adapter caseStaticVarCompensator( StaticVarCompensator object ) {
            return createStaticVarCompensatorAdapter();
        }

        @Override
        public Adapter caseAltGeneratingUnitMeas( AltGeneratingUnitMeas object ) {
            return createAltGeneratingUnitMeasAdapter();
        }

        @Override
        public Adapter caseLimit( Limit object ) {
            return createLimitAdapter();
        }

        @Override
        public Adapter caseNonlinearShuntCompensator( NonlinearShuntCompensator object ) {
            return createNonlinearShuntCompensatorAdapter();
        }

        @Override
        public Adapter caseConformLoadSchedule( ConformLoadSchedule object ) {
            return createConformLoadScheduleAdapter();
        }

        @Override
        public Adapter caseDiagramObjectStyle( DiagramObjectStyle object ) {
            return createDiagramObjectStyleAdapter();
        }

        @Override
        public Adapter caseBusbarSection( BusbarSection object ) {
            return createBusbarSectionAdapter();
        }

        @Override
        public Adapter caseEquivalentNetwork( EquivalentNetwork object ) {
            return createEquivalentNetworkAdapter();
        }

        @Override
        public Adapter caseEnergyConsumer( EnergyConsumer object ) {
            return createEnergyConsumerAdapter();
        }

        @Override
        public Adapter caseCurrentRelay( CurrentRelay object ) {
            return createCurrentRelayAdapter();
        }

        @Override
        public Adapter caseLevelVsVolumeCurve( LevelVsVolumeCurve object ) {
            return createLevelVsVolumeCurveAdapter();
        }

        @Override
        public Adapter caseNameTypeAuthority( NameTypeAuthority object ) {
            return createNameTypeAuthorityAdapter();
        }

        @Override
        public Adapter casePlant( Plant object ) {
            return createPlantAdapter();
        }

        @Override
        public Adapter caseCurveData( CurveData object ) {
            return createCurveDataAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerLinear( PhaseTapChangerLinear object ) {
            return createPhaseTapChangerLinearAdapter();
        }

        @Override
        public Adapter casePerLengthLineParameter( PerLengthLineParameter object ) {
            return createPerLengthLineParameterAdapter();
        }

        @Override
        public Adapter caseRaiseLowerCommand( RaiseLowerCommand object ) {
            return createRaiseLowerCommandAdapter();
        }

        @Override
        public Adapter caseDCConverterUnit( DCConverterUnit object ) {
            return createDCConverterUnitAdapter();
        }

        @Override
        public Adapter caseRegularTimePoint( RegularTimePoint object ) {
            return createRegularTimePointAdapter();
        }

        @Override
        public Adapter caseValueAliasSet( ValueAliasSet object ) {
            return createValueAliasSetAdapter();
        }

        @Override
        public Adapter caseAccumulatorValue( AccumulatorValue object ) {
            return createAccumulatorValueAdapter();
        }

        @Override
        public Adapter caseMeasurementValue( MeasurementValue object ) {
            return createMeasurementValueAdapter();
        }

        @Override
        public Adapter caseHydroPump( HydroPump object ) {
            return createHydroPumpAdapter();
        }

        @Override
        public Adapter caseDCSwitch( DCSwitch object ) {
            return createDCSwitchAdapter();
        }

        @Override
        public Adapter caseSvPowerFlow( SvPowerFlow object ) {
            return createSvPowerFlowAdapter();
        }

        @Override
        public Adapter caseSwitchSchedule( SwitchSchedule object ) {
            return createSwitchScheduleAdapter();
        }

        @Override
        public Adapter caseRatioTapChangerTablePoint( RatioTapChangerTablePoint object ) {
            return createRatioTapChangerTablePointAdapter();
        }

        @Override
        public Adapter caseReservoir( Reservoir object ) {
            return createReservoirAdapter();
        }

        @Override
        public Adapter caseEquivalentShunt( EquivalentShunt object ) {
            return createEquivalentShuntAdapter();
        }

        @Override
        public Adapter caseSeason( Season object ) {
            return createSeasonAdapter();
        }

        @Override
        public Adapter caseSeriesCompensator( SeriesCompensator object ) {
            return createSeriesCompensatorAdapter();
        }

        @Override
        public Adapter caseHeatRecoveryBoiler( HeatRecoveryBoiler object ) {
            return createHeatRecoveryBoilerAdapter();
        }

        @Override
        public Adapter caseExternalNetworkInjection( ExternalNetworkInjection object ) {
            return createExternalNetworkInjectionAdapter();
        }

        @Override
        public Adapter caseTransformerMeshImpedance( TransformerMeshImpedance object ) {
            return createTransformerMeshImpedanceAdapter();
        }

        @Override
        public Adapter caseSvVoltage( SvVoltage object ) {
            return createSvVoltageAdapter();
        }

        @Override
        public Adapter caseIEC61970CIMVersion( IEC61970CIMVersion object ) {
            return createIEC61970CIMVersionAdapter();
        }

        @Override
        public Adapter caseStringMeasurementValue( StringMeasurementValue object ) {
            return createStringMeasurementValueAdapter();
        }

        @Override
        public Adapter caseAnalogValue( AnalogValue object ) {
            return createAnalogValueAdapter();
        }

        @Override
        public Adapter caseRatioTapChangerTable( RatioTapChangerTable object ) {
            return createRatioTapChangerTableAdapter();
        }

        @Override
        public Adapter caseIrregularTimePoint( IrregularTimePoint object ) {
            return createIrregularTimePointAdapter();
        }

        @Override
        public Adapter caseACLineSegmentPhase( ACLineSegmentPhase object ) {
            return createACLineSegmentPhaseAdapter();
        }

        @Override
        public Adapter caseConformLoad( ConformLoad object ) {
            return createConformLoadAdapter();
        }

        @Override
        public Adapter caseACLineSegment( ACLineSegment object ) {
            return createACLineSegmentAdapter();
        }

        @Override
        public Adapter caseStartupModel( StartupModel object ) {
            return createStartupModelAdapter();
        }

        @Override
        public Adapter caseEquivalentBranch( EquivalentBranch object ) {
            return createEquivalentBranchAdapter();
        }

        @Override
        public Adapter caseLine( Line object ) {
            return createLineAdapter();
        }

        @Override
        public Adapter caseBusNameMarker( BusNameMarker object ) {
            return createBusNameMarkerAdapter();
        }

        @Override
        public Adapter caseDCTopologicalNode( DCTopologicalNode object ) {
            return createDCTopologicalNodeAdapter();
        }

        @Override
        public Adapter caseDCDisconnector( DCDisconnector object ) {
            return createDCDisconnectorAdapter();
        }

        @Override
        public Adapter caseDCShunt( DCShunt object ) {
            return createDCShuntAdapter();
        }

        @Override
        public Adapter casePhaseImpedanceData( PhaseImpedanceData object ) {
            return createPhaseImpedanceDataAdapter();
        }

        @Override
        public Adapter caseIncrementalHeatRateCurve( IncrementalHeatRateCurve object ) {
            return createIncrementalHeatRateCurveAdapter();
        }

        @Override
        public Adapter caseSubLoadArea( SubLoadArea object ) {
            return createSubLoadAreaAdapter();
        }

        @Override
        public Adapter caseDayType( DayType object ) {
            return createDayTypeAdapter();
        }

        @Override
        public Adapter caseCombustionTurbine( CombustionTurbine object ) {
            return createCombustionTurbineAdapter();
        }

        @Override
        public Adapter caseBreaker( Breaker object ) {
            return createBreakerAdapter();
        }

        @Override
        public Adapter caseGeneratingUnit( GeneratingUnit object ) {
            return createGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseEmissionAccount( EmissionAccount object ) {
            return createEmissionAccountAdapter();
        }

        @Override
        public Adapter caseAsynchronousMachine( AsynchronousMachine object ) {
            return createAsynchronousMachineAdapter();
        }

        @Override
        public Adapter caseStationSupply( StationSupply object ) {
            return createStationSupplyAdapter();
        }

        @Override
        public Adapter caseAccumulator( Accumulator object ) {
            return createAccumulatorAdapter();
        }

        @Override
        public Adapter caseCAESPlant( CAESPlant object ) {
            return createCAESPlantAdapter();
        }

        @Override
        public Adapter casePerLengthPhaseImpedance( PerLengthPhaseImpedance object ) {
            return createPerLengthPhaseImpedanceAdapter();
        }

        @Override
        public Adapter caseShuntCompensator( ShuntCompensator object ) {
            return createShuntCompensatorAdapter();
        }

        @Override
        public Adapter caseLinearShuntCompensatorPhase( LinearShuntCompensatorPhase object ) {
            return createLinearShuntCompensatorPhaseAdapter();
        }

        @Override
        public Adapter caseAnalogLimit( AnalogLimit object ) {
            return createAnalogLimitAdapter();
        }

        @Override
        public Adapter caseNonlinearShuntCompensatorPhase( NonlinearShuntCompensatorPhase object ) {
            return createNonlinearShuntCompensatorPhaseAdapter();
        }

        @Override
        public Adapter caseBasicIntervalSchedule( BasicIntervalSchedule object ) {
            return createBasicIntervalScheduleAdapter();
        }

        @Override
        public Adapter caseBaseVoltage( BaseVoltage object ) {
            return createBaseVoltageAdapter();
        }

        @Override
        public Adapter caseTapSchedule( TapSchedule object ) {
            return createTapScheduleAdapter();
        }

        @Override
        public Adapter caseDisconnector( Disconnector object ) {
            return createDisconnectorAdapter();
        }

        @Override
        public Adapter caseSynchrocheckRelay( SynchrocheckRelay object ) {
            return createSynchrocheckRelayAdapter();
        }

        @Override
        public Adapter caseGeographicalRegion( GeographicalRegion object ) {
            return createGeographicalRegionAdapter();
        }

        @Override
        public Adapter casePhaseTapChangerTable( PhaseTapChangerTable object ) {
            return createPhaseTapChangerTableAdapter();
        }

        @Override
        public Adapter caseCommunicationLink( CommunicationLink object ) {
            return createCommunicationLinkAdapter();
        }

        @Override
        public Adapter caseFrequencyConverter( FrequencyConverter object ) {
            return createFrequencyConverterAdapter();
        }

        @Override
        public Adapter caseOperationalLimitType( OperationalLimitType object ) {
            return createOperationalLimitTypeAdapter();
        }

        @Override
        public Adapter caseMeasurementValueQuality( MeasurementValueQuality object ) {
            return createMeasurementValueQualityAdapter();
        }

        @Override
        public Adapter caseConnector( Connector object ) {
            return createConnectorAdapter();
        }

        @Override
        public Adapter caseSupercritical( Supercritical object ) {
            return createSupercriticalAdapter();
        }

        @Override
        public Adapter caseDiagramStyle( DiagramStyle object ) {
            return createDiagramStyleAdapter();
        }

        @Override
        public Adapter caseEmissionCurve( EmissionCurve object ) {
            return createEmissionCurveAdapter();
        }

        @Override
        public Adapter caseTailbayLossCurve( TailbayLossCurve object ) {
            return createTailbayLossCurveAdapter();
        }

        @Override
        public Adapter caseApparentPowerLimit( ApparentPowerLimit object ) {
            return createApparentPowerLimitAdapter();
        }

        @Override
        public Adapter caseFossilSteamSupply( FossilSteamSupply object ) {
            return createFossilSteamSupplyAdapter();
        }

        @Override
        public Adapter caseEnergySource( EnergySource object ) {
            return createEnergySourceAdapter();
        }

        @Override
        public Adapter caseDiagram( Diagram object ) {
            return createDiagramAdapter();
        }

        @Override
        public Adapter caseStringMeasurement( StringMeasurement object ) {
            return createStringMeasurementAdapter();
        }

        @Override
        public Adapter caseShutdownCurve( ShutdownCurve object ) {
            return createShutdownCurveAdapter();
        }

        @Override
        public Adapter caseHydroPumpOpSchedule( HydroPumpOpSchedule object ) {
            return createHydroPumpOpScheduleAdapter();
        }

        @Override
        public Adapter caseStartRampCurve( StartRampCurve object ) {
            return createStartRampCurveAdapter();
        }

        @Override
        public Adapter caseVsConverter( VsConverter object ) {
            return createVsConverterAdapter();
        }

        @Override
        public Adapter caseThermalGeneratingUnit( ThermalGeneratingUnit object ) {
            return createThermalGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseDCBreaker( DCBreaker object ) {
            return createDCBreakerAdapter();
        }

        @Override
        public Adapter caseRegularIntervalSchedule( RegularIntervalSchedule object ) {
            return createRegularIntervalScheduleAdapter();
        }

        @Override
        public Adapter caseEnergyConsumerPhase( EnergyConsumerPhase object ) {
            return createEnergyConsumerPhaseAdapter();
        }

        @Override
        public Adapter caseFaultIndicator( FaultIndicator object ) {
            return createFaultIndicatorAdapter();
        }

        @Override
        public Adapter caseHydroGeneratingEfficiencyCurve( HydroGeneratingEfficiencyCurve object ) {
            return createHydroGeneratingEfficiencyCurveAdapter();
        }

        @Override
        public Adapter caseEarthFaultCompensator( EarthFaultCompensator object ) {
            return createEarthFaultCompensatorAdapter();
        }

        @Override
        public Adapter caseDCEquipmentContainer( DCEquipmentContainer object ) {
            return createDCEquipmentContainerAdapter();
        }

        @Override
        public Adapter caseSubstation( Substation object ) {
            return createSubstationAdapter();
        }

        @Override
        public Adapter caseOperationalLimitSet( OperationalLimitSet object ) {
            return createOperationalLimitSetAdapter();
        }

        @Override
        public Adapter caseCurve( Curve object ) {
            return createCurveAdapter();
        }

        @Override
        public Adapter caseHydroPowerPlant( HydroPowerPlant object ) {
            return createHydroPowerPlantAdapter();
        }

        @Override
        public Adapter caseSurgeArrester( SurgeArrester object ) {
            return createSurgeArresterAdapter();
        }

        @Override
        public Adapter caseMutualCoupling( MutualCoupling object ) {
            return createMutualCouplingAdapter();
        }

        @Override
        public Adapter caseDiagramObjectGluePoint( DiagramObjectGluePoint object ) {
            return createDiagramObjectGluePointAdapter();
        }

        @Override
        public Adapter caseControlAreaGeneratingUnit( ControlAreaGeneratingUnit object ) {
            return createControlAreaGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseHeatRateCurve( HeatRateCurve object ) {
            return createHeatRateCurveAdapter();
        }

        @Override
        public Adapter caseJumper( Jumper object ) {
            return createJumperAdapter();
        }

        @Override
        public Adapter caseInflowForecast( InflowForecast object ) {
            return createInflowForecastAdapter();
        }

        @Override
        public Adapter caseCut( Cut object ) {
            return createCutAdapter();
        }

        @Override
        public Adapter caseConnectivityNode( ConnectivityNode object ) {
            return createConnectivityNodeAdapter();
        }

        @Override
        public Adapter caseTopologicalNode( TopologicalNode object ) {
            return createTopologicalNodeAdapter();
        }

        @Override
        public Adapter casePWRSteamSupply( PWRSteamSupply object ) {
            return createPWRSteamSupplyAdapter();
        }

        @Override
        public Adapter caseFossilFuel( FossilFuel object ) {
            return createFossilFuelAdapter();
        }

        @Override
        public Adapter caseGroundingImpedance( GroundingImpedance object ) {
            return createGroundingImpedanceAdapter();
        }

        @Override
        public Adapter caseLoadArea( LoadArea object ) {
            return createLoadAreaAdapter();
        }

        @Override
        public Adapter caseSolarGeneratingUnit( SolarGeneratingUnit object ) {
            return createSolarGeneratingUnitAdapter();
        }

        @Override
        public Adapter caseEnergyArea( EnergyArea object ) {
            return createEnergyAreaAdapter();
        }

        @Override
        public Adapter caseTopologicalIsland( TopologicalIsland object ) {
            return createTopologicalIslandAdapter();
        }

        @Override
        public Adapter caseDiscreteCommand( DiscreteCommand object ) {
            return createDiscreteCommandAdapter();
        }

        @Override
        public Adapter caseNonlinearShuntCompensatorPhasePoint( NonlinearShuntCompensatorPhasePoint object ) {
            return createNonlinearShuntCompensatorPhasePointAdapter();
        }

        @Override
        public Adapter caseMeasurementValueSource( MeasurementValueSource object ) {
            return createMeasurementValueSourceAdapter();
        }

        @Override
        public Adapter caseSteamSendoutSchedule( SteamSendoutSchedule object ) {
            return createSteamSendoutScheduleAdapter();
        }

        @Override
        public Adapter caseTerminal( Terminal object ) {
            return createTerminalAdapter();
        }

        @Override
        public Adapter caseIdentifiedObject( IdentifiedObject object ) {
            return createIdentifiedObjectAdapter();
        }

        @Override
        public Adapter caseSeasonDayTypeSchedule( SeasonDayTypeSchedule object ) {
            return createSeasonDayTypeScheduleAdapter();
        }

        @Override
        public Adapter caseDCChopper( DCChopper object ) {
            return createDCChopperAdapter();
        }

        @Override
        public Adapter caseLineFault( LineFault object ) {
            return createLineFaultAdapter();
        }

        @Override
        public Adapter caseCsConverter( CsConverter object ) {
            return createCsConverterAdapter();
        }

        @Override
        public Adapter casePerLengthImpedance( PerLengthImpedance object ) {
            return createPerLengthImpedanceAdapter();
        }

        @Override
        public Adapter caseBasePower( BasePower object ) {
            return createBasePowerAdapter();
        }

        @Override
        public Adapter casePowerElectronicsUnit( PowerElectronicsUnit object ) {
            return createPowerElectronicsUnitAdapter();
        }

        @Override
        public Adapter caseDiagramObject( DiagramObject object ) {
            return createDiagramObjectAdapter();
        }

        @Override
        public Adapter caseDrumBoiler( DrumBoiler object ) {
            return createDrumBoilerAdapter();
        }

        @Override
        public Adapter caseFault( Fault object ) {
            return createFaultAdapter();
        }

        @Override
        public Adapter caseRotatingMachine( RotatingMachine object ) {
            return createRotatingMachineAdapter();
        }

        @Override
        public Adapter casePowerTransformerEnd( PowerTransformerEnd object ) {
            return createPowerTransformerEndAdapter();
        }

        @Override
        public Adapter caseSteamSupply( SteamSupply object ) {
            return createSteamSupplyAdapter();
        }

        @Override
        public Adapter casePostLineSensor( PostLineSensor object ) {
            return createPostLineSensorAdapter();
        }

        @Override
        public Adapter caseSteamTurbine( SteamTurbine object ) {
            return createSteamTurbineAdapter();
        }

        @Override
        public Adapter caseRecloser( Recloser object ) {
            return createRecloserAdapter();
        }

        @Override
        public Adapter caseSwitchPhase( SwitchPhase object ) {
            return createSwitchPhaseAdapter();
        }

        @Override
        public Adapter caseSwitch( Switch object ) {
            return createSwitchAdapter();
        }

        @Override
        public Adapter caseGroundDisconnector( GroundDisconnector object ) {
            return createGroundDisconnectorAdapter();
        }

        @Override
        public Adapter caseNameType( NameType object ) {
            return createNameTypeAdapter();
        }

        @Override
        public Adapter caseContingencyElement( ContingencyElement object ) {
            return createContingencyElementAdapter();
        }

        @Override
        public Adapter caseHeatInputCurve( HeatInputCurve object ) {
            return createHeatInputCurveAdapter();
        }

        @Override
        public Adapter caseOperationalLimit( OperationalLimit object ) {
            return createOperationalLimitAdapter();
        }

        @Override
        public Adapter caseStartMainFuelCurve( StartMainFuelCurve object ) {
            return createStartMainFuelCurveAdapter();
        }

        @Override
        public Adapter casePerLengthDCLineParameter( PerLengthDCLineParameter object ) {
            return createPerLengthDCLineParameterAdapter();
        }

        @Override
        public Adapter casePowerTransformer( PowerTransformer object ) {
            return createPowerTransformerAdapter();
        }

        @Override
        public Adapter caseControl( Control object ) {
            return createControlAdapter();
        }

        @Override
        public Adapter caseLinearShuntCompensator( LinearShuntCompensator object ) {
            return createLinearShuntCompensatorAdapter();
        }

        @Override
        public Adapter caseTapChangerTablePoint( TapChangerTablePoint object ) {
            return createTapChangerTablePointAdapter();
        }

        @Override
        public Adapter caseVoltageLimit( VoltageLimit object ) {
            return createVoltageLimitAdapter();
        }

        @Override
        public Adapter defaultCase( EObject object ) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter( Notifier target ) {
        return modelSwitch.doSwitch( ( EObject ) target );
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimObjectWithID <em>Object With ID</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimObjectWithID
     * @generated
     */
    public Adapter createCimObjectWithIDAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BusbarSection <em>Busbar Section</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BusbarSection
     * @generated
     */
    public Adapter createBusbarSectionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTopologicalIsland <em>DC Topological Island</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTopologicalIsland
     * @generated
     */
    public Adapter createDCTopologicalIslandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemotePoint <em>Remote Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemotePoint
     * @generated
     */
    public Adapter createRemotePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentNetwork <em>Equivalent Network</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentNetwork
     * @generated
     */
    public Adapter createEquivalentNetworkAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TopologicalIsland <em>Topological Island</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TopologicalIsland
     * @generated
     */
    public Adapter createTopologicalIslandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthImpedance <em>Per Length Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthImpedance
     * @generated
     */
    public Adapter createPerLengthImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadBreakSwitch <em>Load Break Switch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadBreakSwitch
     * @generated
     */
    public Adapter createLoadBreakSwitchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SubLoadArea <em>Sub Load Area</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SubLoadArea
     * @generated
     */
    public Adapter createSubLoadAreaAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.InflowForecast <em>Inflow Forecast</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.InflowForecast
     * @generated
     */
    public Adapter createInflowForecastAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulationSchedule <em>Regulation Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulationSchedule
     * @generated
     */
    public Adapter createRegulationScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPhasePoint <em>Nonlinear Shunt Compensator Phase Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPhasePoint
     * @generated
     */
    public Adapter createNonlinearShuntCompensatorPhasePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CombustionTurbine <em>Combustion Turbine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CombustionTurbine
     * @generated
     */
    public Adapter createCombustionTurbineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SubGeographicalRegion <em>Sub Geographical Region</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SubGeographicalRegion
     * @generated
     */
    public Adapter createSubGeographicalRegionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthSequenceImpedance <em>Per Length Sequence Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthSequenceImpedance
     * @generated
     */
    public Adapter createPerLengthSequenceImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CTTempActivePowerCurve <em>CT Temp Active Power Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CTTempActivePowerCurve
     * @generated
     */
    public Adapter createCTTempActivePowerCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorLimit <em>Accumulator Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorLimit
     * @generated
     */
    public Adapter createAccumulatorLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentEquipment <em>Equivalent Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentEquipment
     * @generated
     */
    public Adapter createEquivalentEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BasicIntervalSchedule <em>Basic Interval Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BasicIntervalSchedule
     * @generated
     */
    public Adapter createBasicIntervalScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerSymmetrical <em>Phase Tap Changer Symmetrical</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerSymmetrical
     * @generated
     */
    public Adapter createPhaseTapChangerSymmetricalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorValue <em>Accumulator Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorValue
     * @generated
     */
    public Adapter createAccumulatorValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTerminal <em>DC Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTerminal
     * @generated
     */
    public Adapter createDCTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerCutZone <em>Power Cut Zone</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerCutZone
     * @generated
     */
    public Adapter createPowerCutZoneAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PSRType <em>PSR Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PSRType
     * @generated
     */
    public Adapter createPSRTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegularIntervalSchedule <em>Regular Interval Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegularIntervalSchedule
     * @generated
     */
    public Adapter createRegularIntervalScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PrimeMover <em>Prime Mover</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PrimeMover
     * @generated
     */
    public Adapter createPrimeMoverAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChangerTablePoint <em>Ratio Tap Changer Table Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChangerTablePoint
     * @generated
     */
    public Adapter createRatioTapChangerTablePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Jumper <em>Jumper</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Jumper
     * @generated
     */
    public Adapter createJumperAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Recloser <em>Recloser</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Recloser
     * @generated
     */
    public Adapter createRecloserAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACLineSegmentPhase <em>AC Line Segment Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACLineSegmentPhase
     * @generated
     */
    public Adapter createACLineSegmentPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthLineParameter <em>Per Length Line Parameter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthLineParameter
     * @generated
     */
    public Adapter createPerLengthLineParameterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ApparentPowerLimit <em>Apparent Power Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ApparentPowerLimit
     * @generated
     */
    public Adapter createApparentPowerLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RecloseSequence <em>Reclose Sequence</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RecloseSequence
     * @generated
     */
    public Adapter createRecloseSequenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACLineSegment <em>AC Line Segment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACLineSegment
     * @generated
     */
    public Adapter createACLineSegmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergySource <em>Energy Source</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergySource
     * @generated
     */
    public Adapter createEnergySourceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Clamp <em>Clamp</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Clamp
     * @generated
     */
    public Adapter createClampAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoad <em>Conform Load</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoad
     * @generated
     */
    public Adapter createConformLoadAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadResponseCharacteristic <em>Load Response Characteristic</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadResponseCharacteristic
     * @generated
     */
    public Adapter createLoadResponseCharacteristicAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FloatQuantity <em>Float Quantity</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FloatQuantity
     * @generated
     */
    public Adapter createFloatQuantityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChangerTable <em>Ratio Tap Changer Table</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChangerTable
     * @generated
     */
    public Adapter createRatioTapChangerTableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MonthDayInterval <em>Month Day Interval</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MonthDayInterval
     * @generated
     */
    public Adapter createMonthDayIntervalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamSupply <em>Steam Supply</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamSupply
     * @generated
     */
    public Adapter createSteamSupplyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GroundingImpedance <em>Grounding Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GroundingImpedance
     * @generated
     */
    public Adapter createGroundingImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvStatus <em>Sv Status</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvStatus
     * @generated
     */
    public Adapter createSvStatusAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiscreteCommand <em>Discrete Command</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiscreteCommand
     * @generated
     */
    public Adapter createDiscreteCommandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChanger <em>Tap Changer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChanger
     * @generated
     */
    public Adapter createTapChangerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BWRSteamSupply <em>BWR Steam Supply</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BWRSteamSupply
     * @generated
     */
    public Adapter createBWRSteamSupplyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroGeneratingUnit <em>Hydro Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroGeneratingUnit
     * @generated
     */
    public Adapter createHydroGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthPhaseImpedance <em>Per Length Phase Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthPhaseImpedance
     * @generated
     */
    public Adapter createPerLengthPhaseImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ControlAreaGeneratingUnit <em>Control Area Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ControlAreaGeneratingUnit
     * @generated
     */
    public Adapter createControlAreaGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperatingShare <em>Operating Share</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperatingShare
     * @generated
     */
    public Adapter createOperatingShareAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageControlZone <em>Voltage Control Zone</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageControlZone
     * @generated
     */
    public Adapter createVoltageControlZoneAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PWRSteamSupply <em>PWR Steam Supply</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PWRSteamSupply
     * @generated
     */
    public Adapter createPWRSteamSupplyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DecimalQuantity <em>Decimal Quantity</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DecimalQuantity
     * @generated
     */
    public Adapter createDecimalQuantityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBaseTerminal <em>DC Base Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBaseTerminal
     * @generated
     */
    public Adapter createDCBaseTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VsConverter <em>Vs Converter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VsConverter
     * @generated
     */
    public Adapter createVsConverterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyConsumerPhase <em>Energy Consumer Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyConsumerPhase
     * @generated
     */
    public Adapter createEnergyConsumerPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ControlArea <em>Control Area</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ControlArea
     * @generated
     */
    public Adapter createControlAreaAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCDisconnector <em>DC Disconnector</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCDisconnector
     * @generated
     */
    public Adapter createDCDisconnectorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Contingency <em>Contingency</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Contingency
     * @generated
     */
    public Adapter createContingencyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShuntCompensatorPhase <em>Shunt Compensator Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShuntCompensatorPhase
     * @generated
     */
    public Adapter createShuntCompensatorPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Bay <em>Bay</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Bay
     * @generated
     */
    public Adapter createBayAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Diagram <em>Diagram</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Diagram
     * @generated
     */
    public Adapter createDiagramAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LineFault <em>Line Fault</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LineFault
     * @generated
     */
    public Adapter createLineFaultAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorLimitSet <em>Accumulator Limit Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorLimitSet
     * @generated
     */
    public Adapter createAccumulatorLimitSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentInjection <em>Equivalent Injection</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentInjection
     * @generated
     */
    public Adapter createEquivalentInjectionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EmissionAccount <em>Emission Account</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EmissionAccount
     * @generated
     */
    public Adapter createEmissionAccountAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsConnection <em>Power Electronics Connection</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsConnection
     * @generated
     */
    public Adapter createPowerElectronicsConnectionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimitType <em>Operational Limit Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimitType
     * @generated
     */
    public Adapter createOperationalLimitTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CommunicationLink <em>Communication Link</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CommunicationLink
     * @generated
     */
    public Adapter createCommunicationLinkAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FossilFuel <em>Fossil Fuel</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FossilFuel
     * @generated
     */
    public Adapter createFossilFuelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteUnit <em>Remote Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteUnit
     * @generated
     */
    public Adapter createRemoteUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatRecoveryBoiler <em>Heat Recovery Boiler</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatRecoveryBoiler
     * @generated
     */
    public Adapter createHeatRecoveryBoilerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsUnit <em>Power Electronics Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsUnit
     * @generated
     */
    public Adapter createPowerElectronicsUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimitSet <em>Operational Limit Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimitSet
     * @generated
     */
    public Adapter createOperationalLimitSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyArea <em>Energy Area</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyArea
     * @generated
     */
    public Adapter createEnergyAreaAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartRampCurve <em>Start Ramp Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartRampCurve
     * @generated
     */
    public Adapter createStartRampCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquipmentFault <em>Equipment Fault</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquipmentFault
     * @generated
     */
    public Adapter createEquipmentFaultAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Ground <em>Ground</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Ground
     * @generated
     */
    public Adapter createGroundAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SynchrocheckRelay <em>Synchrocheck Relay</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SynchrocheckRelay
     * @generated
     */
    public Adapter createSynchrocheckRelayAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChanger <em>Ratio Tap Changer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RatioTapChanger
     * @generated
     */
    public Adapter createRatioTapChangerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LinearShuntCompensator <em>Linear Shunt Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LinearShuntCompensator
     * @generated
     */
    public Adapter createLinearShuntCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPumpOpSchedule <em>Hydro Pump Op Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPumpOpSchedule
     * @generated
     */
    public Adapter createHydroPumpOpScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IEC61970CIMVersion <em>IEC61970CIM Version</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IEC61970CIMVersion
     * @generated
     */
    public Adapter createIEC61970CIMVersionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PostLineSensor <em>Post Line Sensor</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PostLineSensor
     * @generated
     */
    public Adapter createPostLineSensorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NameType <em>Name Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NameType
     * @generated
     */
    public Adapter createNameTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoadSchedule <em>Conform Load Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoadSchedule
     * @generated
     */
    public Adapter createConformLoadScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadGroup <em>Load Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadGroup
     * @generated
     */
    public Adapter createLoadGroupAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseImpedanceData <em>Phase Impedance Data</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseImpedanceData
     * @generated
     */
    public Adapter createPhaseImpedanceDataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvShuntCompensatorSections <em>Sv Shunt Compensator Sections</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvShuntCompensatorSections
     * @generated
     */
    public Adapter createSvShuntCompensatorSectionsAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadArea <em>Load Area</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LoadArea
     * @generated
     */
    public Adapter createLoadAreaAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerEnd <em>Transformer End</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerEnd
     * @generated
     */
    public Adapter createTransformerEndAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTabular <em>Phase Tap Changer Tabular</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTabular
     * @generated
     */
    public Adapter createPhaseTapChangerTabularAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartupModel <em>Startup Model</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartupModel
     * @generated
     */
    public Adapter createStartupModelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DateInterval <em>Date Interval</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DateInterval
     * @generated
     */
    public Adapter createDateIntervalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IrregularIntervalSchedule <em>Irregular Interval Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IrregularIntervalSchedule
     * @generated
     */
    public Adapter createIrregularIntervalScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Switch <em>Switch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Switch
     * @generated
     */
    public Adapter createSwitchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCShunt <em>DC Shunt</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCShunt
     * @generated
     */
    public Adapter createDCShuntAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Command <em>Command</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Command
     * @generated
     */
    public Adapter createCommandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BaseVoltage <em>Base Voltage</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BaseVoltage
     * @generated
     */
    public Adapter createBaseVoltageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Reservoir <em>Reservoir</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Reservoir
     * @generated
     */
    public Adapter createReservoirAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConnectivityNode <em>Connectivity Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConnectivityNode
     * @generated
     */
    public Adapter createConnectivityNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.WindGeneratingUnit <em>Wind Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.WindGeneratingUnit
     * @generated
     */
    public Adapter createWindGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BusNameMarker <em>Bus Name Marker</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BusNameMarker
     * @generated
     */
    public Adapter createBusNameMarkerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvTapStep <em>Sv Tap Step</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvTapStep
     * @generated
     */
    public Adapter createSvTapStepAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GenUnitOpSchedule <em>Gen Unit Op Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GenUnitOpSchedule
     * @generated
     */
    public Adapter createGenUnitOpScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TargetLevelSchedule <em>Target Level Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TargetLevelSchedule
     * @generated
     */
    public Adapter createTargetLevelScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentShunt <em>Equivalent Shunt</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentShunt
     * @generated
     */
    public Adapter createEquivalentShuntAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerMeshImpedance <em>Transformer Mesh Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerMeshImpedance
     * @generated
     */
    public Adapter createTransformerMeshImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiscreteValue <em>Discrete Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiscreteValue
     * @generated
     */
    public Adapter createDiscreteValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TailbayLossCurve <em>Tailbay Loss Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TailbayLossCurve
     * @generated
     */
    public Adapter createTailbayLossCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurveData <em>Curve Data</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurveData
     * @generated
     */
    public Adapter createCurveDataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Discrete <em>Discrete</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Discrete
     * @generated
     */
    public Adapter createDiscreteAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringMeasurementValue <em>String Measurement Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringMeasurementValue
     * @generated
     */
    public Adapter createStringMeasurementValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatInputCurve <em>Heat Input Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatInputCurve
     * @generated
     */
    public Adapter createHeatInputCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCNode <em>DC Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCNode
     * @generated
     */
    public Adapter createDCNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BranchGroup <em>Branch Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BranchGroup
     * @generated
     */
    public Adapter createBranchGroupAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthDCLineParameter <em>Per Length DC Line Parameter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PerLengthDCLineParameter
     * @generated
     */
    public Adapter createPerLengthDCLineParameterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTable <em>Phase Tap Changer Table</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTable
     * @generated
     */
    public Adapter createPhaseTapChangerTableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConnectivityNodeContainer <em>Connectivity Node Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConnectivityNodeContainer
     * @generated
     */
    public Adapter createConnectivityNodeContainerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CompositeSwitch <em>Composite Switch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CompositeSwitch
     * @generated
     */
    public Adapter createCompositeSwitchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapSchedule <em>Tap Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapSchedule
     * @generated
     */
    public Adapter createTapScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChanger <em>Phase Tap Changer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChanger
     * @generated
     */
    public Adapter createPhaseTapChangerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultIndicator <em>Fault Indicator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultIndicator
     * @generated
     */
    public Adapter createFaultIndicatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IntegerQuantity <em>Integer Quantity</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IntegerQuantity
     * @generated
     */
    public Adapter createIntegerQuantityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AltTieMeas <em>Alt Tie Meas</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AltTieMeas
     * @generated
     */
    public Adapter createAltTieMeasAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DateTimeInterval <em>Date Time Interval</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DateTimeInterval
     * @generated
     */
    public Adapter createDateTimeIntervalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TextDiagramObject <em>Text Diagram Object</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TextDiagramObject
     * @generated
     */
    public Adapter createTextDiagramObjectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringQuantity <em>String Quantity</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringQuantity
     * @generated
     */
    public Adapter createStringQuantityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCConductingEquipment <em>DC Conducting Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCConductingEquipment
     * @generated
     */
    public Adapter createDCConductingEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SetPoint <em>Set Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SetPoint
     * @generated
     */
    public Adapter createSetPointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SwitchPhase <em>Switch Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SwitchPhase
     * @generated
     */
    public Adapter createSwitchPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Fault <em>Fault</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Fault
     * @generated
     */
    public Adapter createFaultAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringMeasurement <em>String Measurement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StringMeasurement
     * @generated
     */
    public Adapter createStringMeasurementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AirCompressor <em>Air Compressor</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AirCompressor
     * @generated
     */
    public Adapter createAirCompressorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShutdownCurve <em>Shutdown Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShutdownCurve
     * @generated
     */
    public Adapter createShutdownCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCConverterDCTerminal <em>ACDC Converter DC Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCConverterDCTerminal
     * @generated
     */
    public Adapter createACDCConverterDCTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultCauseType <em>Fault Cause Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultCauseType
     * @generated
     */
    public Adapter createFaultCauseTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValueSource <em>Measurement Value Source</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValueSource
     * @generated
     */
    public Adapter createMeasurementValueSourceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SeasonDayTypeSchedule <em>Season Day Type Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SeasonDayTypeSchedule
     * @generated
     */
    public Adapter createSeasonDayTypeScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TimeInterval <em>Time Interval</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TimeInterval
     * @generated
     */
    public Adapter createTimeIntervalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Curve <em>Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Curve
     * @generated
     */
    public Adapter createCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PotentialTransformer <em>Potential Transformer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PotentialTransformer
     * @generated
     */
    public Adapter createPotentialTransformerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerTransformerEnd <em>Power Transformer End</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerTransformerEnd
     * @generated
     */
    public Adapter createPowerTransformerEndAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GrossToNetActivePowerCurve <em>Gross To Net Active Power Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GrossToNetActivePowerCurve
     * @generated
     */
    public Adapter createGrossToNetActivePowerCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsWindUnit <em>Power Electronics Wind Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerElectronicsWindUnit
     * @generated
     */
    public Adapter createPowerElectronicsWindUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MutualCoupling <em>Mutual Coupling</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MutualCoupling
     * @generated
     */
    public Adapter createMutualCouplingAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PetersenCoil <em>Petersen Coil</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PetersenCoil
     * @generated
     */
    public Adapter createPetersenCoilAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerTank <em>Transformer Tank</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerTank
     * @generated
     */
    public Adapter createTransformerTankAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GeneratingUnit <em>Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GeneratingUnit
     * @generated
     */
    public Adapter createGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AsynchronousMachine <em>Asynchronous Machine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AsynchronousMachine
     * @generated
     */
    public Adapter createAsynchronousMachineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GenUnitOpCostCurve <em>Gen Unit Op Cost Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GenUnitOpCostCurve
     * @generated
     */
    public Adapter createGenUnitOpCostCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StateVariable <em>State Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StateVariable
     * @generated
     */
    public Adapter createStateVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RotatingMachine <em>Rotating Machine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RotatingMachine
     * @generated
     */
    public Adapter createRotatingMachineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamSendoutSchedule <em>Steam Sendout Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamSendoutSchedule
     * @generated
     */
    public Adapter createSteamSendoutScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValueQuality <em>Measurement Value Quality</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValueQuality
     * @generated
     */
    public Adapter createMeasurementValueQualityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NuclearGeneratingUnit <em>Nuclear Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NuclearGeneratingUnit
     * @generated
     */
    public Adapter createNuclearGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Measurement <em>Measurement</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Measurement
     * @generated
     */
    public Adapter createMeasurementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ProtectionEquipment <em>Protection Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ProtectionEquipment
     * @generated
     */
    public Adapter createProtectionEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ValueAliasSet <em>Value Alias Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ValueAliasSet
     * @generated
     */
    public Adapter createValueAliasSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Fuse <em>Fuse</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Fuse
     * @generated
     */
    public Adapter createFuseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FrequencyConverter <em>Frequency Converter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FrequencyConverter
     * @generated
     */
    public Adapter createFrequencyConverterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ProtectedSwitch <em>Protected Switch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ProtectedSwitch
     * @generated
     */
    public Adapter createProtectedSwitchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BaseFrequency <em>Base Frequency</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BaseFrequency
     * @generated
     */
    public Adapter createBaseFrequencyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoad <em>Non Conform Load</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoad
     * @generated
     */
    public Adapter createNonConformLoadAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CombinedCyclePlant <em>Combined Cycle Plant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CombinedCyclePlant
     * @generated
     */
    public Adapter createCombinedCyclePlantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SeriesCompensator <em>Series Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SeriesCompensator
     * @generated
     */
    public Adapter createSeriesCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BranchGroupTerminal <em>Branch Group Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BranchGroupTerminal
     * @generated
     */
    public Adapter createBranchGroupTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Plant <em>Plant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Plant
     * @generated
     */
    public Adapter createPlantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ContingencyElement <em>Contingency Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ContingencyElement
     * @generated
     */
    public Adapter createContingencyElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCSeriesDevice <em>DC Series Device</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCSeriesDevice
     * @generated
     */
    public Adapter createDCSeriesDeviceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPump <em>Hydro Pump</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPump
     * @generated
     */
    public Adapter createHydroPumpAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Line <em>Line</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Line
     * @generated
     */
    public Adapter createLineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvVoltage <em>Sv Voltage</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvVoltage
     * @generated
     */
    public Adapter createSvVoltageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogLimit <em>Analog Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogLimit
     * @generated
     */
    public Adapter createAnalogLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GroundDisconnector <em>Ground Disconnector</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GroundDisconnector
     * @generated
     */
    public Adapter createGroundDisconnectorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquipmentContainer <em>Equipment Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquipmentContainer
     * @generated
     */
    public Adapter createEquipmentContainerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FossilSteamSupply <em>Fossil Steam Supply</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FossilSteamSupply
     * @generated
     */
    public Adapter createFossilSteamSupplyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Accumulator <em>Accumulator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Accumulator
     * @generated
     */
    public Adapter createAccumulatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCLine <em>DC Line</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCLine
     * @generated
     */
    public Adapter createDCLineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerLinear <em>Phase Tap Changer Linear</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerLinear
     * @generated
     */
    public Adapter createPhaseTapChangerLinearAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Sensor <em>Sensor</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Sensor
     * @generated
     */
    public Adapter createSensorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LinearShuntCompensatorPhase <em>Linear Shunt Compensator Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LinearShuntCompensatorPhase
     * @generated
     */
    public Adapter createLinearShuntCompensatorPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Subcritical <em>Subcritical</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Subcritical
     * @generated
     */
    public Adapter createSubcriticalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Supercritical <em>Supercritical</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Supercritical
     * @generated
     */
    public Adapter createSupercriticalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCEquipmentContainer <em>DC Equipment Container</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCEquipmentContainer
     * @generated
     */
    public Adapter createDCEquipmentContainerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Terminal <em>Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Terminal
     * @generated
     */
    public Adapter createTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvInjection <em>Sv Injection</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvInjection
     * @generated
     */
    public Adapter createSvInjectionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulatingCondEq <em>Regulating Cond Eq</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulatingCondEq
     * @generated
     */
    public Adapter createRegulatingCondEqAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Breaker <em>Breaker</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Breaker
     * @generated
     */
    public Adapter createBreakerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LimitSet <em>Limit Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LimitSet
     * @generated
     */
    public Adapter createLimitSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Season <em>Season</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Season
     * @generated
     */
    public Adapter createSeasonAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCChopper <em>DC Chopper</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCChopper
     * @generated
     */
    public Adapter createDCChopperAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPowerPlant <em>Hydro Power Plant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroPowerPlant
     * @generated
     */
    public Adapter createHydroPowerPlantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentTransformer <em>Current Transformer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentTransformer
     * @generated
     */
    public Adapter createCurrentTransformerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTablePoint <em>Phase Tap Changer Table Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerTablePoint
     * @generated
     */
    public Adapter createPhaseTapChangerTablePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogValue <em>Analog Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogValue
     * @generated
     */
    public Adapter createAnalogValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ReportingSuperGroup <em>Reporting Super Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ReportingSuperGroup
     * @generated
     */
    public Adapter createReportingSuperGroupAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteControl <em>Remote Control</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteControl
     * @generated
     */
    public Adapter createRemoteControlAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCConverterUnit <em>DC Converter Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCConverterUnit
     * @generated
     */
    public Adapter createDCConverterUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShuntCompensator <em>Shunt Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ShuntCompensator
     * @generated
     */
    public Adapter createShuntCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectGluePoint <em>Diagram Object Glue Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectGluePoint
     * @generated
     */
    public Adapter createDiagramObjectGluePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ActivePowerLimit <em>Active Power Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ActivePowerLimit
     * @generated
     */
    public Adapter createActivePowerLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteSource <em>Remote Source</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RemoteSource
     * @generated
     */
    public Adapter createRemoteSourceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroGeneratingEfficiencyCurve <em>Hydro Generating Efficiency Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroGeneratingEfficiencyCurve
     * @generated
     */
    public Adapter createHydroGeneratingEfficiencyCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBusbar <em>DC Busbar</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBusbar
     * @generated
     */
    public Adapter createDCBusbarAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Equipment <em>Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Equipment
     * @generated
     */
    public Adapter createEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FuelAllocationSchedule <em>Fuel Allocation Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FuelAllocationSchedule
     * @generated
     */
    public Adapter createFuelAllocationScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConductingEquipment <em>Conducting Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConductingEquipment
     * @generated
     */
    public Adapter createConductingEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogLimitSet <em>Analog Limit Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogLimitSet
     * @generated
     */
    public Adapter createAnalogLimitSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Quality61850 <em>Quality61850</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Quality61850
     * @generated
     */
    public Adapter createQuality61850Adapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoadGroup <em>Conform Load Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ConformLoadGroup
     * @generated
     */
    public Adapter createConformLoadGroupAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogControl <em>Analog Control</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AnalogControl
     * @generated
     */
    public Adapter createAnalogControlAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ThermalGeneratingUnit <em>Thermal Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ThermalGeneratingUnit
     * @generated
     */
    public Adapter createThermalGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerSystemResource <em>Power System Resource</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerSystemResource
     * @generated
     */
    public Adapter createPowerSystemResourceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerCoreAdmittance <em>Transformer Core Admittance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerCoreAdmittance
     * @generated
     */
    public Adapter createTransformerCoreAdmittanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SynchronousMachine <em>Synchronous Machine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SynchronousMachine
     * @generated
     */
    public Adapter createSynchronousMachineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Sectionaliser <em>Sectionaliser</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Sectionaliser
     * @generated
     */
    public Adapter createSectionaliserAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Substation <em>Substation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Substation
     * @generated
     */
    public Adapter createSubstationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Cut <em>Cut</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Cut
     * @generated
     */
    public Adapter createCutAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EarthFaultCompensator <em>Earth Fault Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EarthFaultCompensator
     * @generated
     */
    public Adapter createEarthFaultCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SwitchSchedule <em>Switch Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SwitchSchedule
     * @generated
     */
    public Adapter createSwitchScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimit <em>Operational Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperationalLimit
     * @generated
     */
    public Adapter createOperationalLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ReactiveCapabilityCurve <em>Reactive Capability Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ReactiveCapabilityCurve
     * @generated
     */
    public Adapter createReactiveCapabilityCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyConsumer <em>Energy Consumer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EnergyConsumer
     * @generated
     */
    public Adapter createEnergyConsumerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Conductor <em>Conductor</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Conductor
     * @generated
     */
    public Adapter createConductorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Disconnector <em>Disconnector</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Disconnector
     * @generated
     */
    public Adapter createDisconnectorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TopologicalNode <em>Topological Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TopologicalNode
     * @generated
     */
    public Adapter createTopologicalNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerStarImpedance <em>Transformer Star Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerStarImpedance
     * @generated
     */
    public Adapter createTransformerStarImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Limit <em>Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Limit
     * @generated
     */
    public Adapter createLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoadSchedule <em>Non Conform Load Schedule</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoadSchedule
     * @generated
     */
    public Adapter createNonConformLoadScheduleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Control <em>Control</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Control
     * @generated
     */
    public Adapter createControlAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BasePower <em>Base Power</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.BasePower
     * @generated
     */
    public Adapter createBasePowerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Junction <em>Junction</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Junction
     * @generated
     */
    public Adapter createJunctionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartIgnFuelCurve <em>Start Ign Fuel Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartIgnFuelCurve
     * @generated
     */
    public Adapter createStartIgnFuelCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RaiseLowerCommand <em>Raise Lower Command</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RaiseLowerCommand
     * @generated
     */
    public Adapter createRaiseLowerCommandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ContingencyEquipment <em>Contingency Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ContingencyEquipment
     * @generated
     */
    public Adapter createContingencyEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulatingControl <em>Regulating Control</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegulatingControl
     * @generated
     */
    public Adapter createRegulatingControlAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerAsymmetrical <em>Phase Tap Changer Asymmetrical</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerAsymmetrical
     * @generated
     */
    public Adapter createPhaseTapChangerAsymmetricalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCGround <em>DC Ground</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCGround
     * @generated
     */
    public Adapter createDCGroundAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SolarGeneratingUnit <em>Solar Generating Unit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SolarGeneratingUnit
     * @generated
     */
    public Adapter createSolarGeneratingUnitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValue <em>Measurement Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.MeasurementValue
     * @generated
     */
    public Adapter createMeasurementValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StaticVarCompensator <em>Static Var Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StaticVarCompensator
     * @generated
     */
    public Adapter createStaticVarCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentRelay <em>Current Relay</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentRelay
     * @generated
     */
    public Adapter createCurrentRelayAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatRateCurve <em>Heat Rate Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HeatRateCurve
     * @generated
     */
    public Adapter createHeatRateCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChangerTablePoint <em>Tap Changer Table Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChangerTablePoint
     * @generated
     */
    public Adapter createTapChangerTablePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IdentifiedObject <em>Identified Object</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IdentifiedObject
     * @generated
     */
    public Adapter createIdentifiedObjectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CogenerationPlant <em>Cogeneration Plant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CogenerationPlant
     * @generated
     */
    public Adapter createCogenerationPlantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCLineSegment <em>DC Line Segment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCLineSegment
     * @generated
     */
    public Adapter createDCLineSegmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AuxiliaryEquipment <em>Auxiliary Equipment</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AuxiliaryEquipment
     * @generated
     */
    public Adapter createAuxiliaryEquipmentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTopologicalNode <em>DC Topological Node</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCTopologicalNode
     * @generated
     */
    public Adapter createDCTopologicalNodeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Analog <em>Analog</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Analog
     * @generated
     */
    public Adapter createAnalogAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CAESPlant <em>CAES Plant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CAESPlant
     * @generated
     */
    public Adapter createCAESPlantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VsCapabilityCurve <em>Vs Capability Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VsCapabilityCurve
     * @generated
     */
    public Adapter createVsCapabilityCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EmissionCurve <em>Emission Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EmissionCurve
     * @generated
     */
    public Adapter createEmissionCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TieFlow <em>Tie Flow</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TieFlow
     * @generated
     */
    public Adapter createTieFlowAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCTerminal <em>ACDC Terminal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCTerminal
     * @generated
     */
    public Adapter createACDCTerminalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoadGroup <em>Non Conform Load Group</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonConformLoadGroup
     * @generated
     */
    public Adapter createNonConformLoadGroupAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DrumBoiler <em>Drum Boiler</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DrumBoiler
     * @generated
     */
    public Adapter createDrumBoilerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StationSupply <em>Station Supply</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StationSupply
     * @generated
     */
    public Adapter createStationSupplyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IrregularTimePoint <em>Irregular Time Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IrregularTimePoint
     * @generated
     */
    public Adapter createIrregularTimePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.WaveTrap <em>Wave Trap</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.WaveTrap
     * @generated
     */
    public Adapter createWaveTrapAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBreaker <em>DC Breaker</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCBreaker
     * @generated
     */
    public Adapter createDCBreakerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Name <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Name
     * @generated
     */
    public Adapter createNameAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CsConverter <em>Cs Converter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CsConverter
     * @generated
     */
    public Adapter createCsConverterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageLevel <em>Voltage Level</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageLevel
     * @generated
     */
    public Adapter createVoltageLevelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PenstockLossCurve <em>Penstock Loss Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PenstockLossCurve
     * @generated
     */
    public Adapter createPenstockLossCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Connector <em>Connector</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Connector
     * @generated
     */
    public Adapter createConnectorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperatingParticipant <em>Operating Participant</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperatingParticipant
     * @generated
     */
    public Adapter createOperatingParticipantAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AltGeneratingUnitMeas <em>Alt Generating Unit Meas</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AltGeneratingUnitMeas
     * @generated
     */
    public Adapter createAltGeneratingUnitMeasAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartMainFuelCurve <em>Start Main Fuel Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.StartMainFuelCurve
     * @generated
     */
    public Adapter createStartMainFuelCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageLimit <em>Voltage Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VoltageLimit
     * @generated
     */
    public Adapter createVoltageLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroTurbine <em>Hydro Turbine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.HydroTurbine
     * @generated
     */
    public Adapter createHydroTurbineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DayType <em>Day Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DayType
     * @generated
     */
    public Adapter createDayTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCConverter <em>ACDC Converter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ACDCConverter
     * @generated
     */
    public Adapter createACDCConverterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPhase <em>Nonlinear Shunt Compensator Phase</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPhase
     * @generated
     */
    public Adapter createNonlinearShuntCompensatorPhaseAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentBranch <em>Equivalent Branch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.EquivalentBranch
     * @generated
     */
    public Adapter createEquivalentBranchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCSwitch <em>DC Switch</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DCSwitch
     * @generated
     */
    public Adapter createDCSwitchAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvPowerFlow <em>Sv Power Flow</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SvPowerFlow
     * @generated
     */
    public Adapter createSvPowerFlowAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GeographicalRegion <em>Geographical Region</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.GeographicalRegion
     * @generated
     */
    public Adapter createGeographicalRegionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramStyle <em>Diagram Style</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramStyle
     * @generated
     */
    public Adapter createDiagramStyleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectPoint <em>Diagram Object Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectPoint
     * @generated
     */
    public Adapter createDiagramObjectPointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChangerControl <em>Tap Changer Control</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TapChangerControl
     * @generated
     */
    public Adapter createTapChangerControlAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamTurbine <em>Steam Turbine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SteamTurbine
     * @generated
     */
    public Adapter createSteamTurbineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPoint <em>Nonlinear Shunt Compensator Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensatorPoint
     * @generated
     */
    public Adapter createNonlinearShuntCompensatorPointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerNonLinear <em>Phase Tap Changer Non Linear</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PhaseTapChangerNonLinear
     * @generated
     */
    public Adapter createPhaseTapChangerNonLinearAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorReset <em>Accumulator Reset</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.AccumulatorReset
     * @generated
     */
    public Adapter createAccumulatorResetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerTankEnd <em>Transformer Tank End</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.TransformerTankEnd
     * @generated
     */
    public Adapter createTransformerTankEndAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VisibilityLayer <em>Visibility Layer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.VisibilityLayer
     * @generated
     */
    public Adapter createVisibilityLayerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ValueToAlias <em>Value To Alias</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ValueToAlias
     * @generated
     */
    public Adapter createValueToAliasAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultImpedance <em>Fault Impedance</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.FaultImpedance
     * @generated
     */
    public Adapter createFaultImpedanceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentLimit <em>Current Limit</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CurrentLimit
     * @generated
     */
    public Adapter createCurrentLimitAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObject <em>Diagram Object</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObject
     * @generated
     */
    public Adapter createDiagramObjectAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NameTypeAuthority <em>Name Type Authority</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NameTypeAuthority
     * @generated
     */
    public Adapter createNameTypeAuthorityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerTransformer <em>Power Transformer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerTransformer
     * @generated
     */
    public Adapter createPowerTransformerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensator <em>Nonlinear Shunt Compensator</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.NonlinearShuntCompensator
     * @generated
     */
    public Adapter createNonlinearShuntCompensatorAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegularTimePoint <em>Regular Time Point</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.RegularTimePoint
     * @generated
     */
    public Adapter createRegularTimePointAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LevelVsVolumeCurve <em>Level Vs Volume Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.LevelVsVolumeCurve
     * @generated
     */
    public Adapter createLevelVsVolumeCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SurgeArrester <em>Surge Arrester</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.SurgeArrester
     * @generated
     */
    public Adapter createSurgeArresterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IncrementalHeatRateCurve <em>Incremental Heat Rate Curve</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.IncrementalHeatRateCurve
     * @generated
     */
    public Adapter createIncrementalHeatRateCurveAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ExternalNetworkInjection <em>External Network Injection</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.ExternalNetworkInjection
     * @generated
     */
    public Adapter createExternalNetworkInjectionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectStyle <em>Diagram Object Style</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.DiagramObjectStyle
     * @generated
     */
    public Adapter createDiagramObjectStyleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //CimAdapterFactory
