/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.CimPackage;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Control;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.Measurement;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.OperatingShare;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PSRType;
import fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.PowerSystemResource;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Power System Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.PowerSystemResourceImpl#getMeasurements <em>Measurements</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.PowerSystemResourceImpl#getControls <em>Controls</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.PowerSystemResourceImpl#getOperatingShare <em>Operating Share</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim16.iec61970cim16v33.cim.impl.PowerSystemResourceImpl#getPSRType <em>PSR Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PowerSystemResourceImpl extends IdentifiedObjectImpl implements PowerSystemResource {
    /**
     * The cached value of the '{@link #getMeasurements() <em>Measurements</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMeasurements()
     * @generated
     * @ordered
     */
    protected EList< Measurement > measurements;
    /**
     * The cached value of the '{@link #getControls() <em>Controls</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getControls()
     * @generated
     * @ordered
     */
    protected EList< Control > controls;
    /**
     * The cached value of the '{@link #getOperatingShare() <em>Operating Share</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOperatingShare()
     * @generated
     * @ordered
     */
    protected EList< OperatingShare > operatingShare;
    /**
     * The cached value of the '{@link #getPSRType() <em>PSR Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPSRType()
     * @generated
     * @ordered
     */
    protected PSRType psrType;
    /**
     * This is true if the PSR Type reference has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean psrTypeESet;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PowerSystemResourceImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CimPackage.eINSTANCE.getPowerSystemResource();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Measurement > getMeasurements() {
        if( measurements == null ) {
            measurements = new EObjectWithInverseResolvingEList.Unsettable< Measurement >( Measurement.class, this,
                    CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS, CimPackage.MEASUREMENT__POWER_SYSTEM_RESOURCE );
        }
        return measurements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetMeasurements() {
        if( measurements != null ) ( ( InternalEList.Unsettable< ? > ) measurements ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetMeasurements() {
        return measurements != null && ( ( InternalEList.Unsettable< ? > ) measurements ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Control > getControls() {
        if( controls == null ) {
            controls = new EObjectWithInverseResolvingEList.Unsettable< Control >( Control.class, this,
                    CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS, CimPackage.CONTROL__POWER_SYSTEM_RESOURCE );
        }
        return controls;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetControls() {
        if( controls != null ) ( ( InternalEList.Unsettable< ? > ) controls ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetControls() {
        return controls != null && ( ( InternalEList.Unsettable< ? > ) controls ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< OperatingShare > getOperatingShare() {
        if( operatingShare == null ) {
            operatingShare = new EObjectWithInverseResolvingEList.Unsettable< OperatingShare >( OperatingShare.class,
                    this, CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE,
                    CimPackage.OPERATING_SHARE__POWER_SYSTEM_RESOURCE );
        }
        return operatingShare;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetOperatingShare() {
        if( operatingShare != null ) ( ( InternalEList.Unsettable< ? > ) operatingShare ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetOperatingShare() {
        return operatingShare != null && ( ( InternalEList.Unsettable< ? > ) operatingShare ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PSRType getPSRType() {
        return psrType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetPSRType( PSRType newPSRType, NotificationChain msgs ) {
        PSRType oldPSRType = psrType;
        psrType = newPSRType;
        boolean oldPSRTypeESet = psrTypeESet;
        psrTypeESet = true;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.SET,
                    CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE, oldPSRType, newPSRType, !oldPSRTypeESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setPSRType( PSRType newPSRType ) {
        if( newPSRType != psrType ) {
            NotificationChain msgs = null;
            if( psrType != null )
                msgs = ( ( InternalEObject ) psrType ).eInverseRemove( this,
                        CimPackage.PSR_TYPE__POWER_SYSTEM_RESOURCES, PSRType.class, msgs );
            if( newPSRType != null )
                msgs = ( ( InternalEObject ) newPSRType ).eInverseAdd( this,
                        CimPackage.PSR_TYPE__POWER_SYSTEM_RESOURCES, PSRType.class, msgs );
            msgs = basicSetPSRType( newPSRType, msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldPSRTypeESet = psrTypeESet;
            psrTypeESet = true;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE,
                        newPSRType, newPSRType, !oldPSRTypeESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicUnsetPSRType( NotificationChain msgs ) {
        PSRType oldPSRType = psrType;
        psrType = null;
        boolean oldPSRTypeESet = psrTypeESet;
        psrTypeESet = false;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.UNSET,
                    CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE, oldPSRType, null, oldPSRTypeESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetPSRType() {
        if( psrType != null ) {
            NotificationChain msgs = null;
            msgs = ( ( InternalEObject ) psrType ).eInverseRemove( this, CimPackage.PSR_TYPE__POWER_SYSTEM_RESOURCES,
                    PSRType.class, msgs );
            msgs = basicUnsetPSRType( msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldPSRTypeESet = psrTypeESet;
            psrTypeESet = false;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE,
                        null, null, oldPSRTypeESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetPSRType() {
        return psrTypeESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getMeasurements() ).basicAdd( otherEnd,
                    msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getControls() ).basicAdd( otherEnd,
                    msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getOperatingShare() )
                    .basicAdd( otherEnd, msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            if( psrType != null )
                msgs = ( ( InternalEObject ) psrType ).eInverseRemove( this,
                        CimPackage.PSR_TYPE__POWER_SYSTEM_RESOURCES, PSRType.class, msgs );
            return basicSetPSRType( ( PSRType ) otherEnd, msgs );
        }
        return super.eInverseAdd( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            return ( ( InternalEList< ? > ) getMeasurements() ).basicRemove( otherEnd, msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            return ( ( InternalEList< ? > ) getControls() ).basicRemove( otherEnd, msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            return ( ( InternalEList< ? > ) getOperatingShare() ).basicRemove( otherEnd, msgs );
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            return basicUnsetPSRType( msgs );
        }
        return super.eInverseRemove( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            return getMeasurements();
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            return getControls();
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            return getOperatingShare();
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            return getPSRType();
        }
        return super.eGet( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void eSet( int featureID, Object newValue ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            getMeasurements().clear();
            getMeasurements().addAll( ( Collection< ? extends Measurement > ) newValue );
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            getControls().clear();
            getControls().addAll( ( Collection< ? extends Control > ) newValue );
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            getOperatingShare().clear();
            getOperatingShare().addAll( ( Collection< ? extends OperatingShare > ) newValue );
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            setPSRType( ( PSRType ) newValue );
            return;
        }
        super.eSet( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset( int featureID ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            unsetMeasurements();
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            unsetControls();
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            unsetOperatingShare();
            return;
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            unsetPSRType();
            return;
        }
        super.eUnset( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet( int featureID ) {
        switch( featureID ) {
        case CimPackage.POWER_SYSTEM_RESOURCE__MEASUREMENTS:
            return isSetMeasurements();
        case CimPackage.POWER_SYSTEM_RESOURCE__CONTROLS:
            return isSetControls();
        case CimPackage.POWER_SYSTEM_RESOURCE__OPERATING_SHARE:
            return isSetOperatingShare();
        case CimPackage.POWER_SYSTEM_RESOURCE__PSR_TYPE:
            return isSetPSRType();
        }
        return super.eIsSet( featureID );
    }

} //PowerSystemResourceImpl
